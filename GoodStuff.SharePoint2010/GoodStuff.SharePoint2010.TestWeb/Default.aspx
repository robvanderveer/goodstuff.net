﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TestWeb.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="GoodStuff.SharePoint2010.TestWeb._Default" %>

<%@ Register Assembly="GoodStuff.SharePoint2010, Version=1.0.0.0, Culture=neutral, PublicKeyToken=3241d6f3eb95dd1c"
    Namespace="GoodStuff.SharePoint2010.WebParts.RedirectWebPart" TagPrefix="cc1" %>

<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <asp:WebPartZone runat="server" ID="zone1" PartChromeStyle-BorderStyle="None">
        <ZoneTemplate>
            <cc1:RedirectWebPart runat="server" id="testLogin" RedirectUrl="http://goodstuff.codeplex.com" IsActive="true" />
        </ZoneTemplate>
    </asp:WebPartZone>
</asp:Content>
