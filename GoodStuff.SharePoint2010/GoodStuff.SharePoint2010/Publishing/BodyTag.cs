﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using Microsoft.SharePoint;

namespace GoodStuff.SharePoint2010.Publishing
{
    /// <summary>
    /// HTML bodytag aware of the authenticated status of the current request. When request is not anonymous (thus authenticated) the body tag 
    /// receive the v4master (css)class. The benefit to hide this for anonymous requests is avoiding the scolling mechanism of SharePoint (Ribbon).
    /// </summary>
    public class LoginStatusHtmlBodyTag : HtmlContainerControl
    {
        /// <summary>
        /// Scroll property to maintain the possibility to specifiy scroll 'no' or 'yes', based on the default masterpages within SharePoint. Default no scroll attribute will be rendered.
        /// </summary>
        public string Scroll { get; set; }

        /// <summary>
        /// Alternative CSS class besides the SharePoint classes
        /// </summary>
        public string CssClass { get; set; }

        /// <summary>
        /// Overriden the default the hide normal behavior
        /// </summary>
        /// <param name="writer"></param>
        protected override void RenderBeginTag(HtmlTextWriter writer)
        {
        }

        /// <summary>
        /// Overriden the default the hide normal behavior
        /// </summary>
        /// <param name="writer"></param>
        protected override void RenderEndTag(HtmlTextWriter writer)
        {
        }

        /// <summary>
        /// Render the body tag based on the login status of the current request.
        /// </summary>
        /// <param name="writer"></param>
        protected override void Render(HtmlTextWriter writer)
        {
            // Define CSS class
            string cssClass = string.IsNullOrEmpty(this.CssClass) ? Constants.CssClasses.Masterpage.Body : string.Format("{0} {1}", Constants.CssClasses.Masterpage.Body, this.CssClass);

            //Check for logged in visitors compared to logged in editors
            if (this.IsLoggedInIntoCMS())
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);
            }
            else if (!string.IsNullOrEmpty(this.CssClass))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, this.CssClass);
            }

            if (!string.IsNullOrEmpty(this.Scroll))
            {
                writer.AddAttribute("scroll", this.Scroll);
            }
            writer.AddAttribute("onload", this.GetOnLoadAttributeValue());
            writer.RenderBeginTag(HtmlTextWriterTag.Body);
            base.Render(writer);
            writer.RenderEndTag();
        }

        /// <summary>
        /// Returns boolean indicating if the user (current request) is authenticated (logged in CMS) or not (anonymous visitor on public endpoint). If true SharePoint css-class will be added. By default check on authenticated request and AddAndCustomizePages-permission.
        /// </summary>
        /// <returns></returns>
        protected virtual bool IsLoggedInIntoCMS()
        {
            return this.Context.Request.IsAuthenticated && SPContext.Current.Web.DoesUserHavePermissions(SPBasePermissions.AddAndCustomizePages);
        }

        /// <summary>
        /// Get the javascriptscript to load as 'onload' attribute. Default --> if (typeof(_spBodyOnLoadWrapper) != 'undefined') _spBodyOnLoadWrapper();
        /// </summary>
        /// <returns></returns>
        protected virtual string GetOnLoadAttributeValue()
        {
            return "if (typeof(_spBodyOnLoadWrapper) != 'undefined') _spBodyOnLoadWrapper();";
        }
    }
}
