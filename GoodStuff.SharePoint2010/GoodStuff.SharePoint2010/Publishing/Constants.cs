﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.SharePoint2010.Publishing
{
    /// <summary>
    /// Constants class holder static information about the SharePoint 2010 Publishing features
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Constants about SiteColumns created by the SharePoint 2010 Publishing features
        /// </summary>
        /// <remarks>http://msdn.microsoft.com/en-us/library/ff648024.aspx</remarks>
        public static class SiteColumns
        {
            /// <summary>
            /// Pagecontent sitecolumn
            /// </summary>
            public static class Content
            {
                /// <summary>
                /// ID (Guid) of the PageContent sitecolumn.
                /// </summary>
                public static readonly Guid ID = new Guid("F55C4D88-1F2E-4ad9-AAA8-819AF4EE7EE8");
            }

            /// <summary>
            /// RollUpImage sitecolumn
            /// </summary>
            public static class RollUpImage
            {
                /// <summary>
                /// ID (Guid) of the RollUpImage sitecolumn.
                /// </summary>
                public static readonly Guid ID = new Guid("543bc2cf-1f30-488e-8f25-6fe3b689d9ac");
            }

            /// <summary>
            /// Comments sitecolumn.
            /// </summary>
            public static class Comments
            {
                /// <summary>
                /// ID (Guid) of the comments sitecolumn.
                /// </summary>
                public static readonly Guid ID = new Guid("9da97a8a-1da5-4a77-98d3-4bc10456e700");
            }
        }

        /// <summary>
        /// Constants of CssClasses used by SharePoint 2010 publishing features
        /// </summary>
        public static class CssClasses
        {
            /// <summary>
            /// CssClasses used in default SharePoint 2010 Publishing masterpages
            /// </summary>
            public static class Masterpage
            {
                /// <summary>
                /// Default CssClass of the HTML-BODY-element
                /// </summary>
                public static readonly string Body = "v4master";
            }
        }
    }
}
