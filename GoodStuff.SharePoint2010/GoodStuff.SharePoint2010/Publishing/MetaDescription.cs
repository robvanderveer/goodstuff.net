﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using Microsoft.SharePoint;

namespace GoodStuff.SharePoint2010.Publishing
{
    /// <summary>
    /// Meta description tag based on SharePoint page (sitecolumn information). When not available a default value will be rendered (if configured by DefaultDescription property). If both are not available, no meta description tag will be rendered.
    /// </summary>
    /// <remarks>Meta description tags, while not important to search engine rankings, are extremely important in gaining user click-through from search engine result pages (SERPs). These short paragraphs are webmasters opportunity to advertise content to searchers and let them know exactly what the given page has with regard to what they’re looking for. The meta description should employ the keywords intelligently, but also create a compelling description that a searcher will want to click. Direct relevance to the page and uniqueness between each page’s meta description is key. The description should optimally be between 150-160 characters.</remarks>
    public class MetaDescription : HtmlMeta
    {
        /// <summary>
        /// Overriden OnLoad method from HtmlMeta baseclass
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.Name = "description";
            this.Content = this.GetMetaDescription();
        }

        /// <summary>
        /// Default description as fallback in case SharePoint page doesn't have a descriptions
        /// </summary>
        public string DefaultDescription { get; set; }

        /// <summary>
        /// Gets the MetaDescription to render. First checks the SharePointpage (method GetMetaDescriptionFromSharePointPage), and if not available falls back to the default (method GetMetaDescriptionDefault).
        /// </summary>
        /// <returns></returns>
        protected virtual string GetMetaDescription()
        {
            string description = null;

            //1) Get it from the SharePoint page
            //2) Get it from the property configured on the control (within markup/(master)pagelayout)
            //3) Use the coded default
            //4) Hide when non is available

            //1) From SharePoint
            description = this.GetMetaDescriptionFromSharePointPage();

            if (string.IsNullOrEmpty(description))
            {
                //2) Get the default description
                string defaultDescription = this.GetMetaDescriptionDefault();
                if (!string.IsNullOrEmpty(defaultDescription))
                {
                    description = defaultDescription;
                }
            }

            // Hide when non is available - check visibility
            this.Visible = string.IsNullOrEmpty(description) == false;

            return description;
        }

        /// <summary>
        /// Get the Metadescription based on the current SPListItem (page) within the current SPContext. Based on the Comments field of an default Publishing Page
        /// </summary>
        /// <returns></returns>
        protected virtual string GetMetaDescriptionFromSharePointPage()
        {
            return GetMetaDescriptionFromSharePointPage(Constants.SiteColumns.Comments.ID);
        }

        /// <summary>
        /// Get the Metadescription based on the current SPListItem (page) within the current SPContext. Based on the field specified
        /// </summary>
        /// <param name="spFieldID">The ID (Guid) of the field containing the meta description</param>
        /// <returns></returns>
        protected virtual string GetMetaDescriptionFromSharePointPage(Guid spFieldID)
        {
            if (SPContext.Current != null && SPContext.Current.ListItem != null)
            {
                SPListItem currentPage = SPContext.Current.ListItem;
                if (currentPage[spFieldID] != null)
                {
                    string descriptionValue = Convert.ToString(currentPage[spFieldID]);
                    if (string.IsNullOrEmpty(descriptionValue) == false)
                    {
                        return descriptionValue;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the default description (in case a meta description isn't configured on the SharePoint page). Can be overriden in case you want to use another default.
        /// </summary>
        /// <returns></returns>
        protected virtual string GetMetaDescriptionDefault()
        {
            return this.DefaultDescription;
        }
    }
}
