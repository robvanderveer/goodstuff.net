﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using System.Web;

namespace GoodStuff.SharePoint2010
{
    /// <summary>
    /// Wrapper around SharePoint's SPContext
    /// </summary>
    public static class SPContextGoodStuff
    {
        
        /// <summary>
        /// Returns a SPContext.Current only when the application is hosted by SharePoint (otherwise NULL)
        /// </summary>
        [CLSCompliant(false)]
        public static SPContext CurrentContextSafe
        {
            get
            {
                if(IsSharePointHosted)
                {
                     return SPContext.Current;
                }

                return null;
            }
        }

        /// <summary>
        /// Returns a boolean indicating whether the application is currently running in a SharePoint environment.
        /// </summary>
        public static bool IsSharePointHosted
        {
            get
            {
                if (HttpContext.Current != null)
                {
                    if (HttpContext.Current.Items["HttpHandlerSPWeb"] != null)
                    {
                        return true;
                    }
                }
                return false;
            }
        }        
    }    
}
