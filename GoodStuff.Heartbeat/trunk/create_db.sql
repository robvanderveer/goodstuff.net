/****** Object:  Table [dbo].[SensorReadings]    Script Date: 8-8-2011 16:38:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SensorReadings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SensorID] [int] NOT NULL,
	[Channel] [int] NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[Value] [float] NOT NULL,
	[Aggregated] [bit] NOT NULL,
 CONSTRAINT [PK_SensorReadings] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[SensorReadings] ADD  CONSTRAINT [DF_SensorReadings_Aggregated]  DEFAULT ((0)) FOR [Aggregated]
GO

