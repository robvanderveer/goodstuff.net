﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Heartbeat.Core
{
    public interface IComponent
    {
        string MachineName { get; }
        string Name { get; }

        IList<ISensor> Sensors { get; }
    }
}
