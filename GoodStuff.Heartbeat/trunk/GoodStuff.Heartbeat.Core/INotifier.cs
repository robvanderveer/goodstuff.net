﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Heartbeat.Core
{
    /// <summary>
    /// Implement this interface to have a failed monitor fire. Monitors are only GOOD, WARN or FAIL. The implementation of the notifier should decide
    /// to send (or resend) the notication. So the implementation of the notifier should contain state whether the component went DOWN or UP according to previous state.
    /// </summary>
    public interface INotifier
    {
        void Notify(IMonitor monitor);
    }
}
