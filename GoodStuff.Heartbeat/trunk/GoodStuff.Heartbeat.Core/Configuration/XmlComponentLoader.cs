﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace GoodStuff.Heartbeat.Core.Configuration
{
    public class XmlComponentLoader
    {
        public IComponent LoadComponent(XmlNode node)
        {
            string name = node.Attributes["name"].Value;
            Component newComponent = new Component(name);

            if (node.Attributes["machine"] != null)
            {
                string machineName = node.Attributes["machine"].Value;
                newComponent.MachineName = machineName;
            }

            XmlSensorLoader loader = new XmlSensorLoader(newComponent);

            foreach (XmlNode sensorNode in node.SelectSingleNode("sensors").ChildNodes)
            {
                ISensor newSensor = loader.LoadSensor(sensorNode);
                newComponent.Sensors.Add(newSensor);
            }

            return newComponent;
        }
    }
}
