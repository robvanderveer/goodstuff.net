﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace GoodStuff.Heartbeat.Core.Configuration
{
    public class XmlSensorLoader
    {
        private IComponent _component;

        public XmlSensorLoader(IComponent component)
        {
            _component = component;
        }

        public ISensor LoadSensor(XmlNode node)
        {
            switch (node.Name)
            {
                case "CPUSensor":
                    return new Sensors.CPUSensor(_component.MachineName);
                case "MemorySensor":
                    return new Sensors.MemorySensor(_component.MachineName);
                case "DriveSensor":
                    string driveName = node.Attributes["drive"].Value;

                    return new Sensors.DriveSensor(_component.MachineName, driveName);
                case "WebRequestSensor":
                    string requestUrl = node.Attributes["request"].Value;

                    return new Sensors.WebRequestSensor(requestUrl);
                case "Heartbeat":
                    return new Sensors.HeartbeatSensor();
                default:
                    throw new ArgumentOutOfRangeException("SensorType", node.Name, "sensor type not implemented");
            }
        }
    }
}
