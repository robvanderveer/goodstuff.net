﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace GoodStuff.Heartbeat.Core.Configuration
{
    public class XmlConfigurationLoader
    {
        public IList<IComponent> LoadConfiguration(XmlDocument doc)
        {
            XmlComponentLoader loader = new XmlComponentLoader();

            List<IComponent> components = new List<IComponent>();

            foreach (XmlNode componentNode in doc.SelectSingleNode("configuration/components").ChildNodes)
            {
                IComponent newComponent  = loader.LoadComponent(componentNode);
                components.Add(newComponent);
            }

            return components;
        }
    }
}
