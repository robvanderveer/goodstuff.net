﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Heartbeat.Core.Notifiers
{
    public class EmailNotifier : INotifier
    {
        private MonitorState _previousState = MonitorState.Healthy;

        public void Notify(IMonitor monitor)
        {
            //only notify the user if the monitors state is different from the previous one.
            if (monitor.State != _previousState)
            {
                switch (monitor.State)
                {
                    case MonitorState.Healthy:
                        Console.WriteLine("The component is healthy again");
                        break;
                    case MonitorState.Warning:
                        Console.WriteLine("The component has warnings.");
                        break;
                    case MonitorState.Failure:
                        Console.WriteLine("Component has failed.");
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("state", monitor.State, "unknown state");
                }

                _previousState = monitor.State;
            }
        }
    }
}
