﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoodStuff.Heartbeat.Core.Data;

namespace GoodStuff.Heartbeat.Core
{    
    [System.ServiceModel.ServiceContract()]
    public interface ISensorService 
    {
        [System.ServiceModel.OperationContract]
        IEnumerable<SensorReading> GetSensorReadings(int sensorID, int channelID, DateTime from, DateTime end, TimeSpan interval);

        [System.ServiceModel.OperationContract]
        IEnumerable<SensorChannelInfo> GetSensorInfo();
    }

    public class SensorChannelInfo
    {
        public string Name { get; set; }
        public int SensorID { get; set; }
        public int ChannelID { get; set; }
        public MonitorState State { get; set; }
    }
}
