﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Heartbeat.Core.Sensors
{
    public class MemorySensor : ISensor
    {
        private System.Diagnostics.PerformanceCounter availableMBytes;
        private SensorChannel _availableInfo;

        public MemorySensor(string machineName)
        {
            availableMBytes = new System.Diagnostics.PerformanceCounter("Memory", "Available MBytes", true);
            if (!string.IsNullOrEmpty(machineName))
            {
                availableMBytes.MachineName = machineName;
            }

            //var cat = new System.Diagnostics.PerformanceCounterCategory("Processor");
            //var counters = cat.GetInstanceNames();


            _availableInfo = new SensorChannel() { Description = "Free Memory", Units = "MB", DefaultLow = 150.0f, DefaultHigh = float.MaxValue };
        }

        public string Name
        {
            get { return "Free Memory"; }
        }

        public void Update()
        {
            _availableInfo.Value = availableMBytes.NextValue();
        }

        public IEnumerable<ISensorChannel> Values
        {
            get
            {
                yield return _availableInfo;
            }
        }
    }
}