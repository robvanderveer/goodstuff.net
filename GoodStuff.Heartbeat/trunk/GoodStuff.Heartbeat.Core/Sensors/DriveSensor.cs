﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace GoodStuff.Heartbeat.Core.Sensors
{
    public class DriveSensor : ISensor
    {
        private System.Diagnostics.PerformanceCounter _diskSpace;
        
        private SensorChannel _diskSpaceInfo;
        private string _drivename;

        public DriveSensor(string machineName, string drivename)
        {
            _drivename = drivename;
            _diskSpace = new System.Diagnostics.PerformanceCounter("LogicalDisk", "Free Megabytes", drivename, true);
            if (machineName != null)
            {
                _diskSpace.MachineName = machineName;
            }
            _diskSpaceInfo = new SensorChannel() { Description = "Free Megabytes", Units = "MB", DefaultLow = 500.0f, DefaultHigh = float.MaxValue };
        }

        public string Name
        {
            get { return "Disk health on " + _drivename; }
        }     

        public void Update()
        {
            _diskSpaceInfo.Value = _diskSpace.NextValue();
        }

        public IEnumerable<ISensorChannel> Values
        {
            get 
            {
                yield return _diskSpaceInfo;
            }
        }
    }
}
