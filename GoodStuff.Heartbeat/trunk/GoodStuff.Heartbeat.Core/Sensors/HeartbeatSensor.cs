﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Heartbeat.Core.Sensors
{
    /// <summary>
    /// A basic (default) sensor that always returns an OK value. This sensor is used to track the response of the system.
    /// </summary>
    public class HeartbeatSensor : ISensor
    {
        private SensorChannel _heartbeat;

        public HeartbeatSensor()
        {
            _heartbeat = new SensorChannel() { Description = "Heartbeat", Units = "bool", DefaultLow = 0, DefaultHigh = float.MaxValue, Value = 1.0f };
        }

        public string Name
        {
            get { return "Heartbeat"; }
        }         

        public void Update()
        {            
        }

        public IEnumerable<ISensorChannel> Values
        {
            get 
            {
                yield return _heartbeat;
            }
        }
    }
}
