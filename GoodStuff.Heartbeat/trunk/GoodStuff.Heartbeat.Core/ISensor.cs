﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Heartbeat.Core
{
    public interface ISensor
    {
        string Name { get; }

        void Update();

        IEnumerable<ISensorChannel> Values { get; }
    }
}
