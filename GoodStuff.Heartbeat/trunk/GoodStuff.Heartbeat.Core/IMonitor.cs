﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Heartbeat.Core
{
    /// <summary>
    /// A monitor can be set on a sensor value to track its values. It will take action when a counter value reaches a certain limit.
    /// A monitor is always a configuration item, but there can be several types of monitors (e.g. RunningAverage, NoMoreThan, etc.)
    /// Also, there can be multiple monitors for a single sensorvalue.
    /// </summary>
    public interface IMonitor
    {
        void Validate(ISensorChannel channel);
        MonitorState State { get; }
    }

    public enum MonitorState
    {
        Healthy,
        Warning,
        Failure
    }
}
