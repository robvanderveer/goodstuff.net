﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Heartbeat.Core
{
    /// <summary>
    /// Defines a configurable network component like a WebSite, SQL Server, etc. Each component has a selection of sensors that describe the health of the component.
    /// All the sensors for a component are logged and eventually aggregated (system wide).
    /// The component has default checking frequency that is inherited by the sensors. 
    /// A sensor has a reference to the component to retrieve the component configuration details like machinename, application instance and connection passwords.
    /// </summary>
    public class Component : IComponent
    {

        public Component(string name)
       {
            Sensors = new List<ISensor>();
            Name = name;
        }

        public string Name { get; private set; }

        public IList<ISensor> Sensors { get; private set; }

        public string MachineName { get; set; }
    }
}
