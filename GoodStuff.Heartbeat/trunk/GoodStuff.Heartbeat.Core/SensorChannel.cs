﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Heartbeat.Core
{
    public class SensorChannel : ISensorChannel
    {
        public string Description { get; set; }
        public float DefaultLow { get; set; }
        public float DefaultHigh { get; set; }
        public string Units { get; set; }
        public float Value { get; set; }
    }
}
