﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Heartbeat.Core
{
    /// <summary>
    /// Provides abstract access to sensor data
    /// </summary>
    public interface ISensorChannel
    {
        string Description { get; }
        float DefaultLow { get; }
        float DefaultHigh { get; }
        string Units { get; }
        float Value { get; }
    }
}
