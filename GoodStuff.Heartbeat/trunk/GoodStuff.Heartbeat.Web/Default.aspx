﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="GoodStuff.Heartbeat.Web._Default" %>
<%@ Register TagPrefix="gs" Namespace="GoodStuff.Web.Controls" Assembly="GoodStuff" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MijnVestia Status Monitor</title>
    <meta http-equiv="refresh" content="60">
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <p><a href="?span=1">1 hour</a> <a href="?span=2">2 hours</a> <a href="?span=24">1 day</a> <a href="?span=744">1 month</a></p>
           <asp:Chart ID="chart2" runat="server" RenderType="ImageTag" Height="200px" Visible="false"
            Width="800px" >
            <Series>
                <asp:Series ChartArea="ChartArea1" ChartType="Line" Name="Series1" 
                    XValueMember="TimeStamp" YValueMembers="Value" XValueType="Time" CustomProperties="EmptyPointValue=Zero">
                    <EmptyPointStyle Color="Red"  BorderWidth="2"  />
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1" AlignmentOrientation="All" 
                    AlignmentStyle="None">
                    <AxisY Title="Free MB localhost" IsLabelAutoFit="False">
                    </AxisY>
                    <AxisX IsLabelAutoFit="False">
                    </AxisX>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>

        <asp:PlaceHolder ID="graphs" runat="server" />
    </div>
    </form>
</body>
</html>
