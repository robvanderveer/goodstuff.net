# README #

This library is a collection of patterns and best practices learned while doing custom application development of a professional base. The classes have been developed according to the K.I.S.S. principle, because we have the feeling that a lot of existing libraries are bloated and over-engineered.

##Highlights##

* Exception logger, mailer and viewer (exceptions.axd)   GoodStuff.Diagnostics.ExceptionHandler 
* Template based messaging provider for sending emails with multilanguage support  GoodStuff.Providers.MessagingProvider 
* Builder pattern for working with Linq2Sql classes  GoodStuff.Data 
* Large library of web controls (styled buttons, validators, popups, captcha's, image handlers, etc.) 
* Simple ASP.Net robust data cache utility 
* Utility classes for retrieving strongly typed QueryString parameters 
* Extension methods for Dates, Enums, Linq etc. 
* Utility classes for reading web.config configuration sections 
* Provide proxy credentials in configuration with  AuthenticatingProxy 
* Extension methods for working with reflection and Attributes 
* And much more...