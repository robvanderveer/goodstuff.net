/****** Object:  Table [dbo].[LogEntries]    Script Date: 01/06/2011 10:35:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LogEntries](
	[ID] [uniqueidentifier] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Severity] [int] NOT NULL,
	[Module] [nvarchar](50) NOT NULL,
	[Message] [ntext] NOT NULL,
	[RemoteIP] [nvarchar](50) NOT NULL,
	[HostHeader] [nvarchar](150) NOT NULL,
	[Url] [nvarchar](150) NOT NULL,
	[QueryString] [nvarchar](50) NOT NULL,
	[StackTrace] [ntext] NOT NULL,
	[Application] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_LogEntry] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


