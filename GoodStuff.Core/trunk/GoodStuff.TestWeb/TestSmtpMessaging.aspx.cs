﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodStuff.TestWeb
{
    public partial class TestSmtpMessaging : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //perform a test mailmerge with inline images.
            var msg = GoodStuff.Providers.MessagingService.CreateMessage();
            msg.MessageClass = "testMessage";
            GoodStuff.Providers.MessagingService.SendMessage(msg);
        }
    }
}