﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestImageCropping.aspx.cs" Inherits="GoodStuff.TestWeb.TestImageCropping" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="jquery/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="jquery/jquery.Jcrop.min.js" type="text/javascript"></script>
    <link href="jquery/jquery.Jcrop.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Image ID="image1" runat="server" ImageUrl="~/images/1680x1050_Zixpk_HD_Wallpaper_209.jpg" />
        <gs:ImageCropping id="cropper1" runat="server" TargetImage="image1" AspectRatio="1.75" BoxWidth="600" BoxHeight="300" RequiredWidth="950" />
        <asp:Button ID="btnCrop" runat="server" />

        <hr />
        <p>Output:</p>
        <asp:Image ID="image2" runat="server" ImageUrl="~/images/output.png" />
    </div>
    </form>
</body>
</html>
