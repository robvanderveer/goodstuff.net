﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestCustomValidator.aspx.cs" Inherits="GoodStuff.TestWeb.TestCustomValidator" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <asp:Button ID="btnClick" runat="server" Text="Button"/>
        <asp:ValidationSummary ID="summary1" runat="server" />
        <hr />
        <asp:Button ID="btnGroup" runat="server" ValidationGroup="Group1" Text="Button ValidationGroup"/>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Group1" />
    </div>
    </form>
</body>
</html>
