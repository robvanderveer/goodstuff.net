﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestAutoPostback.aspx.cs" Inherits="GoodStuff.TestWeb.TestAutoPostback" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <gs:AutoSubmit ID="AutoSubmit1" runat="server" DelayMilliseconds="1000" />
    </div>
    </form>
</body>
</html>
