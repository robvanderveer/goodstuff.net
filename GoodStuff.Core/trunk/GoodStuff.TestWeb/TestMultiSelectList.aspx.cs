﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodStuff.TestWeb
{
    public partial class TestMultiSelectList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            button.Click += new EventHandler(button_Click);
        }

        void button_Click(object sender, EventArgs e)
        {
            Response.Write("Data Saved!");
            GoodStuff.Web.Controls.ModalPopup.Dismiss(this, true);
        }
    }
}
