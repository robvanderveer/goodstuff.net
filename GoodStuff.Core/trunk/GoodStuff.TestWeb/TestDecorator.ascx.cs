﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

namespace GoodStuff.TestWeb
{
    [ParseChildren(true)]
    public partial class TestDecorator : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            butWijzigen.Click += new EventHandler(butWijzigen_Click);
            butCancel.Click += new EventHandler(butCancel_Click);

            if (!this.IsPostBack)
            {
                mvViews.SetActiveView(vwNormal);
                
            }
        }

        void butCancel_Click(object sender, EventArgs e)
        {
            mvViews.SetActiveView(vwNormal);
        }

        void butWijzigen_Click(object sender, EventArgs e)
        {
            mvViews.SetActiveView(vwEdit);
        }

        public string Label
        {
            get { return lbLabel.Text; }
            set {
                lbLabel.Text = value;
                lbLabelEdit.Text = value;
            }
        }      
    }
}