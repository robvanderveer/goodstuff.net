﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodStuff.TestWeb
{
    public partial class TestPasswordInitial : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                txPassword.Text = "This is an initial string that is way longer than the text box.";                
            }

            btLogin.Click += new EventHandler(btLogin_Click);
            btTest.Click += new EventHandler(Button1_Click);
        }

        void Button1_Click(object sender, EventArgs e)
        {
            Response.Write(txPassword.Text);
        }

        void btLogin_Click(object sender, EventArgs e)
        {
            Response.Write(txPassword.Text);
            txPassword.Text = null;
        }
    }
}