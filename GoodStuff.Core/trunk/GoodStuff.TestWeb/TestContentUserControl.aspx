﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestContentUserControl.aspx.cs" Inherits="GoodStuff.TestWeb.TestContentUserControl" %>

<%@ Register src="TestDecorator.ascx" tagname="TestDecorator" tagprefix="uc1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
        <gs:DecoratorControl ID="TestDecorator1" runat="server" Template="~/TestDecorator.ascx">          
                <asp:TextBox ID="tbNaam" runat="server"/>
        </gs:DecoratorControl>
        <gs:DecoratorControl ID="TestDecorator2" runat="server" Template="~/TestDecorator.ascx">                    
               <asp:DropDownList ID="ddlCountry" runat="server" />
               <asp:RequiredFieldValidator ID="reqVal1" runat="server" ControlToValidate="ddlCountry" Text="Kies een land aub" />
        </gs:DecoratorControl>        
        </table>
    </div>
    </form>
</body>
</html>
