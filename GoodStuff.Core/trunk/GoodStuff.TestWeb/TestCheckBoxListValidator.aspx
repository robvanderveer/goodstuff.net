﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestCheckBoxListValidator.aspx.cs" Inherits="GoodStuff.TestWeb.TestCheckBoxListValidator" %>
<%@ Register TagPrefix="gs" Assembly="GoodStuff" Namespace="GoodStuff.Web.Controls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:CheckBoxList ID="CheckBoxList1" runat="server">
            <asp:ListItem Text="Aap" />
            <asp:ListItem Text="Noot" />
            <asp:ListItem Text="Mies" />
            <asp:ListItem Text="Wim" />
        </asp:CheckBoxList>
        
        <asp:CheckBoxList ID="CheckBoxList2" runat="server">
            <asp:ListItem Text="Aap" />
            <asp:ListItem Text="Noot" />
            <asp:ListItem Text="Mies" />
            <asp:ListItem Text="Wim" />
        </asp:CheckBoxList>
        
        <asp:TextBox ID="NotAListBox" runat="server" />
        
        <gs:CheckBoxListRequiredFieldValidator ID="validator" runat="server" ControlToValidate="CheckBoxList1" Text="No go" />
        
        <gs:CheckBoxListRequiredFieldValidator ID="CheckBoxListRequiredFieldValidator1" runat="server" ControlToValidate="NotAListBox" Text="No go without Javascript" EnableClientScript="false" />
        
        <asp:Button Text="Validate" runat="server" />
    </div>
    </form>
</body>
</html>
