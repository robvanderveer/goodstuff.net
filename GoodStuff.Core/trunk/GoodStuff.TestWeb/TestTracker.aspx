﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestTracker.aspx.cs" Inherits="GoodStuff.TestWeb.TestTracker" %>
<%@ Register TagPrefix="gs" Assembly="GoodStuff" Namespace="GoodStuff.Web.Controls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <gs:Trackers ID="MyTracker" runat="server" />
    </div>
    </form>
</body>
</html>
