﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TestDecorator.ascx.cs" Inherits="GoodStuff.TestWeb.TestDecorator" %>
<asp:MultiView ID="mvViews" runat="server" ActiveViewIndex="0" >
    <asp:View ID="vwNormal" runat="server">
        <tr>
            <td><asp:Label ID="lbLabel" runat="server"></asp:Label></td>
            <td><asp:Label ID="lbValue" runat="server"></asp:Label></td>
            <td><asp:LinkButton ID="butWijzigen" runat="server">wijzigen</asp:LinkButton></td>
        </tr>
    </asp:View>

    <asp:View ID="vwEdit" runat="server">
        <tr>
            <td><asp:Label ID="lbLabelEdit" runat="server"></asp:Label></td>
            <td><asp:PlaceHolder ID="Contents" runat="server" /></td>
            <td><asp:LinkButton ID="butCancel" runat="server" CausesValidation="false">annuleren</asp:LinkButton></td>
        </tr>    
        <tr>
            <td></td>
            <td><asp:Button ID="butOpslaan" runat="server" Text="Opslaan" /></td>
            <td></td>
        </tr>
    </asp:View>
</asp:MultiView>
