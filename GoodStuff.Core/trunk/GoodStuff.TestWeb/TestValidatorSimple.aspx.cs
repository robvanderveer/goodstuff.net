﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GoodStuff.Web.Controls.Validation;

namespace GoodStuff.TestWeb
{
    public partial class TestValidatorSimple : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            form1.SetupValidators(
                ValidationMessagePattern.AsText,
                new ValidateRequired() { Properties = new string[] { "Voornaam" }, Message = "Voornaam is verplicht" },
                new ValidateMaximumLength() { Properties = new string[] { "Voornaam" }, MaximumLength = 20 },
                new ValidateExpression() { Properties = new string[] { "Voornaam" }, Message = "Geen juiste email", Expression = ValidateExpression.Email },
                new ValidateRequired() { Properties = new string[] { "Achternaam" }, Message = "Achternaam verplicht" },
                new ValidateRequired() { Properties = new string[] { "Wachtwoord1" }, Message = "Wachtwoord is verplicht" },
                new ValidateRequired() { Properties = new string[] { "Wachtwoord2" }, Message = "Wachtwoord is verplicht" },
                new ValidateRequired() { Properties = new string[] { "Voorwaarden" }, Message = "U dient akkoord te gaan met de voorwaarden" },
                new ValidateRequired() { Properties = new string[] { "Opties" }, Message = "Kies ten minste 1 optie" },
                new ValidateRequired() { Properties = new string[] { "Keuzes" }, Message = "Verplicht" },
                new ValidateCompare() { Properties = new string[] { "Wachtwoord1", "Wachtwoord2" } , Message = "Komen niet overeen" }
                );

            submit.Click += new EventHandler(submit_Click);
        }

        void submit_Click(object sender, EventArgs e)
        {
            GoodStuff.Web.Controls.ModalPopup.Dismiss(this, true);
        }
    }
}