﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StyledButtonTest.aspx.cs" Inherits="GoodStuff.TestWeb.StyledButtonTest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style>
        /* REQUIRED BUTTON STYLES: */		
.submitBtn  
{   
    border: none;
    padding: 0px;
    margin: 0px;
    background: transparent url(images/btn_blue_sprite.gif) no-repeat right -140px; 
    line-height: 19px;
    text-decoration: none;       
}

.submitBtn span
{    
    background: transparent url(images/btn_blue_sprite.gif) no-repeat left 0px; 
    margin-right: 30px;
    padding-left: 30px;
    color: White;
    font-family: Arial;
    font-weight: bold;
    font-size: 10pt;
}

.submitBtn:hover span
{
    background-position: left -70px;
}

.submitBtn:hover 
{
    background-position: right -210px;
}


    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <gs:StyledLinkButton id="link1" CssClass="submitBtn" runat="server" Text="Bevestigen" />
        
        <gs:StyledLinkButton id="StyledLinkButton1" CssClass="submitBtn" runat="server" Text="iuewyriuew riuew re rew iure" />
        <br />
        
                <gs:StyledLinkButton id="StyledLinkButton2" CssClass="submitBtn" runat="server" Text="iuewyriuew riuew re rew iure" />
                        <gs:StyledLinkButton id="StyledLinkButton3" CssClass="submitBtn" runat="server" Text="iuewyriuew riuew re rew iure" />
                                <gs:StyledLinkButton id="StyledLinkButton4" CssClass="submitBtn" runat="server" Text="iuewyriuew riuew re rew iure" />
                                
                                <hr />
                                
                                <gs:StyledButton id="StyledLinkButton9" CssClass="submitBtn" runat="server" Text="Bevestigen" />
    </div>
    </form>
</body>
</html>
