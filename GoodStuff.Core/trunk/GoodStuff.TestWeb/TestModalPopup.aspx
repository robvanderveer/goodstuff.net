﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestModalPopup.aspx.cs" Inherits="GoodStuff.TestWeb.TestModalPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>       
        DIV.Modal > DIV.Popup
        {
            position: absolute;
            width: 100%;
            height: 100%;
            left: 0px; 
            top: 0px;
            text-align: center; 
            vertical-align: middle; 

        } 
        
        DIV.Modal > DIV.Popup > IFRAME
        {
            border: 1px solid black;
            margin-left: auto;
            margin-top: auto;
            width: 900px;
            height: 80%;
            margin-top: 100px;
        }
        
        DIV.Modal > DIV.Shadow 
        {
            left: 0px; 
            top: 0px; 
            position: absolute;
            background: black; 
            opacity: 0.2; 
            filter: alpha(opacity=20);
            -moz-opacity: 0.2;         
            width: 100%; 
            height: 100%;             
        }         
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <p>This is the page content. This is the page content.This is the page content.This is the page content.This is the page content.This is the page content</p>

        <asp:Button ID="button1" runat="server" />
        <asp:HyperLink NavigateUrl="#" ID="link2" runat="server" Text="click me" />
        <gs:ModalPopup id="ext1" runat="server" LinkID="link2" CssClass="Modal" PopupUrl="TestMultiSelectList.aspx" />


        <asp:HyperLink NavigateUrl="#" ID="HyperLink1" runat="server" Text="click me number 2" />
        <gs:ModalPopup id="ModalPopup1" runat="server" LinkID="HyperLink1" CssClass="Modal" PopupUrl="TestValidatorSimple.aspx" />
    </div>
    </form>
</body>
</html>
