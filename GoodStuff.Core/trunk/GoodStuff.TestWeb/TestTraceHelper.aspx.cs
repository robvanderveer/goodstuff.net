﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GoodStuff.Diagnostics;

namespace GoodStuff.TestWeb
{
    public partial class TestTraceHelper : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (var helper = TraceHelper.Push())
            {
                Method1("aap");
            }
        }

        private void Method1(string p)
        {
            using (var helper = TraceHelper.Push())
            {
                try
                {
                    throw new NotImplementedException();
                }
                catch
                {
                    Trace.Write(TraceHelper.GetInfo());
                }
            }
            Trace.Write(TraceHelper.GetInfo());
        }
    }
}