﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GoodStuff.Security;

namespace GoodStuff.TestWeb
{
    public partial class TestFloodGate : System.Web.UI.Page
    {
        private static FloodGate _floodGate = new FloodGate(TimeSpan.FromSeconds(10), 3, TimeSpan.FromSeconds(60), p => new Exception("Too many clicks: " + p));

        protected void Page_Load(object sender, EventArgs e)
        {
            ClickMe.Click += new EventHandler(ClickMe_Click);
        }

        void ClickMe_Click(object sender, EventArgs e)
        {
            _floodGate.Assert();

            Response.Write("Ok");
        }
    }
}