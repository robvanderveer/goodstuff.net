﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodStuff.TestWeb
{
    public class TestRedirector : IHttpModule, GoodStuff.Web.IRedirectProvider
    {
        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        public void Init(HttpApplication context)
        {
            GoodStuff.Web.LegacyRedirectModule.AddProvider(this);
        }


        public string DetermineRedirect(string virtualPath)
        {
            if (virtualPath.ToLower().StartsWith("~/index.aspx?id=4"))
            {
                return "~/TestMultiSelectList.aspx?id=5";
            }
            return null;
        }
    }
}
