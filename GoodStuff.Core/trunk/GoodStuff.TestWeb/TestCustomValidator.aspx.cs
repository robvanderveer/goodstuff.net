﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GoodStuff.Web.Controls;

namespace GoodStuff.TestWeb
{
    public partial class TestCustomValidator : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            btnClick.Click += new EventHandler(btnClick_Click);
            btnGroup.Click += new EventHandler(btnGroup_Click);
        }

        void btnGroup_Click(object sender, EventArgs e)
        {
            Page.Validate("Group1");
            this.Page.Validators.Add(new CustomValidationError("Fatal group") { ValidationGroup = "Group1" });
        }

        void btnClick_Click(object sender, EventArgs e)
        {
            this.Page.Validators.Add(new CustomValidationError("Fatal")  );
        }
    }
}