﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodStuff.TestWeb
{
    public partial class TestDynamicForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                //Formuliertje.DataItem = new TestFormData() { Name = "Rob", Email = "rob@driebier.net" };
                Formuliertje.DataBind();
            }
        }

        public class TestFormData
        {
            public string Name { get; set; }
            public string Email { get; set; }
        }
    }    
}