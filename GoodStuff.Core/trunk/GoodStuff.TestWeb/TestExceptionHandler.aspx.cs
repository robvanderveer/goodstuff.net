﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GoodStuff.Diagnostics;

namespace GoodStuff.TestWeb
{
    public partial class TestExceptionHandler : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Exception testException = new ArgumentNullException("testparam");
            testException.AddData("param", 430);
            ExceptionHandlerModule.LogError(testException);

            try
            {
                DemoClasses.FakeException.FakeIt();
            }
            catch (Exception vex)
            {
                vex.AddData(() => sender);
                vex.AddData(() => e);
                vex.AddData("Demo", "waarde");
                throw new Exception("Foutje...bedankt!", vex);
            }

            
        }
    }
}