﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GoodStuff.Web;

namespace GoodStuff.TestWeb
{
    public partial class TestStateServer : System.Web.UI.Page
    {
        //scope is INSTANCE
        private static StateServer<VestiaState> stateService = new StateServer<VestiaState>() { Timeout = TimeSpan.FromSeconds(60) }; 

        protected void Page_Load(object sender, EventArgs e)
        {
            Update.Click += new EventHandler(Update_Click);

            VestiaState state2 = stateService.Get();
            if (state2 != null)
            {
                ltValue.Text = state2.UserName;
            }            
        }

        void Update_Click(object sender, EventArgs e)
        {
            VestiaState state2 = stateService.Get();
            if (state2 == null)
            {
                state2 = new VestiaState();
            }
            state2.UserName = tbNaam.Text;
            stateService.Set(state2);

            ltValue.Text = state2.UserName;

            tbNaam.Text = null;
        }

        public class VestiaState
        {
            public string UserName { get; set; }
        }
    }
}