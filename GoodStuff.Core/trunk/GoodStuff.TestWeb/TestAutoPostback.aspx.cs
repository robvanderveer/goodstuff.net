﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodStuff.TestWeb
{
    public partial class TestAutoPostback : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AutoSubmit1.OnSubmit += new EventHandler(AutoSubmit1_OnSubmit);
        }

        void AutoSubmit1_OnSubmit(object sender, EventArgs e)
        {
            Response.Write(DateTime.Now.ToString());
            AutoSubmit1.Visible = false;
        }
    }
}
