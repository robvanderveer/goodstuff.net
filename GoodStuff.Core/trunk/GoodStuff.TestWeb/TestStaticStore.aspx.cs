﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GoodStuff.Web.Controls;

namespace GoodStuff.TestWeb
{
    public partial class TestStaticStore : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Guid? oldKey = ViewState.GetValue<Guid?>("myKey");
            if (oldKey != null)
            {
                string value = GoodStuff.StaticState.Retrieve<string>(oldKey.Value);
                Response.Write(value);
            }

            string naam = "rob" + DateTime.Now.ToString();

            var key = GoodStuff.StaticState.Store(naam, TimeSpan.FromSeconds(10));
            ViewState["myKey"] = key;
        }        
    }
}