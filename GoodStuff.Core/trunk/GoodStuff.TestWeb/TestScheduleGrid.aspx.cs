﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodStuff.TestWeb
{
    public partial class TestScheduleGrid : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            IDictionary<string, IEnumerable<IDateRange>> dict1 = new Dictionary<string, IEnumerable<IDateRange>>();
            dict1.Add("Piet", new List<IDateRange>() { new DateRange(DateTime.Now, DateTime.Now.AddHours(4)) });
            dict1.Add("Jan", new List<IDateRange>() 
            { 
                new DateRange(DateTime.Now.AddHours(6), DateTime.Now.AddHours(8)),
                new DateRange(DateTime.Now.AddHours(7), DateTime.Now.AddHours(10))  //<-- overlap
            });
            dict1.Add("Klaas", new List<IDateRange>() 
            { 
                new DateRange(DateTime.Now.AddHours(3), DateTime.Now.AddHours(5)),
                new DateRange(DateTime.Now.AddHours(5), DateTime.Now.AddHours(7))  //<-- concats, same size.
            });
            dict1.Add("ExactSlot", new List<IDateRange>() 
            { 
                new DateRange(DateTime.Today.AddHours(8), DateTime.Today.AddHours(12))
            });
            dict1.Add("ExactSlot2", new List<IDateRange>() 
            { 
                new DateRange(DateTime.Today.AddHours(12), DateTime.Today.AddHours(16))
            });
            dict1.Add("ExactSlot3", new List<IDateRange>() 
            { 
                new DateRange(DateTime.Today.AddHours(8), DateTime.Today.AddHours(16))
            });
            dict1.Add("TooLongForTheDay", new List<IDateRange>() 
            { 
                new DateRange(DateTime.Today.AddHours(8), DateTime.Today.AddHours(28))
            });
            dict1.Add("AfterHours", new List<IDateRange>() 
            { 
                new DateRange(DateTime.Today.AddHours(22), DateTime.Today.AddHours(23))
            });

            grid1.Events = dict1;
            grid1.DataBind();
        }
    }
}