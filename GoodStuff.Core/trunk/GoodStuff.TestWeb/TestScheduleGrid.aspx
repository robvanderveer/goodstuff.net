﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestScheduleGrid.aspx.cs" Inherits="GoodStuff.TestWeb.TestScheduleGrid" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        TABLE.ScheduleGrid { border-collapse: collapse; font-size: 10px; }
        TABLE.ScheduleGrid .HeaderDate TD { border: 1px solid black; background: #E0E0E0; text-align: center }
        TABLE.ScheduleGrid .HeaderTime TD { border: 1px solid black; background: #E0E0E0; text-align: left; width: 30px;}
        TABLE.ScheduleGrid .Row TD.Label { border: 1px solid black; background: #E0E0E0; text-align: left; }
        TABLE.ScheduleGrid .Row TD { border: 1px solid #c0c0c0; background: #F0F0F0; height: 30px; }
        TABLE.ScheduleGrid .Row TD.StartDay { border-left: 1px solid black; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <gs:ScheduleGrid runat="server" ID="grid1"/>
    </div>
    </form>
</body>
</html>
