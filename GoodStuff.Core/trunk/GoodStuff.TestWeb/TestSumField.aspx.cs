﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodStuff.TestWeb
{
    public partial class TestSumField : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<Dummy> list = new List<Dummy>();
            list.Add(new Dummy { ItemCount = 6 });
            list.Add(new Dummy { ItemCount = 3 });
            list.Add(new Dummy { ItemCount = 8 });
            list.Add(new Dummy { ItemCount = 24 });
            list.Add(new Dummy { ItemCount = 1 });
            list.Add(new Dummy { ItemCount = 8 });

            //sum = 50, count = 6, average = 8,33

            gvMyGrid.DataSource = list;
            gvMyGrid.DataBind();
        }

        public class Dummy
        {
            public decimal ItemCount { get; set; }
        }
    }
}