﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestValidatorSimple.aspx.cs" Inherits="GoodStuff.TestWeb.TestValidatorSimple" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:TextBox ID="tbVoornaam" runat="server"/><gs:AutoValidator runat="server" Key="Voornaam" ControlToValidate="tbVoornaam" /><br />

        <asp:CheckBox ID="cbAkkoord" runat="server" /><gs:AutoValidator ID="AutoValidator3" runat="server" Key="Voorwaarden" ControlToValidate="cbAkkoord" /><br />
        <asp:CheckBoxList ID="cbOpties" runat="server">
            <asp:ListItem>Een</asp:ListItem>
            <asp:ListItem>Twee</asp:ListItem>
            <asp:ListItem>Drie</asp:ListItem>
        </asp:CheckBoxList>
        
        <gs:AutoValidator ID="AutoValidator4" runat="server" Key="Opties" ControlToValidate="cbOpties" /><br />
        <asp:DropDownList ID="ddlKeuzes" runat="server">
            <asp:ListItem Value="">--maak een keuze--</asp:ListItem>
                <asp:ListItem>Een</asp:ListItem>
            <asp:ListItem>Twee</asp:ListItem>
            <asp:ListItem>Drie</asp:ListItem>
        
        </asp:DropDownList>
        <gs:AutoValidator ID="AutoValidator5" runat="server" Key="Keuzes" ControlToValidate="ddlKeuzes" /><br />


        <asp:TextBox ID="tbAchternaam" runat="server"/><gs:AutoValidator runat="server" Key="Achternaam" ControlToValidate="tbAchternaam" /><br />
        <hr />

        <asp:TextBox ID="tbWachtwoord1" runat="server"/><gs:AutoValidator ID="AutoValidator1" runat="server" Key="Wachtwoord1" ControlToValidate="tbWachtwoord1" /><br />
        <asp:TextBox ID="tbWachtwoord2" runat="server"/><gs:AutoValidator ID="AutoValidator2" runat="server" Key="Wachtwoord2" ControlToValidate="tbWachtwoord2" /><br />

        <asp:Button ID="submit" runat="server" Text="Submit" />

        <asp:ValidationSummary runat="server" />
    </div>
    </form>
</body>
</html>
