﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodStuff.TestWeb
{
    public partial class TestValidEmailDNS : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            btnVerify.Click += new EventHandler(btnVerify_Click);
        }

        void btnVerify_Click(object sender, EventArgs e)
        {
            lblResult.Text = GoodStuff.Net.EmailHelper.Verify(txtEmail.Text).ToString();
        }
    }
}