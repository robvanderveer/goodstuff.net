﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodStuff.TestWeb
{
    public partial class ConfigSectionDemo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DemoClasses.MyConfiguration config = ConfigurationHelper.GetSection<DemoClasses.MyConfiguration>();
            ParameterValue.Text = config.Name;


            int x = ConfigurationHelper.GetSetting<int>("Nummertje");

            bool y = ConfigurationHelper.GetSetting<bool>("Switch", false);
            TestEnum t = ConfigurationHelper.GetSetting<TestEnum>("Enum", TestEnum.Two);
        }
    }

    public enum TestEnum 
    {
        One,
        Two
    }

}
