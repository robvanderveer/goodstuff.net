﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestBoxAutoGrow.aspx.cs"
    Inherits="GoodStuff.TestWeb.TextBoxAutoGrow" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <gs:TextBoxAutoGrow runat="server" Width="400" MaxHeight="250" RowsToGrow="3" />
    </div>
    </form>
</body>
</html>
