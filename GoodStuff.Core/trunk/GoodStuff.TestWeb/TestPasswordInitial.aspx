﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestPasswordInitial.aspx.cs" Inherits="GoodStuff.TestWeb.TestPasswordInitial" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <gs:SecureTextBox id="txPassword" runat="server"/>
     <asp:RequiredFieldValidator ID="mpmp" runat="server" ControlToValidate="txPassword" ErrorMessage="Wachtwoord is verplicht" />
        <asp:Button ID="btLogin" runat="server" Text="Test" />
        <asp:Button ID="btTest" runat="server" Text="No reset, just an error" />
    </div>
    </form>
</body>
</html>
