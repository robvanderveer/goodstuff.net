﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodStuff.TestWeb
{
    public partial class TestContentUserControl : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                //DropDownList _ddlCountry = (DropDownList)TestDecorator2.FindControl("ddlCountry");
                //_ddlCountry.DataSource = new string[] { "Nederland", "Engeland", "Frankrijk", "Anders" };
                //_ddlCountry.DataBind();

                ddlCountry.DataSource = new string[] { "Nederland", "Engeland", "Frankrijk", "Anders" };
                ddlCountry.DataBind();
                ddlCountry.Items.Insert(0, new ListItem("Kies een land", ""));
            }
        }
    }
}