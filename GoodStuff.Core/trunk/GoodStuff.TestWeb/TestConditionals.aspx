﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestConditionals.aspx.cs" Inherits="GoodStuff.TestWeb.TestConditionals" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <gs:ConditionalLinkButton ID="Conditional1" runat="server" Text="Text Content" />

        <gs:ConditionalLinkButton ID="Conditional2" runat="server">Inner Contents</gs:ConditionalLinkButton>

        <gs:ConditionalHyperLink ID="Conditional3" runat="server" Text="Text Content" NavigateUrl="http://www.google.nl" />
        <gs:ConditionalHyperLink ID="Conditional4" runat="server" NavigateUrl="http://www.google.nl">Inner contents</gs:ConditionalHyperLink>

        <asp:CheckBox ID="cbEnabled" runat="server" AutoPostBack="true" />
    </div>
    </form>
</body>
</html>
