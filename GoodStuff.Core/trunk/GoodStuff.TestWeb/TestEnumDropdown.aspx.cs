﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodStuff.TestWeb
{
    public partial class TestEnumDropdown : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                enumDropDownList.DataSource = Enum<DataBoundControlMode>.GetValues();
                enumDropDownList.DataBind();

                bullet.DataSource = Enum<DataBoundControlMode>.GetValues();
                bullet.DataBind();
                rblTest.DataSource = Enum<DataBoundControlMode>.GetValues();
                rblTest.DataBind();
            
            }

            TheButton.Click += new EventHandler(TheButton_Click);
        }

        void TheButton_Click(object sender, EventArgs e)
        {
            
        }
    }
}
