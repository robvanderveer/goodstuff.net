﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodStuff.TestWeb
{
    public partial class TestConditionals : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            cbEnabled.CheckedChanged += new EventHandler(cbEnabled_CheckedChanged);
        }

        void cbEnabled_CheckedChanged(object sender, EventArgs e)
        {
            Conditional1.Enabled = cbEnabled.Checked;
            Conditional2.Enabled = cbEnabled.Checked;
            Conditional3.Enabled = cbEnabled.Checked;
            Conditional4.Enabled = cbEnabled.Checked;
        }
    }
}