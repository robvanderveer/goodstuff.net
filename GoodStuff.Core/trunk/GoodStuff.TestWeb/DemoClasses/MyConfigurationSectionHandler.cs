﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodStuff.TestWeb.DemoClasses
{
    public class MyConfigurationSectionHandler : GoodStuff.ConfigurationSectionHandler<MyConfiguration>
    {
    }

    public class MyConfiguration
    {
        public string Name { get; set; }
        public SubConfiguration Sub { get; set; }
    }

    public class SubConfiguration
    {
        public string MyProperty { get; set; }
    }
}
