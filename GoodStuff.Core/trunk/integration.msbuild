<?xml version="1.0" encoding="utf-8"?>
<Project DefaultTargets="Build"  xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
	<Import Project="$(MSBuildExtensionsPath)\MSBuildCommunityTasks\MSBuild.Community.Tasks.Targets"/>
  <!--<Import Project="$(MSBuildExtensionsPath)\ExtensionPack\MSBuild.ExtensionPack.tasks"/>-->

  <!--instead of CALLING msbuild with this project we could include the sln file? -->

  <!-- some documentation:
    The default target for this script is 'BUILD', which in essence will build the
    project and run all unit tests in the solution.
    
    Nothing is done with the output by default.
    
    
    - To rebuild the database:
        msbuild integration.msbuild /t:RebuildDatabase /p:DBServer=YOURSERVER;UserName=DBUSER;Password=DBPASS
        - this will restore the databse using properties below
        - this will roll-over the database scripts to the database (Not Yet Implemented)
  -->
  
  <!-- PROJECT-SPECIFIC CONFIGURABLE PARAMETERS 
  **********************************************************************
  **********************************************************************
  -->

  <PropertyGroup>
    <SolutionName>GoodStuff.sln</SolutionName>
    
    <Configuration>Debug</Configuration>
    <Release>1</Release>
    <MinorVersion>9</MinorVersion>
  </PropertyGroup>

  <PropertyGroup Condition="'$(CCNetLabel)' == ''">
    <CCNetLabel>0</CCNetLabel>
  </PropertyGroup>

  <ItemGroup>
    <UnitTests Include="**/bin/$(Configuration)/*.UnitTests.dll"/>
    <UnitTestResults Include="TestResult.xml"/>
  </ItemGroup>

  <!-- THIS CONCLUDES ALL SETTINGS. 
  **********************************************************************
  **********************************************************************
  -->

  <Target Name="Build" DependsOnTargets="Clean;UpdateSolutionInfo;BuildSolution;CopyOutput">
    <Message Text="Integration of $(SolutionName) completed."/>
  </Target>

  <Target Name="Package" DependsOnTargets="Clean;UpdateSolutionInfo;BuildCore;Documentation">
    <CreateProperty Value="Release_$(Version)">
      <Output TaskParameter="Value" PropertyName="ReleaseFolder"/>
    </CreateProperty>

    <CreateItem Include="GoodStuff\bin\$(Configuration)\*.dll;GoodStuff\bin\$(Configuration)\*.pdb;GoodStuff\bin\$(Configuration)\*.xml;Help\GoodStuff.chm">
      <Output TaskParameter="Include" ItemName="FilesToCopy"/>
    </CreateItem>

    <CreateItem Include="Help\**">
      <Output TaskParameter="Include" ItemName="DocumentationFiles"/>
    </CreateItem>


    <!--<Message Text="Copying %(FilesToCopy.Identity)"/>-->

    <RemoveDir Directories="$(ReleaseFolder)" ContinueOnError="true"/>
    <MakeDir Directories="$(ReleaseFolder)"/>

    <Copy SourceFiles="@(FilesToCopy)" DestinationFolder="$(ReleaseFolder)\%(FilesToCopy.RecursiveDir)"></Copy>
    <Zip ZipFileName="GoodStuff_$(Version).zip" WorkingDirectory="$(ReleaseFolder)" Files="@(FilesToCopy)" />
    
    <Message Text="Packaging of $(SolutionName) $(Version) completed."/>


  </Target>

  <!-- reusable targets below *********************************** -->
  
  <Target Name="Clean">
    <!-- delete the Nunit test results before starting.-->
    <Delete Files="@(UnitTestResults)"/>
    
  </Target>

  <!-- retrieves the HEAD version of the current working folder -->
  <Target Name="GetVersion">      
    <SvnVersion LocalPath="."
                ToolPath="C:\Program Files (x86)\Subversion\bin">      
      <Output TaskParameter="Revision" PropertyName="Revision" />
    </SvnVersion>
    <Message Text="Svn Revision: $(Revision)"/>
    <CreateProperty Value="$(Release).$(MinorVersion).$(CCNetLabel).$(Revision)">
      <Output TaskParameter="Value" PropertyName="Version" />

    </CreateProperty>
  </Target>

  <!-- Updates the solution info with the current revision -->
  <Target Name="UpdateSolutionInfo" DependsOnTargets="GetVersion">
    <AssemblyInfo CodeLanguage="CS"
                OutputFile="$(MSBuildProjectDirectory)\SolutionInfo.cs"
                AssemblyDescription="GoodStuff"
                AssemblyConfiguration=""
                AssemblyCompany="Rob van der Veer"
                AssemblyProduct="GoodStuff"
                AssemblyCopyright="Copyright (c) Rob van der Veer 2014"
                AssemblyTrademark=""
                ComVisible="false"
                CLSCompliant="true"
                AssemblyVersion="$(Version)" />
  </Target>

  <Target Name="BuildCore">
    <MSBuild Projects="GoodStuff\GoodStuff.csproj" Targets="Clean;Build" Properties="Configuration=$(Configuration)"/>
  </Target>
  
  <Target Name="BuildSolution">
    <MSBuild Projects="$(SolutionName)" Targets="Clean;Build" Properties="Configuration=$(Configuration)"/>    
  </Target>

  <Target Name="Documentation" Inputs="GoodStuff.shfbproj;GoodStuff\bin\$(Configuration)\GoodStuff.dll" Outputs="Help\GoodStuff.chm" DependsOnTargets="BuildCore">
    <MSBuild Projects="GoodStuff.shfbproj" Properties="Configuration=$(Configuration)"/>
  </Target>

  <Target Name="PerformUnitTests" DependsOnTargets="CodeCoverage">                      
    <!--<NUnit 
      ToolPath="C:\Program Files\NUnit 2.5.2\bin\net-2.0" 
      DisableShadowCopy="true"
      Assemblies="@(UnitTests)" />>-->
  </Target>

  <Target Name="CodeCoverage">
    <Exec Command="&quot;C:\Program Files\NCover\ncover.console.exe&quot; //reg //a GoodStuff &quot;C:\Program Files\NUnit 2.5.2\bin\net-2.0\nunit-console.exe&quot; /noshadow &quot;GoodStuff.UnitTests/bin/$(Configuration)/GoodStuff.UnitTests.dll&quot;"/>
    <Message Text="Coverage completed"/>
    <Exec Command="&quot;C:\Program Files\NCoverExplorer\NCoverExplorer.Console.exe&quot; coverage.xml /xml /r:ModuleClassSummary /m:80 /p:GoodStuff"/>
    <Message Text="Coverage Report completed"/>
  </Target>

  <Target Name="DeployWebApplication" Condition="'$(WebApplication)' != ''">
    <Message Text="Cleaning"/>
    <Delete Files="$(WebApplicationDrop)/*.*"/>
    
    <Message Text="Deploying web applications: @(WebApplication)"/>
    <MSBuild Projects="$(WebApplication)" Targets="ResolveReferences;_CopyWebApplication" Properties="OutDir=$(WebApplicationDrop)bin\;WebProjectOutputDir=$(WebApplicationDrop)\;Configuration=$(Configuration)"/>
    
  </Target>

  <Target Name="CopyOutput" DependsOnTargets="GetVersion;DeployWebApplication;Documentation">
    <CreateProperty Value="c:\inetpub\GoodStuff\Downloads\Release_$(Version)">
      <Output TaskParameter="Value" PropertyName="ReleaseFolder"/>
    </CreateProperty>
    <CreateProperty Value="c:\inetpub\GoodStuff\Downloads\Latest">
      <Output TaskParameter="Value" PropertyName="Latest"/>
    </CreateProperty>

    <CreateItem Include="$(WebApplicationDrop)\**" Condition="'$(WebApplicationDrop)' != ''">
      <Output TaskParameter="Include" ItemName="FilesToCopy"/>
    </CreateItem>

    <CreateItem Include="GoodStuff\bin\$(Configuration)\*.dll;GoodStuff.SharePoint2007\bin\$(Configuration)\GoodStuff.SharePoint2007.dll;Help\GoodStuff.chm">
      <Output TaskParameter="Include" ItemName="FilesToCopy"/>
    </CreateItem>

    <CreateItem Include="Help\**">
      <Output TaskParameter="Include" ItemName="DocumentationFiles"/>
    </CreateItem>

    
    <!--<Message Text="Copying %(FilesToCopy.Identity)"/>-->

    <RemoveDir Directories="$(Latest)" ContinueOnError="true"/>
    <MakeDir Directories="$(ReleaseFolder)"/>
    <MakeDir Directories="$(Latest)"/>

    <Copy SourceFiles="@(FilesToCopy)" DestinationFolder="$(ReleaseFolder)\%(FilesToCopy.RecursiveDir)"></Copy>
    <Copy SourceFiles="@(FilesToCopy)" DestinationFolder="$(Latest)\%(FilesToCopy.RecursiveDir)"></Copy>

    <Copy SourceFiles="@(DocumentationFiles)" DestinationFolder="$(Latest)\Help\%(DocumentationFiles.RecursiveDir)"></Copy>
  </Target>
  
  <!-- DEPLOYMENT TASKS ************************* 
  
  DISABLED
  <Target Name="BackupDatabase">
    for security reasons, the properties must be passed to the build script
    <Error Text=" The DBServer property must be set on the command line."
           Condition="'$(DBServer)' == ''" />

    <MSBuild.ExtensionPack.Sql2008.Database TaskAction="Backup" DatabaseItem="@(DeploymentDatabase)" MachineName="$(DBServer)" DataFilePath="@(DeploymentDatabaseBackupLocation)"  Username="$(UserName)" UserPassword="$(Password)"/>

  </Target>
  
  <Target Name="RebuildDatabase">
    this will restore the database as configured by the DatabaseBackup property 
    
    for security reasons, the properties must be passed to the build script
    <Error Text=" The DBServer property must be set on the command line."
           Condition="'$(DBServer)' == ''" />

    <MSBuild.ExtensionPack.Sql2008.Database TaskAction="Restore" DatabaseItem="@(DeploymentDatabase)" MachineName="$(DBServer)" DataFilePath="@(DeploymentDatabaseBackupLocation)" Username="$(UserName)" UserPassword="$(Password)"/>


  </Target>
  -->
</Project>