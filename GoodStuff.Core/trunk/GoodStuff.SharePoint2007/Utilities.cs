﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoodStuff.SharePoint2007
{
    public class Utilities
    {
        /// <summary>
        /// Regular expression to isolate a Root Url ("$1") or Server Relative Url ("$2")
        /// </summary>
        /// <example>
        ///    string Url, RootUrl, ServerRelativeUrl;
        ///    if (Util.rexUrlRootRelative.Match(Url).Success)
        ///    {
        ///        RootUrl = Util.rexUrlRootRelative.Match(Url).Result("$1");
        ///        ServerRelativeUrl = Util.rexUrlRootRelative.Match(Url).Result("$2");
        ///    }
        /// </example>
        public static System.Text.RegularExpressions.Regex rexUrlRootRelative =
        new System.Text.RegularExpressions.Regex(
        @"(http://[^/]*){0,1}(/.*){0,1}$",
        System.Text.RegularExpressions.RegexOptions.Compiled);

    }
}
