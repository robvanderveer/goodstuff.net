﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.WebPartPages;

namespace GoodStuff.SharePoint2007
{
    /// <summary>
    /// Toolpart for picking a list
    /// </summary>
    public class ListpickerToolpart : BaseBrowseItemToolpart<Microsoft.SharePoint.WebPartPages.WebPart>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="parentWebPart"></param>
        /// <param name="itemPath"></param>
        /// <param name="itemName"></param>
        public ListpickerToolpart(IListConnected parentWebPart, string itemPath, string itemName)
            : base(parentWebPart as WebPart, itemPath, itemName)
        { }

        /// <summary>
        /// When the user clicks "Save" or "Apply", this function is triggered. Function returns inserted values into werbpart.
        /// </summary>
        public override void ApplyChanges()
        {
            IListConnected x = WebPart as IListConnected;
            if (x != null)
            {
                x.ListName = this.txtItemName.Text;
                x.ListWebPath = this.txtItemPath.Text;
            }
            else
            {
                throw new GoodStuffSharePoint2007Exception("The ListpickerToolpart can only be used on WebParts that implements IListConnected such as the BaseListConnectedWebPart.");
            }
        }

        /// <summary>
        /// Show Lists only
        /// </summary>
        public override BrowseItemToolpartFilter Filter
        {
            get { return BrowseItemToolpartFilter.websLists; }
        }

        /// <summary>
        /// Text on the button
        /// </summary>
        public override string ButtonText
        {
            get { return "Selecteer lijst"; }
        }

        /// <summary>
        /// Title of the toolpart
        /// </summary>
        public override string ToolpartTitle
        {
            get { return "Lijstselectie"; }
        }
    }
}
