﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.SharePoint2007.WebParts
{
    /// <summary>
    /// Experimental class
    /// </summary>
    [Obsolete("Experimental!")]
    public class PagePickerToolPart : BaseBrowseItemToolpart<BaseWebPart>
    {
        public PagePickerToolPart(BaseWebPart webPart, string itemPath, string itemName)
            : base(webPart, itemPath, itemName)
        { }

        public override string ButtonText
        {
            get { return "Selecteer pagina"; }
        }

        public override string  ToolpartTitle
        {
            get { return "Selecteer pagina voor dit WebPart"; }
        }

        public override void  ApplyChanges()
        {
 	        throw new NotImplementedException();
        }

        public override BrowseItemToolpartFilter Filter
        {
            get { return BrowseItemToolpartFilter.pages; }
        }
    }
}
