﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.SharePoint2007
{
    /// <summary>
    /// IListConnected interface which should be used in combination with webparts only
    /// </summary>
    public interface IListConnected
    {
        /// <summary>
        /// The serverrelative url to the web containing the item
        /// </summary>
        string ListWebPath { get; set; }

        /// <summary>
        /// The relative (from it's parentweb) url to the item 
        /// </summary>
        string ListName { get; set; }
    }
}
