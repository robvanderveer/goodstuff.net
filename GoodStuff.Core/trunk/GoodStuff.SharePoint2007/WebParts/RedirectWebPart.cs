﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls.WebParts;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using Microsoft.SharePoint.WebPartPages;
using System.Web.UI.WebControls;
using Microsoft.SharePoint;

namespace GoodStuff.SharePoint2007
{    
    /// <summary>
    /// Basic redirect WebPart which (when configured as active) will redirect all the users that are not allowed to edit the page to the configured url.
    /// </summary>
    public class RedirectWebPart : Microsoft.SharePoint.WebPartPages.WebPart
    {
        /// <summary>
        /// RedirectUrl
        /// </summary>
        /// <remarks>Virtual so that property can be overridden to enable change of attributes</remarks>
        [Personalizable(PersonalizationScope.Shared), WebBrowsable(true), WebDisplayName("RedirectUrl"), WebDescription("Redirect URL (incl http://)")]
        [Category("Eigenschappen")]
        public virtual string RedirectUrl { get; set; }

        /// <summary>
        /// Only redirects when active
        /// </summary>
        /// <remarks>Virtual so that property can be overridden to enable change of attributes</remarks>
        [Personalizable(PersonalizationScope.Shared), WebBrowsable(true), WebDisplayName("Active?"), WebDescription("WebPart will only redirect when active")]
        [Category("Eigenschappen")]
        public virtual bool IsActive { get; set; }

        /// <summary>
        /// Override of default CreateChildControls()
        /// </summary>
        protected override void CreateChildControls()
        {
            //First some validation, that we want to perform directly (so not optional by authors only) to ensure the person who is/was configuring
            //this WebPart will see this error
            
            bool abortRedirect = false;
            if (SPContext.Current.Item is SPListItem)
            {
                //Could be that some non publishing pages in the root of the sitecollection contain this WebPart, 
                //in that case, SPContext.Current.ListItem is NULL
                abortRedirect = SPContext.Current.ListItem.DoesUserHavePermissions(SPBasePermissions.EditListItems);
            }
            else
            {
                //Pagedesign allowed?
                if (this.WebPartManager != null && this.WebPartManager.DisplayMode.AllowPageDesign)
                {
                    abortRedirect = true;
                }
            }

            if (abortRedirect
                    && this.IsActive 
                    && string.IsNullOrEmpty(this.RedirectUrl))
            {
                //WebPart is active, but no url has been configured
                this.Controls.Add(new LiteralControl("RedirectWebPart is set to active, but no url to redirect to has been configured"));
            }

            base.CreateChildControls();

            this.Controls.Clear();

            if (this.IsActive)
            {
                if (abortRedirect == false)
                {
                    if (!string.IsNullOrEmpty(this.RedirectUrl))
                    {
                        this.Page.Response.Redirect(this.RedirectUrl);
                    }
                }
                else
                {
                    this.Controls.Add(new LiteralControl(string.Format(this.MessageNoRedirectFormatString, this.RedirectUrl)));
                    this.Controls.Add(new LiteralControl(" Link: "));
                    this.Controls.Add(new HyperLink
                    {
                        NavigateUrl = this.RedirectUrl,
                        Text = this.RedirectUrl
                    });
                }
            }
        }

        /// <summary>
        /// Message that will be shown when somebody with edit-page rights will load the page containing this RedirectWebPart, that somebody will not be
        /// redirected by this WebPart. Override to specify your own (translated) message. You have to specify {0} as placeholder for the redirecturl.
        /// </summary>
        public virtual string MessageNoRedirectFormatString
        {
            get
            {
                return "Je zit nu in de beheermodus, er vindt geen redirect naar '{0}' plaats. ";
            }
        }

        /// <summary>
        /// The name of the custom properties-category. When configured, this category will be expanded by default when WebPart goed into edit-mode.
        /// Override it to specify your own name, which should be equal to the category attribute of the properties of this (or your overriden) WebPart.
        /// </summary>
        /// <remarks>When NULL, no category will be expanded.</remarks>
        public virtual string CategoryName
        {
            get
            {
                return "Eigenschappen";
            }
        }

        /// <summary>
        /// Override toolpart, return null to ensure no webparts properties are shown
        /// </summary>
        /// <returns></returns>
        public override ToolPart[] GetToolParts()
        {
            List<ToolPart> toolparts = new List<ToolPart>();
            CustomPropertyToolPart custPart = new CustomPropertyToolPart();
            if (!string.IsNullOrEmpty(this.CategoryName))
            {
                custPart.Expand(this.CategoryName);
            }
            toolparts.Add(custPart);
            return toolparts.ToArray();
        }
    }
}
