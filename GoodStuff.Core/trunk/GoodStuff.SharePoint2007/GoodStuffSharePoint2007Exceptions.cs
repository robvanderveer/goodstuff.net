﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;

namespace GoodStuff.SharePoint2007
{
    public class GoodStuffSharePoint2007Exception : Exception
    {
        public GoodStuffSharePoint2007Exception(string message)
            : base(message)
        { }

        public GoodStuffSharePoint2007Exception(string message, Exception inner)
            : base(message, inner)
        { }
    }

    public class GoodStuffSharePoint2007SPQueryException : GoodStuffSharePoint2007Exception
    {
        public GoodStuffSharePoint2007SPQueryException(string message, SPQuery query, Exception inner)
            : base(message, inner)
        {
            this.Query = query;
        }

        /// <summary>
        /// The Query that is used when Exception was throwns
        /// </summary>
        public SPQuery Query { get; set; }

        /// <summary>
        /// Override the string presentation of the base Exception to append the query
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            // Get the (optional) query. Maintain local within this method to only call the getter once, querybuilding
            // in childclass can be expensive.
            SPQuery query = this.Query;

            if (query != null && !string.IsNullOrEmpty(query.Query))
            {
                return string.Concat(base.ToString(), " Query: ", System.Web.HttpUtility.HtmlEncode(query.Query));
            }
            else
            {
                return base.ToString();
            }
        }
    }
}
