﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using System.Web;

namespace GoodStuff.SharePoint2007
{
    public static class SPContextExtensions
    {
        /// <summary>
        /// Returns a SPContext.Current only when the application is hosted by SharePoint
        /// </summary>
        public static SPContext SafeCurrent
        {
            get
            {
                if(IsSharePointHosted)
                {
                     return SPContext.Current;
                }

                return null;
            }
        }

        /// <summary>
        /// Returns a value whether the application is currently running in a SharePoint environment.
        /// </summary>
        public static bool IsSharePointHosted
        {
            get
            {
                if (HttpContext.Current != null)
                {
                    if (HttpContext.Current.Items["HttpHandlerSPWeb"] != null)
                    {
                        return true;
                    }
                }
                return false;
            }
        }        
    }    
}
