﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.SharePoint;

namespace GoodStuff.SharePoint2007
{
    /// <summary>
    /// Extensions for a SPList object
    /// </summary>
    public static class SPListExtensions
    {
        /// <summary>
        /// Check whether a list with the given name exists in the given web
        /// </summary>
        /// <param name="web">The SPWeb to search in</param>
        /// <param name="listName">The name of the list (case-insentive)</param>
        /// <returns></returns>
        public static bool ListExists(this SPWeb web, string listName)
        {
            return web.Lists.ListExists(listName);
        }

        /// <summary>
        /// Check whether a list with the given name exists in the given listcollection
        /// </summary>
        /// <param name="lists">The SPListCollection to search in</param>
        /// <param name="listName">The name of the list (case-insentive)</param>
        /// <returns></returns>
        public static bool ListExists(this SPListCollection lists, string listName)
        {
            return lists.Cast<SPList>().Any(list => string.Compare(list.Title, listName, true) == 0);
        }

        /// <summary>
        /// Creates a SPFieldLookup for the given list
        /// </summary>
        /// <param name="list">list itself</param>
        /// <param name="fieldName">Name of the column to add as lookupcolumn</param>
        /// <param name="lookupListID">GUID of the list to be looked up</param>
        /// <param name="required">required field?</param>
        /// <param name="allowMultipleValues">multiple values?</param>
        /// <returns></returns>
        public static SPFieldLookup CreateLookupField(this SPList list, string fieldName, Guid lookupListID, bool required, bool allowMultipleValues)
        {
            return CreateLookupField(list, fieldName, lookupListID, required, allowMultipleValues, null);
        }
        /// <summary>
        /// Creates a SPFieldLookup for the given list
        /// </summary>
        /// <param name="list">list itself</param>
        /// <param name="fieldName">Name of the column to add as lookupcolumn</param>
        /// <param name="lookupListID">GUID of the list to be looked up</param>
        /// <param name="required">required field?</param>
        /// <param name="allowMultipleValues">multiple values?</param>
        /// <param name="lookupField">name of the field to use as displaytext</param>
        /// <returns></returns>
        public static SPFieldLookup CreateLookupField(this SPList list, string fieldName, Guid lookupListID, bool required, bool allowMultipleValues, string displayField)
        {
            list.Fields.AddLookup(fieldName, lookupListID, required);
            SPFieldLookup lookup = (SPFieldLookup)list.Fields[fieldName];
            lookup.AllowMultipleValues = allowMultipleValues;

            if (!string.IsNullOrEmpty(displayField))
            {
                lookup.LookupField = displayField;
            }

            lookup.Update(true);
            list.Update();
            return lookup;
        }
    }
}
