﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;

namespace GoodStuff.SharePoint2007
{
    /// <summary>
    /// Base usercontrol that can handle a collection of ListItems
    /// </summary>
    public abstract class BaseListConnectedUserControl : BaseUserControl
    {
        /// <summary>
        /// The method ensures that the retrieved listitems must be handled by the child usercontrol.
        /// </summary>
        /// <param name="listItems"></param>
        public abstract void HandleListItems(SPListItemCollection listItems);
    }
}
