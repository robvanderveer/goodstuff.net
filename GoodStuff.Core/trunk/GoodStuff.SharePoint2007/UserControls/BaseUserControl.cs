﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace GoodStuff.SharePoint2007
{
    /// <summary>
    /// Base Usercontrol
    /// </summary>
    public class BaseUserControl : UserControl
    {
        /// <summary>
        /// Property for a displaytitle, so the configurable WebPart title can be passed throught to this usercontrol
        /// </summary>
        public string DisplayTitle { get; set; }
    }
}
