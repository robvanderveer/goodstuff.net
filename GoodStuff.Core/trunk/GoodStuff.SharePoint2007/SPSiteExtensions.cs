﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.SharePoint;

namespace GoodStuff.SharePoint2007
{
    public static class SPSiteExtensions
    {
        /// <summary>
        /// Check whether a SPWeb with the given name exists in the given SPSite
        /// </summary>
        /// <param name="siteCollection">The SPSite to search in</param>
        /// <param name="webName">The name of the SPWeb (case-insentive)</param>
        /// <returns></returns>
        public static bool WebExists(this SPSite siteCollection, string webName)
        {
            return siteCollection.AllWebs.WebExists(webName);
        }

        /// <summary>
        /// Add the given webpart file to the sitecollection. Existing webparts will be overwritten.
        /// </summary>
        /// <param name="siteCollection">The sitecollection to add the WebPart to. Will not be disposed by this extension.</param>
        /// <param name="pathToWebPartFile">full path to the webpartfile. When used in a FeatureReceiver, combine the filename with the 'properties.Definition.RootDirectory' property of the feature.</param>
        public static void AddWebPartFileToWebPartGallery(this SPSite siteCollection, string pathToWebPartFile)
        {
            AddWebPartFileToWebPartGallery(siteCollection, pathToWebPartFile, null);
        }

        /// <summary>
        /// Add the given webpart file to the sitecollection. Existing webparts will be overwritten.
        /// </summary>
        /// <param name="siteCollection">The sitecollection to add the WebPart to. Will not be disposed by this extension.</param>
        /// <param name="pathToWebPartFile">full path to the webpartfile. When used in a FeatureReceiver, combine the filename with the 'properties.Definition.RootDirectory' property of the feature.</param>
        /// <param name="groupName">the name of the group to add the webpart to</param>
        public static void AddWebPartFileToWebPartGallery(this SPSite siteCollection, string pathToWebPartFile, string groupName)
        {
            pathToWebPartFile = pathToWebPartFile.ToLower();

            if (pathToWebPartFile.EndsWith(".webpart") == false && pathToWebPartFile.EndsWith(".dwp") == false)
            {
                throw new System.NotSupportedException("Only .webpart and .dwp files can be added as WebPart. Filename: " + System.IO.Path.GetFileName(pathToWebPartFile));
            }

            SPList ctg_WebParts = siteCollection.GetCatalog(SPListTemplateType.WebPartCatalog);
            FileInfo fileInfo = new FileInfo(pathToWebPartFile);
            using (FileStream stream = fileInfo.Open(FileMode.OpenOrCreate, FileAccess.Read))
            {
                SPFolder webpartFolder = ctg_WebParts.RootFolder;
                string webPartFileName = System.IO.Path.GetFileName(pathToWebPartFile);
                SPFile webpartFile = webpartFolder.Files.Add(webPartFileName, stream, true);
                if (!string.IsNullOrEmpty(groupName))
                {
                    webpartFile.Properties["Group"] = groupName;
                }
                webpartFile.Update();
            }
        }
    }
}
