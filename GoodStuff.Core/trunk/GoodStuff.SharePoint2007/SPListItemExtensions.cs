﻿using System;
using System.Collections.Generic;
using System.Linq;
using GoodStuff.Text;

using Microsoft.SharePoint;

namespace GoodStuff.SharePoint2007
{
    /// <summary>
    /// Extensions for SPListItems
    /// </summary>
    public static class SPListItemExtensions
    {
        #region SPLookup Extensions

        /* http://sharepointdevwiki.com/display/public/Setting+and+Getting+field+values+of+type+SPFieldUser%2C+SPFieldUrl+and+SPFieldLookup+using+extension+methods 
            
            Reads the value of a SPFieldLookup field
            string value = item.GetFieldValueLookup("LookupFieldName");
 
            Read lookup values of a SPFieldLookup field 
            with multiple values allowed
            IEnumerable<string> values = item.GetFieldValueLookupCollection("MultiLookupFieldName");
 
            Set the value of a SPFieldLookup field
            item.SetFieldValueLookup("LookupFieldName", "LookupValue");
 
            Set the value of a SPFieldLookup field with multiple values allowed
            string[] values = {"Hamburg", "London" };
            item.SetFieldValueLookup("MultiLookupFieldName", values);
         */

        /// <summary>
        /// Returns the value of a Lookup Field.
        /// </summary>
        private static string GetFieldValueLookup(this SPListItem item,
            string fieldName)
        {
            if (item != null)
            {
                SPFieldLookupValue lookupValue =
                    new SPFieldLookupValue(item[fieldName] as string);
                return lookupValue.LookupValue;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Returns the value of a Lookup-Field with multiple values.
        /// </summary>
        public static IEnumerable<string> GetFieldValueLookupCollection(
            this SPListItem item, string fieldName)
        {
            List<string> result = new List<string>();
            if (item != null)
            {
                SPFieldLookupValueCollection values =
                    item[fieldName] as SPFieldLookupValueCollection;

                foreach (SPFieldLookupValue value in values)
                {
                    result.Add(value.LookupValue);
                }
            }
            return result;
        }

        /// <summary>
        /// Returns the SPFieldLookupValue instance of a lookup value. 
        /// The ID value will be obtained using SPQuery.
        /// </summary>
        private static SPFieldLookupValue GetLookupValue(
            SPWeb web, SPFieldLookup field, string lookupValue)
        {
            string queryFormat =
                @"<Where>
            <Eq>
                <FieldRef Name='{0}' />
                <Value Type='Text'>{1}</Value>
            </Eq>
          </Where>";

            string queryText =
                string.Format(queryFormat, field.LookupField, lookupValue);
            SPList lookupList = web.Lists[new Guid(field.LookupList)];

            SPListItemCollection lookupItems =
                lookupList.GetItems(new SPQuery() { Query = queryText });

            if (lookupItems.Count > 0)
            {
                int lookupId =
                    Convert.ToInt32(lookupItems[0][SPBuiltInFieldId.ID]);

                return new SPFieldLookupValue(lookupId, lookupValue);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Sets the value of a Lookup-Field.
        /// </summary>
        public static void SetFieldValueLookup(
            this SPListItem item, string fieldName, string lookupValue)
        {
            if (item != null)
            {
                SPFieldLookup field =
                    item.Fields.GetField(fieldName) as SPFieldLookup;
                item[fieldName] = GetLookupValue(item.Web, field, lookupValue);
            }
            else
            {
                item[fieldName] = null;
            }
        }

        /// <summary>
        /// Set the values of a Lookup-Field with multiple values allowed.
        /// </summary>
        public static void SetFieldValueLookup(this SPListItem item,
            string fieldName, IEnumerable<string> lookupValues)
        {
            if (item != null)
            {
                SPFieldLookup field =
                    item.Fields.GetField(fieldName) as SPFieldLookup;

                SPFieldLookupValueCollection fieldValues =
                    new SPFieldLookupValueCollection();

                foreach (string lookupValue in lookupValues)
                {
                    fieldValues.Add(
                        GetLookupValue(item.Web, field, lookupValue));
                }
                item[fieldName] = fieldValues;
            }
        }

        #endregion

        /// <summary>
        /// Creates a serverrelative url to the default dispform of a listitem or to the page itself
        /// </summary>
        /// <param name="item">The listitem to create the url for</param>
        /// <param name="isPage">Is a page (true) or a regular listitem (false)</param>
        /// <returns>a serverrelative url to the listitem</returns>
        public static string GetUrl(this SPListItem item, bool isPage)
        {
            if (isPage)
            {
                string webUr = item.Web.ServerRelativeUrl.EnsureEndWithSlash();
                return string.Concat(webUr, item.Url);
            }
            else
            {
                SPList list = item.ParentList;
                SPWeb web = list.ParentWeb;

                string webUrl = web.ServerRelativeUrl.EnsureEndWithSlash();
                string dispUrl = list.Forms[PAGETYPE.PAGE_DISPLAYFORM].Url;
                dispUrl = String.Format("{0}{1}?ID={2}", webUrl, dispUrl, item.ID);

                return dispUrl;
            }
        }
    }
}
