﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GoodStuff.TestMvc.Controllers
{
    public class JSONController : ApiController
    {
        // GET api/json
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/json/5
        public string Get(int id)
        {
            throw new NotImplementedException("DEMO - Not implemented");

            return "value";
        }

        // POST api/json
        public void Post([FromBody]string value)
        {
        }

        // PUT api/json/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/json/5
        public void Delete(int id)
        {
        }
    }
}
