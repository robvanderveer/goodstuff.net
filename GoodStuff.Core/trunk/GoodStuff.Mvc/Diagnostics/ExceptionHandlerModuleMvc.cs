﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;

namespace GoodStuff.Diagnostics
{
    /// <summary>
    /// This module extends the GoodStuff.Diagnotics.ExceptionHandlerModule with addition support for MVC4 applications
    /// 
    /// It adds both a global ExceptionFilter and a global HandleError that both inject logging to the GoodStuff.Diagnostics tool stack.
    /// To properly register this module, you need to add proper sections to the HttpModule section of system.web, or module to System.WebServer
    /// 
    /// </summary>
    public class ExceptionHandlerModuleMvc : IHttpModule
    {
        private static bool __initialized;

        public void Init(HttpApplication context)
        {
            lock (typeof(ExceptionHandlerModuleMvc))
            {
                if (!__initialized)
                {
                    GlobalFilters.Filters.Add(new LoggingErrorHandlerAttribute());
                    GlobalConfiguration.Configuration.Filters.Add(new LoggingExceptionFilterAttribute());

                    __initialized = true;
                }
            }
        }

        public void Dispose()
        {
        }
    }
}
