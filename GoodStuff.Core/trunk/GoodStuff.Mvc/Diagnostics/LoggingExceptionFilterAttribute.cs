﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;

namespace GoodStuff.Diagnostics
{
    /// <summary>
    /// Custom ExceptionFilterAttribute that logs controller and action details to the GoodStuff log handler.
    /// To register, see ExceptionHandlerModuleMvc class.
    /// </summary>
   
    public class LoggingExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            //add extra details to the exception.

            var actionArguments = actionExecutedContext.ActionContext.ActionArguments;
            //var actionName = (string)actionExecutedContext.ActionContext.Request.RouteData.Values["action"];
            if (actionArguments != null)
            {
                foreach (var id in actionArguments.Keys)
                {
                    actionExecutedContext.Exception.AddData("ActionArguments " + id, actionArguments[id].ToString());
                }
            }
 
            GoodStuff.Diagnostics.ExceptionHandlerModule.LogError(actionExecutedContext.Exception);
            base.OnException(actionExecutedContext);
        }
    }
}