﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq.Expressions;
using System.ComponentModel;

namespace GoodStuff.Silverlight
{
    public class NotifyPropertyChangedBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }
            if (PropertyChanged != null)
            {
                Execute.RunOnUIThread(() => { PropertyChanged(this, new PropertyChangedEventArgs(name)); });
            }
        }

        protected void RaisePropertyChanged<TValue>(Expression<Func<TValue>> propertySelector)
        {
            Raise(propertySelector);
        }

        private void Raise<TValue>(Expression<Func<TValue>> propertySelector)
        {

            if (PropertyChanged != null)
            {
                var memberExpression = propertySelector.Body as MemberExpression;
                if (memberExpression != null)
                {
                    var _sender = ((ConstantExpression)memberExpression.Expression).Value;
                    PropertyChanged(_sender, new PropertyChangedEventArgs(memberExpression.Member.Name));
                }
            }

        }
    }

}
