﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq.Expressions;

namespace GoodStuff.Silverlight
{
    public static class ObjectExtensions
    {
        public static string NameOf<T>(this T target, Expression<Func<T, object>> propertyExpression)
        {
            MemberExpression body = null;

            if (propertyExpression.Body is UnaryExpression)
            {
                var unary = propertyExpression.Body as UnaryExpression;
                if (unary.Operand is MemberExpression)
                {
                    body = unary.Operand as MemberExpression;
                }
            }
            else
            {
                if (propertyExpression.Body is MemberExpression)
                {
                    body = propertyExpression.Body as MemberExpression;
                }
            }

            if (body == null)
            {
                throw new ArgumentException("'propertyExpression' should be a member expression");
            }

            // Extract the right part (after "=>")  
            var vmExpression = body.Expression as ConstantExpression;

            // Extract the name of the property to raise a change on  
            return body.Member.Name;
        }
    }
}
