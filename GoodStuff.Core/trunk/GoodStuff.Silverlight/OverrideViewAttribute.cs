﻿using System;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace GoodStuff.Silverlight
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class OverrideViewAttribute : Attribute
    {
        public OverrideViewAttribute()
	    {

        }

        public Type View { get; set; }
    }
}
