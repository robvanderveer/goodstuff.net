﻿//using System;
//using System.Net;
//using System.Windows;
//using System.Windows.Controls;
//using System.Windows.Documents;
//using System.Windows.Ink;
//using System.Windows.Input;
//using System.Windows.Media;
//using System.Windows.Media.Animation;
//using System.Windows.Shapes;
//using System.Collections.Generic;
//using System.Reflection;

//namespace GoodStuff.Silverlight
//{
//    public class DelegateCommand : ICommand
//    {
//        private Func<object,object> _execute;
//        private PropertyInfo _canExecuteProperty;
//        private object _model;

//        public DelegateCommand(object model, Func<object,object> execute, string canExecuteProp)
//        {
//            //PropertyInfo
//            _model = model;
//            _execute = execute;
//            if (canExecuteProp != null)
//            {
//                _canExecuteProperty = model.GetType().GetProperty(canExecuteProp);
//            }
//        }

//        public DelegateCommand(object model, Action<object> execute, string canExecuteProp):
//            this(model, p => { execute(p); return null; }, canExecuteProp)
//        {
//        }

//        public bool CanExecute(object parameter)
//        {
//            if (_canExecuteProperty != null)
//            {
//                return (bool)_canExecuteProperty.GetValue(_model, null);
//            }
//            return true;
//        }

//        public event EventHandler CanExecuteChanged;

//        public void Execute(object parameter)
//        {
//            object returnValue = _execute(parameter);
//            if (returnValue != null)
//            {
//                HandleReturnValue(returnValue);
//            };
//        }

//        private void HandleReturnValue(object returnValue)
//        {
//            //Make sure it is IEnumerable<Result>
//            if (returnValue is IResult)
//            {
//                returnValue = new IResult[] { (IResult)returnValue };
//            }

//            if (returnValue is IEnumerable<IResult>)
//            {
//                new ResultEnumerator((IEnumerable<IResult>)returnValue).Enumerate();
//            }
//        }
//    }
//}
