﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace GoodStuff.Silverlight
{
    public abstract class BootStrapperBase
    {
        public void Setup()
        {
            Execute.InitializeWithDispatcher();

            IShell shell = CreateShell();
            ModelLocator.Register<IShell>(shell);

            FrameworkElement view = ViewLocator.Locate(shell as ViewModelBase);
            ViewModelBinder.Bind(shell as ViewModelBase, view);

            Application.Current.RootVisual = view;
        }

        protected abstract IShell CreateShell();
    }
}
