﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace GoodStuff.Silverlight
{
    public class ModelLocator
    {
        private static Dictionary<Type, object> _instances = new Dictionary<Type,object>();

        /// <summary>
        /// PROTOTYPE
        /// Seeks up the hierarchy to find a viewModel of the given type
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public static ModelType FindParent<ModelType>(FrameworkElement control) where ModelType : ViewModelBase
        {
            while (control != null && !(control.DataContext is ModelType))
            {
                control = (FrameworkElement)VisualTreeHelper.GetParent(control);
            }
            return (ModelType)control.DataContext;
        }

        public static ModelType Resolve<ModelType>()
        {
            if(_instances.ContainsKey(typeof(ModelType)))
            {
                return (ModelType)_instances[typeof(ModelType)];
            }
            return default(ModelType);
        }

        public static void Register<ModelType>(ModelType t)
        {
            _instances.Add(typeof(ModelType), t);
        }
    }
}
