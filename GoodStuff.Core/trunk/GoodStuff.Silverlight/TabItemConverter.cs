﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Collections.Generic;
using System.Windows.Markup;
using System.Collections.ObjectModel;

namespace GoodStuff.Silverlight
{
    public class TabItemConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            IEnumerable<IHeaderedContent> source = value as IEnumerable<IHeaderedContent>;
            if (source != null)
            {
                ObservableCollection<TabItem> result = new ObservableCollection<TabItem>();
                foreach (IHeaderedContent tab in source)
                {
                    //dynamically create a header - templating does not seem to work.
                    result.Add(Convert(tab));
                }

                if (source is ObservableCollection<IHeaderedContent>)
                {
                    ObservableCollection<IHeaderedContent> observer = (ObservableCollection<IHeaderedContent>)source;
                    observer.CollectionChanged += (s, e) =>
                    {
                        switch (e.Action)
                        {
                            case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                                foreach (IHeaderedContent newItem in e.NewItems)
                                {
                                    result.Add(Convert(newItem));
                                }

                                break;
                            case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                                for (int i = 0; i < e.OldItems.Count; i++ )
                                {
                                    result.RemoveAt(e.OldStartingIndex);
                                }
                                break;
                            default:
                                throw new NotSupportedException("Only add & remove are supported for now");
                        }
                    };
                }

                return result;
            }
            return null;
        }

        void observer_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {            
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private TabItem Convert(IHeaderedContent tab)
        {
            //dynamically create a header - templating does not seem to work. TODO: Move to dynamicTemplate or ModelLocator.

            object head = XamlReader.Load(
           "<Grid xmlns='http://schemas.microsoft.com/winfx/2006/xaml/presentation' " +
                          "xmlns:x='http://schemas.microsoft.com/winfx/2006/xaml'>" +
               "<Grid.ColumnDefinitions><ColumnDefinition Width='Auto'/><ColumnDefinition Width='Auto'/></Grid.ColumnDefinitions><TextBlock x:Name=\"Title\"/><Border x:Name='CanClose' Grid.Column='1' HorizontalAlignment=\"Right\" ><HyperlinkButton x:Name=\"Close\" Style=\"{StaticResource TabCloseButtonStyle}\" Content=\"X\"/></Border>" +
           "</Grid>");

            TabItem newTabItem = new TabItem();
            newTabItem.Header = head;
            newTabItem.IsSelected = true;
            newTabItem.DataContext = tab;
            newTabItem.Content = ViewLocator.Locate(tab as ViewModelBase);
            newTabItem.Style = Application.Current.Resources["AutoTabItemStyle"] as Style;

            ViewModelBinder.Bind(tab as ViewModelBase, newTabItem.Header as DependencyObject);
            ViewModelBinder.Bind(tab as ViewModelBase, newTabItem.Content as DependencyObject);

            return newTabItem;
        }
    }
}
