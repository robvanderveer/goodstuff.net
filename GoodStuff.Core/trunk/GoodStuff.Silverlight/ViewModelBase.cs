﻿using System;
using System.Net;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Windows.Threading;
using System.Windows;
using System.Threading;

namespace GoodStuff.Silverlight
{
    /// <summary>
    /// Base class for ViewModels, supports INotifyPropertyChanged and strongly-type RaiseChanged methods.
    /// </summary>        
    [Obsolete("Please use the base class NotifyPropertyChangedBase")]
    public abstract class ViewModelBase : NotifyPropertyChangedBase
    {        
    }
}
