﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GoodStuff.Silverlight
{    
    /// <remarks>
    /// Courtesy of Rob Eisenberg
    /// http://live.visitmix.com/MIX10/Sessions/EX15
    /// </remarks>
    public static class Execute
    {
        private static Dispatcher _dispatcher;

        public static void InitializeWithDispatcher()
        {
            //http://www.codeproject.com/tips/61869/Get-the-Silverlight-Main-Dispatcher-before-the-Roo.aspx
            _dispatcher = Deployment.Current.Dispatcher;
        }

        public static void RunOnUIThread(Action a)
        {
            _dispatcher.BeginInvoke(a);
        }
    }
}
