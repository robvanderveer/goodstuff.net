﻿using System;
using GoodStuff.Text;
using NUnit.Framework;

namespace GoodStuff.UnitTests
{
    [TestFixture]
    public class DaysAgoFormatterTest
    {
        [Test]
        public void SetSecondsAgo()
        {
            DateTime date = DateTime.Now.AddSeconds(-1.9);
            Assert.AreEqual("2 seconds", date.ToAge());
        }

        [Test]
        public void SetAlmostAMinuteAgo()
        {
            DateTime date = DateTime.Now.AddSeconds(-59);
            Assert.AreEqual("Less than a minute", date.ToAge());
        }

        [Test]
        public void SetAMinuteAgo()
        {
            DateTime date = DateTime.Now.AddSeconds(-67);
            Assert.AreEqual("About a minute", date.ToAge());
        }

        [Test]
        public void Set2MinutesAgo()
        {
            DateTime date = DateTime.Now.AddSeconds(-120);
            Assert.AreEqual("2 minutes", date.ToAge());
        }

        [Test]
        public void SetManyMinutesAgo()
        {
            DateTime date = DateTime.Now.AddMinutes(-59);
            Assert.AreEqual("Almost an hour", date.ToAge());
        }

        [Test]
        public void SetHoursAgo()
        {
            DateTime date = DateTime.Now.AddHours(-2);
            Assert.AreEqual("2 hours", date.ToAge());
        }

        [Test]
        public void SetOneWeek()
        {
            DateTime date = DateTime.Now.AddDays(-8);
            Assert.AreEqual("1 week", date.ToAge());
        }

        [Test]
        public void SetMoreWeek()
        {
            DateTime date = DateTime.Now.AddDays(-14);
            Assert.AreEqual("2 weeks", date.ToAge());
        }
    }
}
