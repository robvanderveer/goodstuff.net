﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace GoodStuff.UnitTests
{
    [TestFixture]
    public class ContainerTest : ITestInterface
    {
        [Test]
        public void TestConfigureFromConfig()
        {
            Container container = new Container();
            container.Register<ITestInterface>(c => c.InjectFromConfiguration<ITestInterface>());

            ITestInterface instance = container.Resolve<ITestInterface>();
        }

        [Test]
        public void TestConfigureFromConfigSimple()
        {
            Container container = new Container();
            container.RegisterFromConfiguration<ITestInterface>();

            ITestInterface instance = container.Resolve<ITestInterface>();
        }


       
    }

    public interface ITestInterface
    {
    }
}
