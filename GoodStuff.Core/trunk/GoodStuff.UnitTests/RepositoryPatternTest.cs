﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoodStuff.Data;
using System.Data.Linq;

namespace GoodStuff.UnitTests
{
    public class RepositoryPatternTest
    {
        public void TestTransactionalQuery()
        {
            using (var context = new DataContextBuilder()
                .EnableObjectTracking()            
                .LoadWith<Objects.Gemeente>(p => p.Provincie)
                .LoadWith<Objects.Gemeente>(p => p.Plaats)
                .BeginTransaction())
            {
                var x = context.Select<Objects.Gemeente>();

                var y = context.Where<Objects.Gemeente>(p => p.Id > 4);
                var z = context.Single<Objects.Gemeente>(p => p.Id == 4);
                
                context.SubmitChanges();
                context.Commit();
            }
        }

        public void TestSimpleQuery()
        {
            using (var context = new DataContextBuilder().Build())
            {
                Objects.Gemeente gemeente = context.Single<Objects.Gemeente>(p => p.Id == 4);
            }
        }  
     

         public void TestSimpleQuery2()
        {
            using (var context = new DataContextBuilder().Build())
            {
                Objects.Gemeente gemeente = context.Single<Objects.Gemeente>(p => p.Id == 4);

                //TODO this cannot compile because the context is readonly/no objecttracking.
                context.SubmitChanges();
            }
        }

         public void TestSimpleQueryWithTransaction()
         {
             using (var context = new DataContextBuilder().BeginTransaction())
             {
                 Objects.Gemeente gemeente = context.Single<Objects.Gemeente>(p => p.Id == 4);

                 context.InsertOnSubmit(gemeente);
                 context.DeleteOnSubmit(gemeente);

                 context.SubmitChanges();

                 context.Commit();
             }
         }

         public void TestEnableLogging()
         {
             using (var context = new DataContextBuilder()
                 .EnableLogging(new Diagnostics.TraceTextWriter())
                 .Build())
             {
                 Objects.Gemeente gemeente = context.Single<Objects.Gemeente>(p => p.Id == 4);
             }
         }

         public void TestDefaultEnableLogging()
         {
             using (var context = new DataContextBuilder()
                 .EnableObjectTracking()
                 .EnableLogging()
                 .Build())
             {
                 Objects.Gemeente gemeente = context.Single<Objects.Gemeente>(p => p.Id == 4);
             }
         }  
    }
}
