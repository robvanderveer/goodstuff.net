﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using GoodStuff.Text;

namespace GoodStuff.UnitTests
{
    [TestFixture]
    public class TestMailMerger
    {
        [Test]
        public void TestSimpleFormat()
        {
            string result = Templating.FormatCollection("Hallo", null);

            Assert.AreEqual("Hallo", result);
        }

        [Test]
        public void TestEmptyListFormat()
        {
            var dict = new Dictionary<string, object>();
            string result = Templating.FormatCollection("Hallo", dict);

            Assert.AreEqual("Hallo", result);
        }

        [Test]
        [ExpectedException(typeof(FormatException))]
        public void TestUnknownTokens()
        {
            var dict = new Dictionary<string, object>();
            string result = Templating.FormatCollection("Hallo {token}", dict);

            Assert.AreEqual("Hallo {token}", result);
        }

        [Test]
        public void TestSingleToken()
        {
            var dict = new Dictionary<string, object>();
            dict.Add("token", "world");
            string result = Templating.FormatCollection("Hallo {token}", dict);

            Assert.AreEqual("Hallo world", result);
        }

        [Test]
        public void TestSingleTokenTwice()
        {
            var dict = new Dictionary<string, object>();
            dict.Add("token", "world");
            string result = Templating.FormatCollection("Hallo {token}{token}", dict);

            Assert.AreEqual("Hallo worldworld", result);
        }

        [Test]
        public void TestDoubleToken()
        {
            var dict = new Dictionary<string, object>();
            dict.Add("token", "world");
            dict.Add("param", "p");
            string result = Templating.FormatCollection("Hallo {token} {param}", dict);

            Assert.AreEqual("Hallo world p", result);
        }

        [Test]
        public void TestFormatDecimal()
        {
            var dict = new Dictionary<string, object>();
            dict.Add("token", "world");
            dict.Add("param", 12.3d);
            string result = Templating.FormatCollection("Hallo {token} {param}", dict);

            Assert.AreEqual("Hallo world 12,3", result);
        }

        [Test]
        public void TestFormatDecimalSpecified()
        {
            var dict = new Dictionary<string, object>();
            dict.Add("param", 12.3d);
            string result = Templating.FormatCollection("Hallo {param:0.00}", dict);

            Assert.AreEqual("Hallo 12,30", result);
        }

        [Test]
        public void TestIncludeOptionalBlock()
        {
            var dict = new Dictionary<string, object>();
            dict.Add("param", true);
            dict.Add("value", 12.30);
            string result = Templating.FormatCollection("Begin[WHEN:{param}]{value:0.00}[/WHEN]End", dict);

            Assert.AreEqual("Begin12,30End", result);
        }

        [Test]
        [ExpectedException(typeof(FormatException))]
        public void TestFormatException()
        {
            var dict = new Dictionary<string, object>();
            dict.Add("param", false);
            dict.Add("value", 12.30);
            string result = Templating.FormatCollection("Begin[WHEN:{param}]Ja[/WHE]", dict);
        }

        [Test]
        public void TestIncludeOptionalBlockButNotTheOther()
        {
            var dict = new Dictionary<string, object>();
            dict.Add("param", true);
            dict.Add("param2", false);
            string result = Templating.FormatCollection("Begin[WHEN:{param}]Ja[/WHEN][WHEN:{param2}]Nee[/WHEN]End", dict);

            Assert.AreEqual("BeginJaEnd", result);
        }

        [Test]
        public void TestIncludeOptionalBlockAtEnd()
        {
            var dict = new Dictionary<string, object>();
            dict.Add("param", true);
            dict.Add("value", 12.30);
            string result = Templating.FormatCollection("Begin[WHEN:{param}]{value:0.00}[/WHEN]", dict);

            Assert.AreEqual("Begin12,30", result);
        }

        [Test]
        public void TestExcludeOptionalBlockAtEnd()
        {
            var dict = new Dictionary<string, object>();
            dict.Add("param", false);
            dict.Add("value", 12.30);
            string result = Templating.FormatCollection("Begin[WHEN:{param}]{value:0.00}[/WHEN]", dict);

            Assert.AreEqual("Begin", result);
        }

        [Test]
        public void TestExcludeOptionalBlockAtBegin()
        {
            var dict = new Dictionary<string, object>();
            dict.Add("param", false);
            dict.Add("value", 12.30);
            string result = Templating.FormatCollection("[WHEN:{param}]{value:0.00}[/WHEN]", dict);

            Assert.AreEqual("", result);
        }

        [Test]
        public void TestExcludeOptionalBlock()
        {
            var dict = new Dictionary<string, object>();
            dict.Add("param", false);
            dict.Add("value", 12.30);
            string result = Templating.FormatCollection("Begin[WHEN:{param}]{value:0.00}[/WHEN]End", dict);

            Assert.AreEqual("BeginEnd", result);
        }

        [Test]
        public void TestExcludeOptionalBlockNotABoolean()
        {
            var dict = new Dictionary<string, object>();
            dict.Add("param", 23);
            dict.Add("value", 12.30);
            string result = Templating.FormatCollection("Begin[WHEN:{param}]{value:0.00}[/WHEN]End", dict);

            Assert.AreEqual("Begin[WHEN:23]12,30End", result);
        }
    }
}
