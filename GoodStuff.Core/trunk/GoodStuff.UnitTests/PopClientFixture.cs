﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Net.Sockets;
using GoodStuff.Net;

namespace GoodStuff.UnitTests
{
    [TestFixture]
    public class PopClientFixture
    {
        [Test]
        [ExpectedException(typeof(SocketException))]
        public void TestConnectionFail()
        {
            using (PopClient client = new PopClient())
            {                                 
                client.Connect("doesnotexist", 1, null, null);
            }
        }

        [Test]
        [Explicit]
        [ExpectedException(typeof(Pop3Exception))]
        public void TestConnectionFailInvalidDetails()
        {
            using (PopClient client = new PopClient())
            {
                client.Connect("mail.driebier.net", 110, "aap", "noot");
            }
        }

        [Test]
        [Explicit]
        public void TestConnectionValidDetails()
        {
            using (PopClient client = new PopClient())
            {
                client.Connect("mail.driebier.net", 110, "test", "test");
            }
        }

        [Test]
        [Explicit]
        public void TestConnectionValidDetailsAPOP()
        {
            using (PopClient client = new PopClient())
            {
                client.AuthenticateWithAPOP = true;
                client.Connect("mail.driebier.net", 110, "test", "test");
            }
        }

        [Test]
        [Explicit]
        public void TestListMails()
        {
            using (PopClient client = new PopClient())
            {
                client.AuthenticateWithAPOP = true;
                client.Connect("mail.driebier.net", 110, "test", "test");

                foreach(var id in client.GetMessageList())
                {
                    Assert.IsTrue(id > 0);
                }
            }
        }
    }
}
