﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using GoodStuff.Data.Linq;
using System.Linq.Expressions;

namespace GoodStuff.UnitTests
{
    [TestFixture]
    public class LinqGetCacheKeyTests
    {
        [Test]
        public void TestKey()
        {
            IEnumerable<string> items = new string[] { "aap", "noot", "mies", "wim" };

            Expression<Func<string,bool>> expression = p => p.StartsWith("m");

            var key = expression.GetCacheKey();
        }

        [Test]
        public void CacheTestKey()
        {
            IEnumerable<string> items = new string[] { "aap", "noot", "mies", "wim" };

            var query = items.AsQueryable().Where(p => p.StartsWith("m")).AsQueryable();

            var results = query.FromCache();
        }
    }
}
