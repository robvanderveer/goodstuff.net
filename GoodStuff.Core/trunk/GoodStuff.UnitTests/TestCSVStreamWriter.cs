﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.IO;
using GoodStuff.Text;

namespace GoodStuff.UnitTests.LowLevelCSV
{
    [TestFixture]
    public class TestCSVStreamWriter
    {
        [Test]  
        public void TestConstruction()
        {
            StringWriter writer = new StringWriter();

            CSVStreamWriter csvWriter = new CSVStreamWriter(writer, ";");
        }

        [Test]
        public void TestSeparator()
        {
            StringWriter writer = new StringWriter();

            CSVStreamWriter csvWriter = new CSVStreamWriter(writer, ";");
            Assert.AreEqual(";", csvWriter.Separator);
        }

        [Test]
        public void TestOneColumn()
        {
            using (StringWriter writer = new StringWriter())
            {
                CSVStreamWriter csvWriter = new CSVStreamWriter(writer, ";");

                csvWriter.Write("Hello world");

                Assert.AreEqual("Hello world", writer.ToString());
            }
        }

        [Test]
        public void TestOneColumnMultiline()
        {
            using (StringWriter writer = new StringWriter())
            {
                CSVStreamWriter csvWriter = new CSVStreamWriter(writer, ";");

                csvWriter.Write("Hello world");
                csvWriter.WriteLine();
                csvWriter.Write("Hello world");

                Assert.AreEqual("Hello world\r\nHello world", writer.ToString());
            }
        }

        [Test]
        public void TestTwoColumnMultiline()
        {
            using (StringWriter writer = new StringWriter())
            {
                CSVStreamWriter csvWriter = new CSVStreamWriter(writer, ";");

                csvWriter.Write("Hello");
                csvWriter.Write("world");
                csvWriter.WriteLine();
                csvWriter.Write("and again");
                csvWriter.Write("end");
                csvWriter.WriteLine();
                
                Assert.AreEqual("Hello;world\r\nand again;end\r\n", writer.ToString());
            }
        }

        [Test]
        public void TestTwoColumn()
        {
            using (StringWriter writer = new StringWriter())
            {
                CSVStreamWriter csvWriter = new CSVStreamWriter(writer, ",");

                csvWriter.Write("Hello");
                csvWriter.Write("world");

                Assert.AreEqual("Hello,world", writer.ToString());
            }
        }
    }
}
