﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GoodStuff.Web;
using NUnit.Framework;

namespace GoodStuff.UnitTests
{
    [TestFixture]
    public class DictionaryCacheTest
    {
        private class CallCounter
        {
            private int _counter;

            public string GetValue()
            {
                return _counter++.ToString();
            }
        }


        [Test]
        public void TestGet()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();

            CallCounter counter = new CallCounter();
            
            string aap = dict.CachedGet("count", () => counter.GetValue());
            Assert.AreEqual("0", aap);
        }

        [Test]
        public void TestGetTwice()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();

            CallCounter counter = new CallCounter();

            string aap = dict.CachedGet("count", () => counter.GetValue());
            string noot = dict.CachedGet("count", () => counter.GetValue());
            Assert.AreEqual("0", aap);
            Assert.AreEqual("0", noot);
        }

        [Test]
        public void TestGetFlush()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();

            CallCounter counter = new CallCounter();

            string aap = dict.CachedGet("count", () => counter.GetValue());
            dict.CacheFlush("count");
            string noot = dict.CachedGet("count", () => counter.GetValue());
            Assert.AreEqual("0", aap);
            Assert.AreEqual("1", noot);
        }

        [Test]
        public void TestGetVaryBy()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();

            CallCounter counter = new CallCounter();
            CallCounter counter2 = new CallCounter();
            
            string aap = dict.CachedGet("count", 2, () => counter.GetValue());
            Assert.AreEqual("0", aap);
            string noot = dict.CachedGet("count", 3, () => counter2.GetValue());
            Assert.AreEqual("0", noot);


            dict.CacheFlush("count", 2);

            string wim = dict.CachedGet("count", 2, () => counter.GetValue());
            Assert.AreEqual("1", wim);
            string mies = dict.CachedGet("count", 3, () => counter2.GetValue());
            Assert.AreEqual("0", mies);
            
        }      
    }
}
