﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using NUnit.Framework;

namespace GoodStuff.UnitTests
{
    [TestFixture]
    public class TestGetConnectionStrings
    {
        [Test]
        public void TestGetAllConnectionStrings()
        {
            foreach (string name in GoodStuff.ConfigurationHelper.GetConnectionStringNames())
            {
                Console.WriteLine(name);
            }
        }
    }
}
