﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using GoodStuff.Web.Controls;

namespace GoodStuff.UnitTests
{
    [TestFixture]
    public class CustomValidatorTest
    {
        [Test]
        public void AlwaysFails()
        {
            CustomValidationError error = new CustomValidationError("Error");
            error.Validate();
            Assert.IsFalse(error.IsValid);
        }

        [Test]
        public void MessageShouldBeSame()
        {
            string expected = "Error";

            CustomValidationError error = new CustomValidationError(expected);
            Assert.AreEqual(expected, error.ErrorMessage);
        }

        [Test]
        public void SettingMessageShouldBeSame()
        {
            string expected = "Error";

            CustomValidationError error = new CustomValidationError(expected);
            error.ErrorMessage = "something else";
            Assert.AreNotEqual(expected, error.ErrorMessage);
        }

        [Test]
        public void IsValidIsImmutable()
        {
            CustomValidationError error = new CustomValidationError("message");
            error.IsValid = true;
            Assert.IsFalse(error.IsValid);
        }
    }
}
