﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace GoodStuff.UnitTests
{
    [TestFixture]
    public class EnumExtensionTest
    {
        [Test]
        public void TestEnumToList()
        {
            TestEnum x = TestEnum.One;

            IEnumerable<TestEnum> list = Enum<TestEnum>.GetValues();

            Assert.AreEqual(new List<TestEnum>() { TestEnum.One, TestEnum.Two }, list);
        }

        [Test]
        public void TestGenericParse()
        {
            TestEnum x = Enum<TestEnum>.Parse("One");
            Assert.AreEqual(TestEnum.One, x);
        }

        public enum TestEnum
        {
            One,
            Two
        }
    }
}
