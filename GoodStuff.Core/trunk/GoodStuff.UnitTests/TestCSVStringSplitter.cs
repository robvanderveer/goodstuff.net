﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using GoodStuff.Text;

namespace GoodStuff.UnitTests.LowLevelCSV
{
    [TestFixture]
    public class TestCSVStringSplitter
    {
        [Test]
        public void TestSplit()
        {
            CSVStringSplitter splitter = new CSVStringSplitter(";");
            var result = splitter.Split("Aap;Noot");
            Assert.AreEqual(2, result.Length);
            Assert.AreEqual("Aap", result[0]);
            Assert.AreEqual("Noot", result[1]);
        }

        [Test]
        public void VeriftEmptyFirst()
        {
            CSVStringSplitter splitter = new CSVStringSplitter(";");
            var result = splitter.Split(";Aap;Noot");
            Assert.AreEqual(3, result.Length);
            Assert.AreEqual("", result[0]);
            Assert.AreEqual("Aap", result[1]);
            Assert.AreEqual("Noot", result[2]);
        }

        [Test]
        public void VerifyEmptyMiddle()
        {
            CSVStringSplitter splitter = new CSVStringSplitter(";");
            var result = splitter.Split("Aap;;Noot");
            Assert.AreEqual(3, result.Length);
            Assert.AreEqual("Aap", result[0]);
            Assert.AreEqual("", result[1]);
            Assert.AreEqual("Noot", result[2]);
        }

        [Test]
        public void VerifySingleCharacters()
        {
            CSVStringSplitter splitter = new CSVStringSplitter(";");
            var result = splitter.Split("A;A;P");
            Assert.AreEqual(3, result.Length);
            Assert.AreEqual("A", result[0]);
            Assert.AreEqual("A", result[1]);
            Assert.AreEqual("P", result[2]);
        }

        [Test]
        public void VerifyEmptyLast()
        {
            CSVStringSplitter splitter = new CSVStringSplitter(";");
            var result = splitter.Split("Aap;Noot;");
            Assert.AreEqual(3, result.Length);
            Assert.AreEqual("Aap", result[0]);
            Assert.AreEqual("Noot", result[1]);
            Assert.AreEqual("", result[2]);
        }

        [Test]
        public void VerifyEscape()
        {
            CSVStringSplitter splitter = new CSVStringSplitter(";");
            var result = splitter.Split("\"\"\";\"\"Aap\";Noot");
            Assert.AreEqual(2, result.Length);
            Assert.AreEqual("\";\"Aap", result[0]);
            Assert.AreEqual("Noot", result[1]);
        }


        [Test]
        [ExpectedException]
        public void VerifyNoClosingQuote()
        {
            CSVStringSplitter splitter = new CSVStringSplitter(";");
            var result = splitter.Split("\"\"\";\"\"Aap;Noot");
        }

        [Test]
        [ExpectedException]
        public void VerifyQuoteMismatch()
        {
            CSVStringSplitter splitter = new CSVStringSplitter(";");
            var result = splitter.Split("\"\"\";\"\"");
        }     
    }
}
