﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using GoodStuff;
using System.ComponentModel;

namespace GoodStuff.UnitTests
{
    [TestFixture]
    public class AttributeHelperTest
    {
        [Test]
        public void TestFirstAttributeOnClass()
        {
            DefaultPropertyAttribute attrib = typeof(TestClass).GetAttribute<DefaultPropertyAttribute>();

            Assert.IsNotNull(attrib);
            Assert.AreEqual("Me", attrib.Name);
        }

        [Test]
        public void TestFirstAttributeOnInstance()
        {
            TestClass c = new TestClass();
            DefaultPropertyAttribute attrib = c.GetAttribute<DefaultPropertyAttribute>();

            Assert.IsNotNull(attrib);
            Assert.AreEqual("Me", attrib.Name);
        }

        [Test]
        public void TestFirstAttributeOnProperty()
        {
            ReadOnlyAttribute attrib = typeof(TestClass).GetMember("MyStringProperty")[0].GetAttribute<ReadOnlyAttribute>();

            Assert.IsNotNull(attrib);
        }

        [Test]
        public void TestFirstAttributeOnPropertyNotFound()
        {
            // select an attribute that is NOT applied
            PlatformAttribute attrib = typeof(TestClass).GetMember("MyStringProperty")[0].GetAttribute<PlatformAttribute>();

            Assert.IsNull(attrib);
        }

        [Test]
        public void GetAllPropertiesOfAClassThatHaveAnAttribute()
        {
            var result = typeof(TestClass).GetProperties<ReadOnlyAttribute>();

            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void GetAllPropertiesOfAnInstanceThatHaveAnAttribute()
        {
            TestClass c = new TestClass();

            var result = c.GetProperties<ReadOnlyAttribute>();
            
            Assert.AreEqual(1, result.Count());
        }

        [Test]
        public void FieldsShouldNotBeFound()
        {
            TestClass c = new TestClass();

            var result = c.GetProperties<ReadOnlyAttribute>();

            Assert.IsFalse(result.Any(p => p.Name == "MyStringField"));
        }


        [DefaultProperty("Me")]
        public class TestClass
        {
            [ReadOnly(true)]
            public string MyStringProperty { get; set; }

            //this is a field and should not be found.
            [ReadOnly(true)]
            public string MyStringField;
        }
    }
}
