﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

using GoodStuff.Text;

namespace GoodStuff.UnitTests
{
    [TestFixture]
    public class TestFormat
    {
        [Test]
        public void SmallDataInt()
        {
            int x = 4;

            Assert.AreEqual("4 bytes", x.ToSizeFormat());            
        }

        [Test]
        public void SmallDataLong()
        {
            long x = 900;

            Assert.AreEqual("900 bytes", x.ToSizeFormat());
        }

        [Test]
        public void SomeKilobytes()
        {
            long x = 1100;

            Assert.AreEqual("1,1 Kb", x.ToSizeFormat());
        }

        [Test]
        public void ExactlyNineMegabytes()
        {
            long x = 9*1024*1024;

            Assert.AreEqual("9,0 Mb", x.ToSizeFormat());
        }
        [Test]
        public void AlmostNineMegaBytes()
        {
            long x = (9*1024*1024) - (long)(0.1d * 1024 * 1024);

            Assert.AreEqual("8,9 Mb", x.ToSizeFormat());
        }

        [Test]
        public void RoundsToNineMegaBytes()
        {
            long x = 9 * 1024 * 1024 - 1;

            Assert.AreEqual("9,0 Mb", x.ToSizeFormat());
        }

    }
}
