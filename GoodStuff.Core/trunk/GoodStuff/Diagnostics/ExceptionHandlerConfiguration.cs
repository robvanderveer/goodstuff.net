﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace GoodStuff.Diagnostics
{
    /// <summary>
    /// ConfigurationSectionHandler for the ExceptionHandlerModule
    /// </summary>
    public class ExceptionHandlerConfigurationSectionHandler : GoodStuff.ConfigurationSectionHandler<ExceptionHandlerConfiguration>
    {
    }

    /// <summary>
    /// Configuration class for the ExceptionHandlerModule
    /// </summary>
    public class ExceptionHandlerConfiguration
    {
        /// <summary>
        /// Master switch
        /// </summary>
        [XmlAttribute("enabled")]
        public bool Enabled { get; set; }

        [XmlElement("mailer")]
        public ExceptionHandlerMailerConfiguration Mailer { get; set; }

        [XmlElement("sql")]
        public ExceptionHandlerSqlConfiguration Sql { get; set; }

        //Additional settings can be loaded throught the configurationSection sections of their own interfaces.
    }

    /// <summary>
    /// Exclude rule class
    /// </summary>
    public class ExceptionHandlerExclude
    {
        /// <summary>
        /// Creates a new Exclude filter with default settings.
        /// </summary>
        public ExceptionHandlerExclude()
        {
            this.CompareMethod = ExceptionHandlerExceptionMessageCompareMethod.Contains;
        }

        /// <summary>
        /// The Source IP-address to filter on
        /// </summary>
        [XmlAttribute("sourceIP")]
        public string SourceIP { get; set; }

        /// <summary>
        /// The name of the type of the Exception
        /// </summary>
        [XmlAttribute("typeName")]
        public string TypeName { get; set; }

        /// <summary>
        /// The message of the Exception to filter on.
        /// </summary>
        [XmlAttribute("message")]
        public string Message { get; set; }

        /// <summary>
        /// The comparemethod to check if a rule applies or not based on the filter for the exceptionmessage
        /// </summary>
        [XmlAttribute("compareMethod")]
        public ExceptionHandlerExceptionMessageCompareMethod CompareMethod { get; set; }
    }

    /// <summary>
    /// The method to use for checking if the configured text matches the message of the Exception. Defaultmethod = Contains.
    /// </summary>
    public enum ExceptionHandlerExceptionMessageCompareMethod
    {
        /// <summary>
        /// The configured text has to be equal as the exact message of the exception.
        /// </summary>
        Exact,

        /// <summary>
        /// The message of the exception must contains the configured text.
        /// </summary>
        Contains,

        /// <summary>
        /// The message of the exception must start with the configured text
        /// </summary>
        StartsWith
    }

    /// <summary>
    /// Configuration for the Mailerhandler of the ExceptionHandlerModule
    /// </summary>
    public class ExceptionHandlerMailerConfiguration
    {
        /// <summary>
        /// The from e-mailaddress
        /// </summary>
        [XmlAttribute("from")]
        public string From { get; set; }

        /// <summary>
        /// The recipients that will receive the e-mail (separated by ; or ,)
        /// </summary>
        [XmlAttribute("recipients")]
        public string Recipients { get; set; }

        /// <summary>
        /// List of exclude filters
        /// </summary>
        [XmlElement("exclude")]
        public List<ExceptionHandlerExclude> Excludes { get; set; }
    }

    /// <summary>
    /// Configuration for the SQLhandler of the ExceptionHandlerModule
    /// </summary>
    public class ExceptionHandlerSqlConfiguration
    {
        /// <summary>
        /// The namne of the connectionstring to use.
        /// </summary>
        [XmlAttribute("connectionName")]
        public string ConnectionName { get; set; }

        /// <summary>
        /// List of exclude filters
        /// </summary>
        [XmlElement("exclude")]
        public List<ExceptionHandlerExclude> Excludes { get; set; }
    }
}
