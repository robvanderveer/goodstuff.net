﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace GoodStuff.Diagnostics
{
    /// <summary>
    /// Allows extensibility for ExceptionHandlerModule
    /// </summary>
    public interface IExceptionHandler
    {
        void HandleException(Exception ee, HttpRequest request);

        IList<ExceptionHandlerExclude> Excludes { get; }
    }
}
