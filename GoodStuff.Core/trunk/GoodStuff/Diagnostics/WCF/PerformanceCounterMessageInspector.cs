﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Dispatcher;

namespace GoodStuff.Diagnostics.WCF
{
    public class PerformanceCounterMessageInspector : IClientMessageInspector  
    {
        public PerformanceCounterMessageInspector()
        {
            //register the performancecounters. For now, just debug.
        }

        public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            CallDuration duration = (CallDuration)correlationState;
            if (duration != null)
            {
                System.Diagnostics.Trace.WriteLine(DateTime.Now - duration.StartDate);
            }

            throw new NotImplementedException();
        }

        public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel)
        {
            return new CallDuration();
        }

        private class CallDuration
        {
            public DateTime StartDate { get; private set; }

            public CallDuration()
            {
                StartDate = DateTime.Now;
            }
        }
    }
}
