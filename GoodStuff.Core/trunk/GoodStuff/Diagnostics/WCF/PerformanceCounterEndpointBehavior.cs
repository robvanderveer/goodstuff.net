﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Configuration;

namespace GoodStuff.Diagnostics.WCF
{
    public class PerformanceCounterEndpointBehavior : BehaviorExtensionElement, IEndpointBehavior
    {   
        public void AddBindingParameters(ServiceEndpoint endpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {
            return;
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(new PerformanceCounterMessageInspector());         
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.EndpointDispatcher endpointDispatcher)
        {          
        }

        public void Validate(ServiceEndpoint endpoint)
        {
            return;
        }

        public override Type BehaviorType
        {
            get { return typeof(PerformanceCounterEndpointBehavior); }
        }

        protected override object CreateBehavior()
        {
            return new PerformanceCounterEndpointBehavior();
        }
    }
}
