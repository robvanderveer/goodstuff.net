﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Diagnostics
{
    /// <summary>
    /// A generic TextWriter that dumps all its output to the trace window.
    /// </summary>
    public class TraceTextWriter : System.IO.TextWriter
    {
        /// <summary>
        /// Writes a line of text to the trace log.
        /// </summary>
        /// <param name="value"></param>
        public override void WriteLine(string value)
        {
            System.Diagnostics.Trace.WriteLine(value);
        }

        /// <summary>
        /// Returns the encoding for this Textwriter.
        /// </summary>
        public override Encoding Encoding
        {
            get { return Encoding.UTF8; }
        }
    }
}
