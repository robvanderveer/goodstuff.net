﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff
{
    /// <summary>
    /// Defines the interface for resolving implementations
    /// </summary>
    public interface ICanResolveDependencies
    {
        /// <summary>
        /// Resolves the instance of a specific interface
        /// </summary>
        /// <typeparam name="TService">The type of the service requested</typeparam>
        /// <returns></returns>
        TService Resolve<TService>();
        
        /// <summary>
        /// Resolves the instance of a specific interface with parameters
        /// </summary>
        /// <typeparam name="TService">The type of the service requested</typeparam>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <returns></returns>
        TService Resolve<TService, TArg>(TArg arg);
        
        /// <summary>
        /// Resolves the instance of a specific interface with parameters
        /// </summary>
        /// <typeparam name="TService">The type of the service requested</typeparam>
        /// <typeparam name="TArg1"></typeparam>
        /// <typeparam name="TArg2"></typeparam>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <returns></returns>
        TService Resolve<TService, TArg1, TArg2>(TArg1 arg1, TArg2 arg2);
    }
}
