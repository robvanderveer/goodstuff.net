using System;
using System.Collections.Generic;
using System.Text;

namespace GoodStuff.Providers
{
    /// <summary>
    /// Wrapper exception used to group exceptions caused by the messaging provider classes.
    /// </summary>
    public class MessagingException : ApplicationException
    {
        public MessagingException(string message)
            : base(message)
        {
        }

        public MessagingException(string message, Exception inner)
            : base(message, inner)
        {
        }

    }
}
