using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace GoodStuff.Providers
{
    /// <summary>
    /// A data container used for constructing messages (emails, sms, etc.).
    /// </summary>
    /// <remarks>
    /// The details of the handling of the message will be provided by the provider implementation.
    /// </remarks>
    public class Message:System.Collections.Generic.Dictionary<string, object>
    {
        /// <summary>
        /// holder for messageClass
        /// </summary>
        private string _messageClass;

        /// <summary>
        /// holder for attachments
        /// </summary>
        private List<MessageAttachment> _attachments;
        
        /// <summary>
        /// The ID of the message. Backend systems may deal with this to merge layout or to start specific workflows.
        /// </summary>
        public string MessageClass
        {
            get { return _messageClass; }
            set { _messageClass = value; }
        }

        /// <summary>
        /// List of attachments to attach to the message. Use childclasses of the MessageAttachment-type
        /// </summary>
        public List<MessageAttachment> Attachments
        {
            get
            {
                if (_attachments == null)
                {
                    _attachments = new List<MessageAttachment>();
                }
                return _attachments;
            }
            set
            {
                _attachments = value;
            }

        }
    }
}
