using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace GoodStuff.Providers
{
    /// <summary>
    /// Contains specific configuration data for a known messaging class.
    /// </summary>
    public class SmtpMessageConfigurationEntry : ConfigurationElement
    {
        public SmtpMessageConfigurationEntry()
        {
        }

        /// <summary>
        /// The name (or MessageClass) of the template
        /// </summary>
        [ConfigurationProperty("name", IsKey = true)]
        public string Name
        {
            get { return (string)this["name"]; }
        }

        /// <summary>
        /// A formatstring that is used to construct the from address of an smtp message. 
        /// When a property 'user' is defined in the message, use "{user}" to use that property as the FROM address.
        /// </summary>
        [ConfigurationProperty("from", DefaultValue = "[FROM]", IsRequired = false)]
        public string From
        {
            get { return (string)this["from"]; }
        }

        /// <summary>
        /// A formatstring that is used to construct the recipient address of an smtp message. 
        /// When a property 'user' is defined in the message, use "{user}" to use that property as the TO address.
        /// </summary>       
        [ConfigurationProperty("to", DefaultValue = "[TO]", IsRequired = false)]
        public string To
        {
            get { return (string)this["to"]; }
        }

        /// <summary>
        /// A formatstring that is used to construct the copy address of an smtp message. 
        /// When a property 'user' is defined in the message, use "{user}" to use that property as the CC address.
        /// </summary>       
        [ConfigurationProperty("cc", IsRequired = false)]
        public string CC
        {
            get { return (string)this["cc"]; }
        }

        /// <summary>
        /// A formatstring that is used to construct the recipient BCC-address of an smtp message. 
        /// When a property 'user' is defined in the message, use "{user}" to use that property as the BCC address.
        /// </summary>       
        [ConfigurationProperty("bcc", IsRequired = false)]
        public string BCC
        {
            get { return (string)this["bcc"]; }
        }
       
        /// <summary>
        /// A formatstring that is used to construct the subject of an smtp message. 
        /// When a property 'subject' is defined in the message, use "Message relating {subject}" to use that property as the subject.
        /// </summary>       
        [ConfigurationProperty("subject", IsRequired = true)]
        public string Subject
        {
            get { return (string)this["subject"]; }
        }

        /// <summary>
        /// The name of file that contains the template that is used to construct the message body. The filename is considered
        /// relative to the root of the webapplication.
        /// </summary>
        /// <example>
        /// When a property 'name' is defined in the message, you can use "Hi {name}," to use that property in the mail template.
        /// </example>
        [ConfigurationProperty("templateFile", IsRequired = true)]
        public string TemplateFile
        {
            get { return (string)this["templateFile"]; }
        }

        /// <summary>
        /// When true the resulting mail is sent as HTML markup.
        /// </summary>
        [ConfigurationProperty("asHtml", IsRequired = false, DefaultValue=false)]
        public bool AsHtml
        {
            get { return (bool)this["asHtml"]; }
        }

        /// <summary>
        /// This is required for the generic collection to work because this returns the Key column.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }
    }
}
