using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration.Provider;

namespace GoodStuff.Providers
{
    /// <summary>
    /// Collection of MessagingProvider instances.
    /// </summary>
    public class MessagingProviderCollection : ProviderCollectionBase<MessagingProviderBase>
    {
    }
}
