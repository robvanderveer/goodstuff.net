using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace GoodStuff.Providers
{
    [ConfigurationCollection(typeof(ConfigurationElement))]
    public class ConfigurationElementCollection<T> : ConfigurationElementCollection where T : ConfigurationElement, new()
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new T();
        }

        /// <summary>
        /// The ConfigurationElement class requires this.
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((T)(element)).ToString();
        }

        public T this[int idx]
        {
            get { return (T)BaseGet(idx); }
        }

        public T this[object key]
        {
            get { return (T)BaseGet(key); }
        }
    }
}