using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace GoodStuff.Providers
{
    /// <summary>
    /// Contains definition for mail, like email template parameters. 
    /// </summary>
    public class SmtpMessagingConfigurationSection : ConfigurationSection 
    {
        public SmtpMessagingConfigurationSection()
        {
        }

        /// <summary>
        /// Specifies the collection of registered message templates.
        /// </summary>
        [ConfigurationProperty("messages")]
        public ConfigurationElementCollection<SmtpMessageConfigurationEntry> Messages
        {
            get { 
                return base["messages"] as ConfigurationElementCollection<SmtpMessageConfigurationEntry>;
            }           
        }

        /// <summary>
        /// The root directory for the messaging templates.
        /// </summary>
        /// <example>
        /// Use a relative folder like 'templates', or a server-side path like "C:\mailtemplates"
        /// </example>
        [ConfigurationProperty("templateRoot")]
        public string TemplateRoot
        {
            get
            {
                return base["templateRoot"] as string;
            }
        }

        /// <summary>
        /// Forces all emails to be sent to this address instead of the configured TO parameter for the specific template.
        /// </summary>
        /// <remarks>
        /// This is an ideal switch to reroute the delivery of email in a testing environment. For debugging any Messaging stuff, I suggest you 
        /// redirect all Smtp mail using the smtp section of the web.config
        /// <code>
        /// &lt;smtp deliveryMethod="SpecifiedPickupDirectory">
        /// &lt;specifiedPickupDirectory pickupDirectoryLocation="C:\temp\pickup"/>
        /// &lt;/smtp>
        /// </code>
        /// </remarks>
        [ConfigurationProperty("forceRecipient")]
        public string ForceRecipient
        {
            get
            {
                return base["forceRecipient"] as string;
            }
        }
    }

    
}
