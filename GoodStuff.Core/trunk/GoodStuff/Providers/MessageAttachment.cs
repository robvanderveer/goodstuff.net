﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace GoodStuff.Providers
{
    /// <summary>
    /// Attachmentclass for messages
    /// </summary>
    public abstract class MessageAttachment
    {
        /// <summary>
        /// the mimetype name of the attached content(type)
        /// </summary>
        internal string MimeType { get; set; }
    }

    /// <summary>
    /// Attachment that is based on a file on disk
    /// </summary>
    public class MessageAttachmentFromFile : MessageAttachment
    {
        /// <summary>
        /// Creates a new attachment that will be read from disk.
        /// </summary>
        /// <param name="fileName">the name/path of the file to attach</param>
        public MessageAttachmentFromFile(string fileName)
        {
            this.Filename = fileName;
        }

        /// <summary>
        /// Creates a new attachment that will be read from disk. The displayname overrides the original filename.
        /// </summary>
        /// <param name="fileName">the name/path of the file to attach</param>
        /// /// <param name="mimeType">the mimetype name of the attached content(type)</param>
        public MessageAttachmentFromFile(string fileName, string mimeType)
        {
            this.Filename = fileName;
            base.MimeType = mimeType;
        }

        /// <summary>
        /// The name/path of the file to attach
        /// </summary>
        internal string Filename { get; set; }
    }

    /// <summary>
    /// Attachment that will be created from a stream
    /// </summary>
    public class MessageAttachmentFromStream : MessageAttachment
    {
        /// <summary>
        /// Creates a new attachment from a stream
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="name">name of the attachment (example: myExport.xml )</param>
        /// <param name="mimeType">mimetype of the content (example: text/xml )</param>
        public MessageAttachmentFromStream(Stream stream, string name, string mimeType)
        {
            this.ContentStream = stream;
            this.Name = name;
            base.MimeType = mimeType;
        }

        /// <summary>
        /// The stream to read
        /// </summary>
        internal Stream ContentStream { get; set; }

        /// <summary>
        /// Displayname of the file
        /// </summary>
        internal string Name { get; set; }
    }
}
