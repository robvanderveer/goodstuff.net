using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration.Provider;

namespace GoodStuff.Providers
{
    public class ProviderCollectionBase<T> : ProviderCollection where T:ProviderBase
    {
        public new T this[string name]
        {   
            get { return (T)base[name]; }
        }
       
        public override void Add(ProviderBase provider)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");

            if (!(provider is T))
                throw new ArgumentException
                    ("Invalid provider type", "provider");

            base.Add(provider);
        }
    }
}
