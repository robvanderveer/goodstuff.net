﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace GoodStuff.Providers
{
    /// <summary>
    /// Thrown when an exception occurs during the sending of a Smtp message via the SmtpMessagingProvider service.
    /// </summary>
    public class SmtpMessagingException : MessagingException
    {
        public SmtpMessagingException(string reason, Message message, MailMessage mailMessage, Exception innerException):base(reason, innerException)
        {
            this.MailMessage = mailMessage;
            this.Message = message;
        }

        /// <summary>
        /// The inner Smtp MailMessage that was constructed.
        /// </summary>
        /// <remarks>
        /// Note that not all the properties may be filled if an exception occurred during population of the object.
        /// </remarks>
        public MailMessage MailMessage { get; private set; }

        /// <summary>
        /// The entire GoodStuff Message object that was passed to the service.
        /// </summary>
        public Message Message { get; private set; }
    }
}
