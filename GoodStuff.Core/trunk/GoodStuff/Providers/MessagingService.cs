using System;
using System.Configuration;
using System.Configuration.Provider;
using System.Web.Configuration;
using System.Web;

namespace GoodStuff.Providers
{
    /// <summary>
    /// Facade singleton for providing messaging services like email etc.
    /// </summary>
    public class MessagingService
    {
        private static MessagingProviderBase _provider = null;
        private static MessagingProviderCollection _providers = null;
        private static object _lock = new object();

        /// <summary>
        /// The provider used for this service instance.
        /// </summary>
        public MessagingProviderBase Provider
        {
            get { return _provider; }
        }

        /// <summary>
        /// A collection of all known providers.
        /// </summary>
        public MessagingProviderCollection Providers
        {
            get { return _providers; }
        }

        /// <summary>
        /// Create a new message so clients can populate and send the message.
        /// </summary>
        /// <returns></returns>
        public static Message CreateMessage()
        {
            LoadProviders();

            return _provider.CreateMessage();
        }

        /// <summary>
        /// Send the message.
        /// </summary>
        /// <param name="message"></param>
        public static void SendMessage(Message message)
        {
            try
            {
                LoadProviders();

                _provider.SendMessage(message);
            }
            catch (Exception ee)
            {
                throw new MessagingException("Unable to send message", ee);
            }
        }
        
        private static void LoadProviders()
        {
            // Avoid claiming lock if providers are already loaded
            if (_provider == null)
            {
                lock (_lock)
                {
                    // Do this again to make sure _provider is still null
                    if (_provider == null)
                    {
                        // Get a reference to the <imageService> section
                        MessagingProviderSection section = (MessagingProviderSection)WebConfigurationManager.GetSection("goodStuff/messageService");

                        if (section == null)
                        { 
                            throw new ProviderException("configsection goodStuff/messageService not found current configuration.");
                        }

                        // Load registered providers and point _provider
                        // to the default provider
                        _providers = new MessagingProviderCollection();
                        ProvidersHelper.InstantiateProviders(section.Providers, _providers, typeof(MessagingProviderBase));
                        _provider = _providers[section.DefaultProvider];

                        if (_provider == null)
                            throw new ProviderException
                                ("Unable to load default messageProvider");
                    }
                }
            }
        }

    }
}
