using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace GoodStuff.Providers
{
    /// <summary>
    /// Base class for sending mails and notifications.
    /// </summary>
    public abstract class MessagingProviderBase : System.Configuration.Provider.ProviderBase
    {
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            if (config == null)
            {
                throw new ArgumentNullException();
            }

            // Assign "name" a default value if it currently has no value
            // or is an empty string
            if (String.IsNullOrEmpty(name))
                name = "Abstract Messaging Provider";

            // Add a default "description" attribute to config if the
            // attribute doesn't exist or is empty
            if (string.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Abstract Messaging Provider");
            }

            base.Initialize(name, config);
        }

        /// <summary>
        /// Generate a new instance of a message.
        /// </summary>
        /// <returns></returns>
        public virtual Message CreateMessage()
        {
            return new Message();
        }

        /// <summary>
        /// Send the message.
        /// </summary>
        /// <param name="message"></param>
        public abstract void SendMessage(Message message);
    }
}
