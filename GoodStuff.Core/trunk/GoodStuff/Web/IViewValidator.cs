﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Web
{
    public interface IViewValidator : IViewBase
    {
        /// <summary>
        /// Returns true when the pagina contains no errors
        /// </summary>
        bool IsValid { get; }

        /// <summary>
        /// Forces a call to the validation framework.
        /// </summary>
        void Validate(); 
    }
}
