﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Web
{
    public interface IViewExceptionHandler
    {
        /// <summary>
        /// Add any business logic exception to the validation framework.
        /// </summary>
        /// <param name="ee"></param>
        void AddException(Exception ee);
    }
}
