﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Web
{
    /// <summary>
    /// Generic controller for Supervising controller pattern.
    /// </summary>
    /// <typeparam name="TView"></typeparam>
    public abstract class GenericController<TView> where TView : IViewBase
    {
        private TView _view;
        private ICanResolveDependencies _container;

        /// <summary>
        /// Creates a new Controller 
        /// </summary>
        /// <param name="view"></param>
        public GenericController(TView view)
        {
            _view = view;    
        }

        /// <summary>
        /// Creates a new Controller 
        /// </summary>
        /// <param name="view"></param>
        public GenericController(ICanResolveDependencies container, TView view)
        {
            _container = container;
            _view = view;
        }

        /// <summary>
        /// Gets the View that is attached to this controller
        /// </summary>
        public TView View
        {
            get
            {
                return _view;
            }
        }

        public ICanResolveDependencies Container
        {
            get { return _container; }
        }

        /// <summary>
        /// Initializes the view for this controller
        /// </summary>
        public virtual void InitializeView()
        {
            if (!View.IsPostBack)
            {
                BindView();
            }
        }        

        /// <summary>
        /// Binds the controller data to the view.
        /// </summary>
        protected abstract void BindView();
    }
}
