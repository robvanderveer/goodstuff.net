﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace GoodStuff.Web
{
    /// <summary>
    /// Helper class to retrieve the SiteUnderConstructionConfiguration
    /// </summary>
    internal class SiteUnderConstructionConfigurationSectionHandler : GoodStuff.ConfigurationSectionHandler<SiteUnderConstructionConfiguration>
    {
    }

    /// <summary>
    /// Helper class which represents the structure of the SiteUnderConstructionConfiguration-configurationsection.
    /// </summary>
    public class SiteUnderConstructionConfiguration
    {
        /// <summary>
        /// Semicolon or comma separated list of IP-addresses (CIDR-notation) of IP's that will be excluded for redirection to the underconstruction page
        /// </summary>
        [XmlAttribute("ignoreIPs")]
        public string IgnoreIPs { get; set; }

        /// <summary>
        /// Relative (~/pagename.html) or aboslute (http://www.underconstruction.com) URL to the under construction page.
        /// </summary>
        [XmlAttribute("underConstructionPageURL")]
        public string UnderConstructionPageURL { get; set; }
    }
}
