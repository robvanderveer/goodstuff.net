using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodStuff.Web.Controls
{
	/// <summary>
	/// A popup panel is not always visible but instead connected via a PopupButton.
	/// the panel is then shown as an absolute positioned layer in the content.
	/// </summary>
	public class PopupPanel : Panel
	{
		//Aah. I think this can also be done with stylesheets by switching the class.
		//Well this is EXACTLY what we'll do.

		/// <summary>
		/// Determines the initial state of the the popup panel.
		/// </summary>
		public bool Shown
		{
			get 
			{ 
				object o = ViewState["Shown"];
				if (o is bool)
					return (bool)o;
				return false;
			}
			set 
			{
				ViewState["Shown"] = value;
			}
		}

		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			writer.AddStyleAttribute("display", (Shown)?"inline":"none");
			writer.AddStyleAttribute("position", "relative");
			base.AddAttributesToRender (writer);
		}

		protected override void RenderChildren(HtmlTextWriter writer)
		{
			//wrap all contents in another (absolute) div
			writer.AddStyleAttribute("position", "absolute");
			writer.RenderBeginTag("div");
			base.RenderChildren (writer);
			writer.RenderEndTag(); //div
		}


		public static string GetToggleScript(PopupPanel panel)
		{
			return string.Format(@"{0}.style.display = ({0}.style.display == 'none')?'inline':'none';", panel.ClientID);
		}

		public static string GetScript(PopupPanel panel, bool show)
		{
			if(show)
			{
				return string.Format(@"{0}.style.display = 'inline';", panel.ClientID);
			}
			else
			{
				return string.Format(@"{0}.style.display = 'none';", panel.ClientID);
			}
		}
	}	
}
