using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace GoodStuff.Web.Controls
{

    /// <summary>
    /// Calender which can be validated by RequiredFieldValidator. Ensure no clientside validation takes place!
    /// </summary>
    /// <example>
    ///     asp:RequiredFieldValidator EnableClientScript="false" ControlToValidate="calDate" runat="server" ErrorMessage="Please select a date" Display="None" />
    /// </example>
    [ValidationProperty("Text")]
    public class ValidatableCalendar : Calendar
    {
        public string Text
        {
            get
            {
                if (this.SelectedDates.Count > 0)
                {
                    return this.SelectedDate.ToString("yyyy/MM/dd");
                }
                else
                {
                    return string.Empty;
                }
            }
        }
    }
}
