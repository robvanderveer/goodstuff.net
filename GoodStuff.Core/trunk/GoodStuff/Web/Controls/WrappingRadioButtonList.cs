using System;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Globalization;

namespace GoodStuff.Web.Controls
{
	/// <summary>
	/// This radiobutton list fixes the line wrapping problem with long option texts. 
	/// </summary>	
	public class WrappingRadioButtonList : System.Web.UI.WebControls.RadioButtonList, IRepeatInfoUser
	{
		public WrappingRadioButtonList ()
		{
			//
			// TODO: Add constructor logic here
			//
			base.RepeatLayout = System.Web.UI.WebControls.RepeatLayout.Table;
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender (e);
		}

		public override RepeatLayout RepeatLayout
		{
			get
			{
				return base.RepeatLayout;
			}
			set
			{
				if(value != System.Web.UI.WebControls.RepeatLayout.Table)
				{
					throw new NotSupportedException("The WrappingRadioButtonList only works with RepeatLayout.Table");
				}
				base.RepeatLayout = value;
			}
		}

		void IRepeatInfoUser.RenderItem(ListItemType itemType, int repeatIndex, RepeatInfo repeatInfo, HtmlTextWriter writer)
		{
			/* taken from Reflector */
			RadioButton button1 = new RadioButton();
			button1.Page = this.Page;
			button1.GroupName = this.UniqueID;
			button1.ID = this.ClientID + "_" + repeatIndex.ToString(NumberFormatInfo.InvariantInfo);
			//button1.Text = this.Items[repeatIndex].Text;
			button1.Attributes["value"] = this.Items[repeatIndex].Value;
			button1.Checked = this.Items[repeatIndex].Selected;
			button1.TextAlign = this.TextAlign;
			button1.AutoPostBack = this.AutoPostBack;
			button1.TabIndex = this.TabIndex;
			button1.Enabled = this.Enabled;
			button1.RenderControl(writer);

			writer.Write("</td><td>");

			writer.Write("<label for=\"" + button1.ClientID + "\">");
			writer.Write(this.Items[repeatIndex].Text);
			writer.Write("</label>");
		}
	}
}
