﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Collections.Specialized;

namespace GoodStuff.Web.Controls
{
    /// <summary>
    /// A custom password style textbox that remembers its value on postback.
    /// </summary>
    [System.ComponentModel.DefaultProperty("Text")]
    [System.Web.UI.ValidationProperty("Text")]
    public class SecureTextBox : System.Web.UI.WebControls.WebControl, System.Web.UI.IPostBackDataHandler
    {
        private string _value;
        private int _maxLength;

        public SecureTextBox()
            : base("input")
        {
            _maxLength = 16;
        }

        bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            _value = postCollection[postDataKey];
            return false;
        }

        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
        }

        [System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        public string Text
        {
            get
            {
                if (_value == null)
                {
                    return this.InitialText;
                }
                return _value;
            }
            set
            {
                this.InitialText = value;
            }
        }

        public int MaxLength
        {
            get
            {
                return _maxLength;
            }
            set
            {
                _maxLength = value;
            }
        }

        public int Columns { get; set; }

        private string InitialText
        {
            get
            {
                string val = this.ViewState["InitialText"] as string;
                if (!string.IsNullOrEmpty(val))
                {
                    val = GoodStuff.Security.MachineKeyCryptography.Decode(val, System.Web.Security.CookieProtection.All);
                }
                return val;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    this.ViewState["InitialText"] = null;
                }
                else
                {
                    this.ViewState["InitialText"] = GoodStuff.Security.MachineKeyCryptography.Encode(value, System.Web.Security.CookieProtection.All);
                }
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (InitialText == null)
            {
                base.Render(writer);
            }
            else
            {
                writer.Write("**********");
            }
        }

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            base.AddAttributesToRender(writer);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, UniqueID);
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "password");
            writer.AddAttribute(HtmlTextWriterAttribute.Maxlength, _maxLength.ToString());

            if (Columns > 0)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Size, Columns.ToString());
            }

            if (_value != null)
            {
                writer.AddAttribute("value", _value);
            }
        }
    }
}
