﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodStuff.Web.Controls
{
    public class ScheduleGrid : DataBoundControl
    {
        public ScheduleGrid()
        {
            StartDate = DateTime.Today;
            EndDate = StartDate.AddDays(7);
            CssClass = "ScheduleGrid";
        }

        private IDictionary<string, IEnumerable<IDateRange>> _events;

        public IDictionary<string, IEnumerable<IDateRange>> Events
        {
            get { return _events;  }
            set { _events = value;  }
        }

        override 

        public string CssClass
        {
            get { return ViewState.GetValue<string>("CssClass"); }
            set { ViewState.SaveValue<string>("CssClass", value); }
        }

        public DateTime StartDate
        {
            get { return ViewState.GetValue<DateTime>("StartDate"); }
            set { ViewState.SaveValue<DateTime>("StartDate", value); }
        }

        public DateTime EndDate
        {
            get { return ViewState.GetValue<DateTime>("EndDate"); }
            set { ViewState.SaveValue<DateTime>("EndDate", value); }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "0");
            writer.AddAttribute(HtmlTextWriterAttribute.Cellspacing, "0");
            //writer.AddAttribute(HtmlTextWriterAttribute.Width, "100%");
            if (!string.IsNullOrEmpty(CssClass))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, CssClass);            
            }
            writer.RenderBeginTag(HtmlTextWriterTag.Table);
            RenderScale(writer);

            foreach(var item in _events)
            {
                RenderRow(writer, item.Key, item.Value);
            }

            writer.RenderEndTag();
        }

        private void RenderRow(HtmlTextWriter writer, string label, IEnumerable<IDateRange> items)
        {
            TimeSpan cellTime = TimeSpan.FromHours(2);

            //hardcoded, 4 columns per date.

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "Row");
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "Label");
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.Write(label);
            writer.RenderEndTag();

            for (DateTime date = StartDate; date < EndDate; date = date.AddDays(1))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "StartDay");

                for (int hour = 8; hour < 18; hour += (int)cellTime.TotalHours)
                {
                    DateRange range = new DateRange(date.AddHours(hour), cellTime);

                    
                    writer.AddAttribute(HtmlTextWriterAttribute.Style, "width: 40px");
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);

                    DateRange today = new DateRange(DateTime.Today.AddHours(8), DateTime.Today.AddHours(18)); 

                    foreach (IDateRange item in items.Where(p => range.IsInRange(p.Start)).OrderBy(p => p.Duration))
                    {
                        IDateRange item2 = item.Clip(today);

                        //writer.Write("X");

                        //assuming 2 hours is 40 pixels, 1 hour is 20 pixels, so a pixel becomes TotalMinutes / 3;

                        //render a floating DIV in the correct size.
                        writer.AddStyleAttribute(HtmlTextWriterStyle.Position, "relative");
                        writer.AddStyleAttribute(HtmlTextWriterStyle.Left, string.Format("{0}px", (int)((item2.Start - range.Start).TotalMinutes / 3)));
                        writer.AddStyleAttribute(HtmlTextWriterStyle.Top, "2px");

                        writer.RenderBeginTag(HtmlTextWriterTag.Div);

                        

                        writer.AddStyleAttribute(HtmlTextWriterStyle.Position, "absolute");
                        writer.AddStyleAttribute(HtmlTextWriterStyle.Width, string.Format("{0}px", (int)(item2.Duration.TotalMinutes / 3)) );
                        writer.AddStyleAttribute(HtmlTextWriterStyle.Height, "10px");
                        writer.AddStyleAttribute(HtmlTextWriterStyle.BackgroundColor, "white");
                        writer.AddStyleAttribute(HtmlTextWriterStyle.BorderWidth, "1px");
                        writer.AddStyleAttribute(HtmlTextWriterStyle.BorderColor, "black");
                        writer.AddStyleAttribute(HtmlTextWriterStyle.BorderStyle, "solid");

                        writer.RenderBeginTag(HtmlTextWriterTag.Div);

                        writer.RenderEndTag();
                        writer.RenderEndTag();

                    }

                    //if (items.Any())
                    //{
                        
                    //}
                    //else
                    //{ 
                    //    writer.Write("&nbsp;");
                    //}
                    writer.RenderEndTag();
                }
            }
            writer.RenderEndTag();
        }

        protected void RenderScale(HtmlTextWriter writer)
        {
            TimeSpan cellTime = TimeSpan.FromHours(2);

            //hardcoded, 4 columns per date.

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "HeaderDate");
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderEndTag();
            
            for (DateTime date = StartDate; date < EndDate; date = date.AddDays(1))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Colspan, "5");                
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(date.ToString("dd/MM"));
                writer.RenderEndTag();
            }
            writer.RenderEndTag();

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "HeaderTime");
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            writer.RenderEndTag();           
            
            for (DateTime date = StartDate; date < EndDate; date = date.AddDays(1))
            {
                for (int hour = 8; hour < 18; hour += (int)cellTime.TotalHours)
                {
                    DateRange range = new DateRange(date.AddHours(hour), cellTime);

                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(hour.ToString());
                    writer.RenderEndTag();
                }
            }
            writer.RenderEndTag();
        }
    }
}