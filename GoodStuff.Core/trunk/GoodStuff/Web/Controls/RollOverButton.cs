using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.ComponentModel;

namespace GoodStuff.Web.Controls
{
	/// <summary>
	/// A hyperlink with an image that changes when the mouse hovers the link.
	/// </summary>
	public class RollOverButton : System.Web.UI.WebControls.LinkButton
	{
		private string imageUrl;
		private string imageOnUrl;
		private string navigateUrl = null;
		
		// Method of IPostBackEventHandler that raises change events.
		protected override void RaisePostBackEvent(string eventArgument)
		{     
			OnClick(EventArgs.Empty);
		}
 
		/// <summary>
		/// The URL of the image when the mouse is over the link
		/// </summary>
		[Category("Layout")]
		[Editor("System.Web.UI.Design.ImageUrlEditor", typeof(System.Drawing.Design.UITypeEditor))]
		public string ImageOnUrl
		{
			get { return this.imageOnUrl; }
			set { this.imageOnUrl = value; }
		}

		/// <summary>
		/// The URL of the image that is shown.
		/// </summary>
		[Category("Layout")]
		[Editor("System.Web.UI.Design.ImageUrlEditor", typeof(System.Drawing.Design.UITypeEditor))]
		public string ImageUrl
		{
			get { return this.imageUrl; }
			set { this.imageUrl = value; }
		}

		/// <summary>
		/// The url that is navigated to when this link is clicked.
		/// </summary>
		[Category("Behavior")]
		public string NavigateUrl
		{
			get { return this.navigateUrl; }
			set { this.navigateUrl = value; }
		}

		protected override void OnPreRender( EventArgs e ) 
		{
			if(this.navigateUrl != null)
			{
				Attributes.Add("href", this.Page.ResolveUrl(this.navigateUrl));
				
			}
			
			Attributes.Add("OnMouseOver", "document.all['" + this.ClientID + "_img'].src = '" + this.Page.ResolveUrl(imageOnUrl) + "'; window.status = ''; return true;" );
			Attributes.Add("OnMouseOut", "document.all['" + this.ClientID + "_img'].src = '" + this.Page.ResolveUrl(imageUrl) + "'; window.status = ''; return true;" );
			
			// add this rollover object to the collection of images used.
			ScriptManager.RegisterArrayDeclaration(this, "RollOverImageOn", "'" + this.ResolveUrl(imageOnUrl) + "'" );

			// register the rollover preload script.
			// if more rollovers are added, this script is included only once.
			ScriptManager.RegisterStartupScript(this, this.GetType(), "RollOver OnLoad", "for(i=0; i<RollOverImageOn.length;i++){var rollover_preloadimg = new Image(); rollover_preloadimg.src = RollOverImageOn[i];}", true);
		}  
      
		protected override void RenderContents(HtmlTextWriter writer)
		{
			if (this.ImageUrl != String.Empty)
			{
				Image myImage = new Image();
				myImage.Attributes["name"] = this.ClientID + "_img";
				myImage.ImageUrl = this.ImageUrl;
				myImage.AlternateText = this.Text;
				myImage.Height = this.Height;
				myImage.Width = this.Width;
				myImage.ToolTip = this.ToolTip;
				myImage.RenderControl(writer);
			}
			else
				base.RenderContents (writer);
		}
	}
}
