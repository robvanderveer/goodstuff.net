﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

namespace GoodStuff.Web.Controls
{
    /// <summary>
    /// This control is a (failed) attempt to support templated user controls. Work in progress
    /// </summary>
    [Obsolete("Work in progress")]
    [ParseChildren(false)]
    public abstract class ContentUserControl : UserControl
    {      
        private ITemplate _content;

        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateContainer(typeof(BasicTemplateContainer))]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [TemplateInstance(TemplateInstance.Single)]
        public ITemplate Content
        {
            get
            {
                return this._content;
            }
            set
            {
                _content = value;
                applyContent();
            }
        }

        private void applyContent()
        {
            PlaceHolder contentPlaceHolder = this.FindControl("contentPlaceHolder") as PlaceHolder;
            if (contentPlaceHolder == null)
            {
                throw new Exception("ContentUserControl template must have a placeholder named 'contentPlaceHolder'");
            }

            BasicTemplateContainer container = new BasicTemplateContainer();

            Content.InstantiateIn(container);
            contentPlaceHolder.Controls.Add(container);
        }      

        /// <summary>
        /// Support container class for the ContentUserControl
        /// </summary>
        public class BasicTemplateContainer : Control, INamingContainer
        {
        }    
    }
} 
