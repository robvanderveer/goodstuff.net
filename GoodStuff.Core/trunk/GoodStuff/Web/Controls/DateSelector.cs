using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.ComponentModel.Design;

namespace GoodStuff.Web.Controls
{
	/// <summary>
	/// Allows the user to select a date using 3 dropdowns for day, month and year.
	/// </summary>
	[DefaultProperty("SelectedDate")]
	[DefaultEvent("SelectionChanged")]
	public class DateSelector : WebControl, INamingContainer
	{
		private DropDownList _dayList;
		private DropDownList _monthList;
		private DropDownList _yearList;

        /// <summary>
        /// Raises an event when the selection has changed.
        /// </summary>
		public event EventHandler SelectionChanged;

		/// <summary>
		/// Get or set the selected date.
		/// </summary>
		/// <remarks>
		///	If the user selected an invalid date, it gets converted to the first previous date (for example, april 31 becomes april 30.
		/// </remarks>
		public DateTime SelectedDate
		{
			get
			{
				object o = ViewState["SelectedDate"];
				if(o is DateTime)
					return (DateTime)o;
				return DateTime.Now;
			}
			set
			{
				EnsureChildControls();
				ViewState["SelectedDate"] = value;

				_yearList.SelectedValue = value.Year.ToString();
				_monthList.SelectedValue = value.Month.ToString();
				_dayList.SelectedValue = value.Day.ToString();
			}
		}

		public override ControlCollection Controls
		{
			get
			{
				EnsureChildControls();
				return base.Controls;
			}
		}

		protected override void CreateChildControls()
		{
			System.Globalization.DateTimeFormatInfo fi = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat;

			_dayList = new DropDownList();
			_dayList.ID = "Day";
			_dayList.SelectedIndexChanged += new EventHandler(Combo_SelectedIndexChanged);
			Controls.Add(_dayList);

			_monthList = new DropDownList();
			_monthList.ID = "Month";
			_monthList.SelectedIndexChanged += new EventHandler(Combo_SelectedIndexChanged);
			Controls.Add(_monthList);

			_yearList = new DropDownList();
			_yearList.ID = "Year";
			_yearList.SelectedIndexChanged += new EventHandler(Combo_SelectedIndexChanged);
			Controls.Add(_yearList);

			for(int yearNumber = DateTime.Now.Year; yearNumber > DateTime.Now.Year-120; yearNumber--)
			{
				_yearList.Items.Add(new ListItem(yearNumber.ToString(), yearNumber.ToString()));
			}
				
			for(int monthNumber = 1; monthNumber <= 12; monthNumber++)
			{
				_monthList.Items.Add(new ListItem( fi.GetMonthName(monthNumber), monthNumber.ToString() ));
			}
				
			for(int dayNumber = 1; dayNumber <= 31; dayNumber++)
			{
				_dayList.Items.Add(new ListItem( dayNumber.ToString(), dayNumber.ToString() ));
			}					
		}

		private void Combo_SelectedIndexChanged(object sender, EventArgs e)
		{
			int possibleDay = int.Parse(_dayList.SelectedValue);

			bool isGeldig = false;
			while(!isGeldig)
			{
				try
				{
					SelectedDate = new DateTime(int.Parse(_yearList.SelectedValue), int.Parse(_monthList.SelectedValue), possibleDay);
					isGeldig = true;
				}
				catch(ArgumentOutOfRangeException)
				{
					possibleDay--;
					if(possibleDay < 0)
					{
						throw;
					}
				}
			}

			if(SelectionChanged != null)
			{
				SelectionChanged(this, e);
			}
		}
	}
}
