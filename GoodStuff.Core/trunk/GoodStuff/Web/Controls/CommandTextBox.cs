using System;
using System.Web.UI.WebControls;

namespace GoodStuff.Web.Controls
{
	/// <summary>
	/// Wordt gebruikt voor het invoeren van de datums en referenties in het detailscherm.
	/// </summary>
	public class CommandTextBox : TextBox
	{
		public string CommandName
		{
			get 
			{
				return ViewState["CommandName"] as string; 
			}
			set 
			{
				ViewState["CommandName"] = value;
			}
		}

		public string CommandArgument
		{
			get 
			{
				return ViewState["CommandArgument"] as string; 
			}
			set 
			{
				ViewState["CommandArgument"] = value;
			}
		}

		protected override void OnTextChanged(EventArgs e)
		{
			base.OnTextChanged (e);

			//When nobody's listening, raise the event.
			this.RaiseBubbleEvent(this, new System.Web.UI.WebControls.CommandEventArgs(this.CommandName, this.CommandArgument));
		}

	}
}
