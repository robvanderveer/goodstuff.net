using System;
using System.Web.UI;

namespace GoodStuff.Web.Controls
{
	/// <summary>
	/// Summary description for RelativeStylesheet.
	/// </summary>
	[System.ComponentModel.DefaultProperty("Stylesheet")]
	public class RelativeStylesheet : System.Web.UI.Control
	{
		public string Stylesheet
		{
			get 
			{ 
				object o = ViewState["Stylesheet"];
				if(o is string)
					return (string)o;
				return null;
			}
			set
			{
				ViewState["Stylesheet"] = value;
			}
		}

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			writer.AddAttribute("rel", "stylesheet");
			writer.AddAttribute(HtmlTextWriterAttribute.Type, "text/css");
			writer.AddAttribute(HtmlTextWriterAttribute.Href, this.ResolveUrl(Stylesheet));
			writer.RenderBeginTag(HtmlTextWriterTag.Link);
			writer.RenderEndTag();
		}

	}
}
