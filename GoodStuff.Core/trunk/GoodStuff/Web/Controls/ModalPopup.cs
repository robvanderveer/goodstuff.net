﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodStuff.Web.Controls
{
    public class ModalPopup : Control
    {
        public string LinkID { get; set; }
        public string CssClass { get; set; }
        public string PopupUrl { get; set; }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            //link the onclick of popup to made this panel visible, for now, it is just visible.            
            IAttributeAccessor link = this.Parent.FindControl(LinkID) as IAttributeAccessor;
            if (link == null)
            {
                throw new Exception(string.Format("LinkID {0} voor ModalPopupExtender {1} bestaat niet.", LinkID, this.ID));
            }

            link.SetAttribute("onclick", GetScript(this, PopupUrl, true));    //TODO: Load/set the iframe source before showing.
        }
     
        public static string GetScript(ModalPopup panel, string url, bool show)
        {
            if (show)
            {
                return string.Format(@"{0}.style.display = 'inline';document.all.{0}_frm.src = """ + url + @""";", panel.ClientID);
            }
            else
            {
                return string.Format(@";{0}.style.display = 'none';", panel.ClientID);
            }
        }

        public static void Dismiss(Control c, bool refresh)
        {

            if (refresh)
            {

                //detect if a control/page screen was rendered in a popup panel, if yes, dismiss the popup. 
                //this method is usually rendered from another page.
                c.Page.ClientScript.RegisterClientScriptBlock(typeof(ModalPopup), "Dismiss", @"
var arrFrames = parent.document.getElementsByTagName(""IFRAME"");
for (var i = 0; i < arrFrames.length; i++) {
  if (arrFrames[i].contentWindow == window) 
  {
     arrFrames[i].parentNode.parentNode.style.display = 'none';
     window.parent.document.forms[0].submit();
  }
}
", true);
            }
            else
            {
                c.Page.ClientScript.RegisterClientScriptBlock(typeof(ModalPopup), "Dismiss", @"
var arrFrames = parent.document.getElementsByTagName(""IFRAME"");
for (var i = 0; i < arrFrames.length; i++) {
  if (arrFrames[i].contentWindow == window) 
  {
     arrFrames[i].parentNode.parentNode.style.display = 'none';
  }
}
", true);
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            writer.AddStyleAttribute("display", (false) ? "inline" : "none");

            //we need a clientID.
            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.ClientID);
            writer.AddAttribute(HtmlTextWriterAttribute.Class, this.CssClass);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "Shadow");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.RenderEndTag();  //inner DIV

            writer.AddAttribute(HtmlTextWriterAttribute.Class, "Popup");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);            
            writer.AddAttribute(HtmlTextWriterAttribute.Src, "");
            writer.AddAttribute(HtmlTextWriterAttribute.Id, this.ClientID + "_frm");
            writer.AddAttribute("Frameborder", "0");
            writer.RenderBeginTag(HtmlTextWriterTag.Iframe);
            writer.RenderEndTag();
            writer.RenderEndTag();  //inner DIV

            writer.RenderEndTag();  //DIV
        }
    }
}
