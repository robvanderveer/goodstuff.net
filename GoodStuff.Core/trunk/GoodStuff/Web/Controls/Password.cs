using System;
using System.Web;
using System.Web.UI;
using System.Collections.Specialized;

namespace GoodStuff.Web.Controls
{
	/// <summary>
	/// A custom password style textbox that remembers its value on postback.
	/// </summary>
    /// <remarks>
    /// To allow very secure use of initial passwords, set the 'InitialText' value instead of the 'Text' value.
    /// </remarks>
	[System.ComponentModel.DefaultProperty("Text")]	
	public class Password : System.Web.UI.WebControls.WebControl, System.Web.UI.IPostBackDataHandler
	{
        private string _value;
        private int _maxLength;

        public Password()
            : base("input")
        {
            _maxLength = 16;
        }

        bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            _value = postCollection[postDataKey];            
            return false;
        }

        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
        }

        [System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        public string Text
        {
            get
            {
                if (_value == null)
                {
                    return this.InitialText;
                }
                return _value;
            }
            set
            {
                _value = value;
            }
        }

        public int MaxLength
        {
            get
            {
                return _maxLength;
            }
            set
            {
                _maxLength = value;
            }
        }

        /// <summary>
        /// Set the initial text so show a hidden, but known password. If validation fails, set it to NULL to allow manual entry.
        /// </summary>
        public string InitialText
        {
            private get
            {
                string val = this.ViewState["InitialText"] as string;
                if(!string.IsNullOrEmpty(val))
                {
                    val = Security.MachineKeyCryptography.Decode(val, System.Web.Security.CookieProtection.All);
                }
                return val;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    this.ViewState["InitialText"] = null;
                }
                else
                {
                    this.ViewState["InitialText"] = Security.MachineKeyCryptography.Encode(value, System.Web.Security.CookieProtection.All);
                }
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (InitialText == null)
            {
                base.Render(writer);
            }
            else
            {
                writer.Write("**********");
            }
        }

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            base.AddAttributesToRender(writer);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, UniqueID);
            writer.AddAttribute(HtmlTextWriterAttribute.Type, "password");
            writer.AddAttribute(HtmlTextWriterAttribute.Maxlength, _maxLength.ToString());

            if (_value != null)
            {
                writer.AddAttribute("value", _value);
            }
        }
	}
}
