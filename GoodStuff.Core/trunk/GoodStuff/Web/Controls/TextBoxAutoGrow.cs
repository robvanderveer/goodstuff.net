﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace GoodStuff.Web.Controls
{
    /// <summary>
    /// Textbox that will auto grow based on its content.
    /// </summary>
    /// <remarks>The onkeyup event will be overriden so you can't use that by yourself.</remarks>
    public class TextBoxAutoGrow : TextBox
    {
        private const string ScriptName = "TextBoxAutoGrowScript";

        /// <summary>
        /// Creates a new TextBoxAutoGrow with default settings.
        /// </summary>
        public TextBoxAutoGrow()
        {
            this.RowsToGrow = 4;
            this.LineHeight = 13;
            
            if (this.TextMode != TextBoxMode.MultiLine)
            {
                this.TextMode = TextBoxMode.MultiLine;
            }
        }

        /// <summary>
        /// Scripts will be added on OnPreRender-stage
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            //Check if scripts already has been added to the page. If not, add it
            if (Page.ClientScript.IsClientScriptBlockRegistered(TextBoxAutoGrow.ScriptName) == false)
            {
                string script = string.Format(
@"function growTextBox(textarea, maxHeight)
        {{
            var newHeight = textarea.scrollHeight;
            var currentHeight = textarea.clientHeight;

            if (newHeight > currentHeight) {{
                newHeight = newHeight + {0} * {1};
                if (newHeight > maxHeight) {{
                    newHeight = maxHeight;
                }};
                textarea.style.height = newHeight + 'px';
            }}
        }}
", this.RowsToGrow, this.LineHeight);

                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), TextBoxAutoGrow.ScriptName, script, true);
            }

            //Set onkeyup event
            string onKeyUp = this.MaxHeight.HasValue? string.Format("growTextBox(this, {0});", this.MaxHeight.Value.ToString()) : "growTextBox(this);";
            this.Attributes["onkeyup"] = onKeyUp;
        }

        /// <summary>
        /// The number of rows that the textbox must grow when the last row has been filled. Default = 4
        /// </summary>
        public int RowsToGrow { get; set; }

        /// <summary>
        /// Optional: the maximum height (in pixels) that should be used. If the textbox has used all lines, and the new height would 
        /// exceed this MaxHeight, the MaxHeight will be applied. A scrollbar then takes over.
        /// </summary>
        public int? MaxHeight { get; set; }

        /// <summary>
        /// The lineheight (in pixels) that is used, see applied CSS styles. Default = 13.
        /// </summary>
        public int LineHeight { get; set; }
    }

}
