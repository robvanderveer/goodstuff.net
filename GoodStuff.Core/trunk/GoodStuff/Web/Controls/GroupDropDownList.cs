﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace GoodStuff.Web.Controls
{
    /// <summary>
    /// special dropdownlist with extended support for rendering option groups.
    /// </summary>
    /// <remarks>
    /// On the render stage, it detects ListItems with Enabled="false" and renders empty option groups instead.
    /// </remarks>
    public class GroupDropDownList : System.Web.UI.WebControls.DropDownList
    {
        /// <summary>
        /// Gets or sets a value wether or not to automatically disable items that have no value. Defaults to false.
        /// </summary>
        public bool DisableEmptyItems
        {
            get
            {
                object o = ViewState["DisableEmptyItems"];
                if (o is bool)
                {
                    return (bool)o;
                }
                return false;
            }
            set
            {
                ViewState["DisableEmptyItems"] = value;
            }
        }

        /// <summary>
        /// Gets or Sets a value wether to HtmlEncode the ListItems value
        /// Defaults to true, which is the normal behaviour for a dropdownlist.
        /// </summary>
        public bool EncodeItems
        {
            get
            {
                object o = ViewState["EncodeItems"];
                if (o is bool)
                {
                    return (bool)o;
                }
                return true;
            }
            set
            {
                ViewState["EncodeItems"] = value;
            }
        }



        protected override void RenderContents(HtmlTextWriter writer)
        {
            ListItemCollection items = this.Items;
            int count = this.Items.Count;
            bool flag = false;
            if (count > 0)
            {
                for (int i = 0; i < count; i++)
                {
                    ListItem item = items[i];

                    if (item.Enabled == false || (DisableEmptyItems == true && string.IsNullOrEmpty(item.Value)))
                    {
                        writer.WriteBeginTag("optgroup");
                        writer.WriteAttribute("style", "font-style: normal;", true);
                        writer.WriteAttribute("label", item.Text, true);
                        writer.Write('>');
                        writer.WriteEndTag("optgroup");
                        writer.WriteLine();
                    }
                    else
                    {
                        writer.WriteBeginTag("option");
                        item.Attributes.AddAttributes(writer);

                        if (item.Selected)
                        {
                            if (flag)
                            {
                                throw new HttpException("Cant_Multiselect_In_DropDownList");
                            }
                            flag = true;
                            writer.WriteAttribute("selected", "selected", false);
                        }
                        writer.WriteAttribute("value", item.Value, true);
                        writer.Write('>');
                        if (EncodeItems)
                        {
                            HttpUtility.HtmlEncode(item.Text, writer);
                        }
                        else
                        {
                            writer.Write(item.Text);
                        }
                        writer.WriteEndTag("option");
                        writer.WriteLine();
                    }                   
                }
            }
        }
    }
}
