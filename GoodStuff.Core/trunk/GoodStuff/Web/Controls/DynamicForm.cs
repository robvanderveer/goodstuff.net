﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace GoodStuff.Web.Controls
{
    public class DynamicForm : Control, INamingContainer
    {
        private IEnumerable<IDynamicFormElement> Elements
        {
            get { throw new NotImplementedException();  }
        }

        public object DataItem { get; set; }

        public override void DataBind()
        {
            base.DataBind();
        }

        protected override void CreateChildControls()
        {
            base.CreateChildControls();
        }

        protected override void Render(HtmlTextWriter writer)
        {
            RenderHeader(writer);
            foreach (var item in Elements)
            {
                RenderElement(writer);
            }

            RenderFooter(writer);
        }


        protected virtual void RenderHeader(HtmlTextWriter writer)
        {
        }

        protected virtual void RenderElement(HtmlTextWriter writer)
        {
        }

        protected virtual void RenderFooter(HtmlTextWriter writer)
        {
        }
    }

    public interface IDynamicFormElement
    {

    }
}
