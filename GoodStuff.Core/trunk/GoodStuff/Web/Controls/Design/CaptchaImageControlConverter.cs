using System;
using System.Collections;
using System.ComponentModel;
using System.Web.UI.WebControls;

namespace GoodStuff.Web.Controls.Design
{
	/// <summary>
	/// Summary description for CaptchaImageControlConverter.
	/// </summary>
	internal class CaptchaImageControlConverter : ValidatedControlConverter
	{
		private ArrayList GetControls(IContainer container)
		{
			ArrayList results = new ArrayList();
			foreach(IComponent comp in container.Components)
			{
				CaptchaImage captcha = comp as CaptchaImage;
				if(captcha != null)
				{
					results.Add(captcha.ID);
				}
			}
			results.Sort(Comparer.Default);
			return results;
		}

		public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
		{
			if(context == null || context.Container == null)
			{
				return null;
			}

			ArrayList controls = GetControls(context.Container);
			return new StandardValuesCollection(controls);
		}

	}
}
