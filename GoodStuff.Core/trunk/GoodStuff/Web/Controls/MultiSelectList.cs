﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace GoodStuff.Web.Controls
{
    public class MultiSelectList : WebControl
    {
        private ListBox _items;
        private ListBox _selectedItems;
        private Button _addItem;
        private Button _removeItem;

        protected override void CreateChildControls()
        {
            this.Controls.Clear();

            _items = new ListBox();
            _items.ID = "Items";
            this.Controls.Add(_items);
            _selectedItems = new ListBox();
            _selectedItems.ID = "SelectedItems";
            this.Controls.Add(_selectedItems);

            _addItem = new Button();
            _addItem.Text = ">>";
            _addItem.ID = "Add";
            this.Controls.Add(_addItem);

            _removeItem = new Button();
            _removeItem.Text = "<<";
            _removeItem.ID = "Remove";
            this.Controls.Add(_removeItem);
        }

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            //lazy - table layout.
            writer.RenderBeginTag(HtmlTextWriterTag.Table);
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            _items.RenderControl(writer);
            writer.RenderEndTag(); //TD;
            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            _addItem.RenderControl(writer);
            writer.WriteBreak();
            _removeItem.RenderControl(writer);

            writer.RenderEndTag(); //TD;
            writer.RenderBeginTag(HtmlTextWriterTag.Td);
            _selectedItems.RenderControl(writer);
            writer.RenderEndTag(); //TD;
            writer.RenderEndTag();
        }
    }
}
