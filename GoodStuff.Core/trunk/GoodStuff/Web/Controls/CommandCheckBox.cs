﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace GoodStuff.Web.Controls
{
    /// <summary>
    /// A variant of the checkbox control that contains Commandname and Arguments
    /// </summary>
    public class CommandCheckBox : CheckBox
    {
        public string CommandName
        {
            get
            {
                return ViewState["CommandName"] as string;
            }
            set
            {
                ViewState["CommandName"] = value;
            }
        }

        public string CommandArgument
        {
            get
            {
                return ViewState["CommandArgument"] as string;
            }
            set
            {
                ViewState["CommandArgument"] = value;
            }
        }

        protected override void OnCheckedChanged(EventArgs e)
        {
            base.OnCheckedChanged(e);

            //When nobody's listening, raise the event.
            this.RaiseBubbleEvent(this, new CommandEventArgs(this.CommandName, this.CommandArgument));

        }        
    }
}
