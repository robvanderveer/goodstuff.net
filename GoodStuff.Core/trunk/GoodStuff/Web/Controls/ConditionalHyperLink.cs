using System;
using System.Web.UI.WebControls;

namespace GoodStuff.Web.Controls
{
	/// <summary>
	/// Een variant van de bekende HyperLink die geen link is wanneer deze disabled is.
	/// </summary>
	public class ConditionalHyperLink : HyperLink
	{
		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			if(this.Enabled)
			{
				base.Render (writer);
			}
			else
			{
                base.RenderContents(writer);
			}
		}

	}
}
