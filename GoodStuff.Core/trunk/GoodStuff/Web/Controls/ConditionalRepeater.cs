using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

namespace GoodStuff.Web.Controls
{
    /// <summary>
    /// You'd be stupid not to fill the EmptyTemplate folder when using this ConditionalRepeater.
    /// </summary>
    [Obsolete("Gebruik de MultiView")]
    public class ConditionalRepeater : Repeater
    {
        private ITemplate _emptyTemplate;
        private bool _hasItems = false;
    
        [Browsable(false)]
        [DefaultValue(null)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [System.Web.UI.TemplateContainer(typeof(RepeaterItem))]
        public ITemplate EmptyTemplate
        {
            get { return _emptyTemplate; }
            set { _emptyTemplate = value; }
        }

        protected override void OnItemCreated(RepeaterItemEventArgs e)
        {
            base.OnItemCreated (e);
            if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                _hasItems = true;
            }
        }

        protected override void CreateControlHierarchy(bool useDataSource)
        {
            _hasItems = false;

            base.CreateControlHierarchy (useDataSource);
            if(!_hasItems)
            {
                this.Controls.Clear();
                if (EmptyTemplate != null)
                {
                    EmptyTemplate.InstantiateIn(this);
                }
            }
        }
    }
}
