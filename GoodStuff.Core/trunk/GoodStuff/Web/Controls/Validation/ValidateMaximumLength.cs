﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodStuff.Web.Controls.Validation
{
    public class ValidateMaximumLength : ValidationRule
    {
        public int MaximumLength { get; set; }

        public override BaseValidator CreateValidatorFor(Control target)
        {
            ((TextBox)target).MaxLength = MaximumLength;    //support not a validator as such, but an adjustment of the target control.
            return null;
        }
    }
}
