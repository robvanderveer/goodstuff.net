﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodStuff.Web.Controls.Validation
{
    public abstract class ValidationRule
    {
        public abstract BaseValidator CreateValidatorFor(Control target);

        public string[] Properties { get; set; }
        public string Message { get; set; }
    }
}
