﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodStuff.Web.Controls.Validation
{
    public class ValidateRequired : ValidationRule
    {
        public override BaseValidator CreateValidatorFor(Control target)
        {
            if (target is CheckBox)
            {
                return new CheckBoxRequiredFieldValidator()
                {
                };
            }
            if (target is CheckBoxList)
            {
                return new CheckBoxListRequiredFieldValidator()
                {
                };
            }

            return new RequiredFieldValidator()
            {
            };
        }
    }
}
