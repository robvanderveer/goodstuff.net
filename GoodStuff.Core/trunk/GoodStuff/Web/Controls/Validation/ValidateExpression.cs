﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodStuff.Web.Controls.Validation
{
    public class ValidateExpression : ValidationRule
    {
        public override BaseValidator CreateValidatorFor(Control target)
        {
            return new RegularExpressionValidator()
            {
                ValidationExpression = Expression
            };
        }

        public string Expression { get; set; }

        public const string Email = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
    }
}
