using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
using System.ComponentModel;


namespace GoodStuff.Web.Controls
{
	/// <summary>
	/// Voegt een Javascript Alert statement toe aan de output
	/// wanneer de <see cref="Message"/> property gevuld is.
	/// </summary>
	[DefaultProperty("Message")]
	public class Alert : Control
	{
		private string _message = "";

		public Alert()
		{			
		}

		public Alert(string message)
		{
			_message = message;
		}

		/// <summary>
		/// De af te tonen melding. Let erop dat alleen quotes en newlines escaped worden.
		/// </summary>
		/// <example>
		/// <code>
		/// this.Controls.Add(new UXP.Web.UI.Message("Hello world!"));
		/// </code>
		/// </example>
		public string Message
		{
			get 
			{ 
				return _message; 
			}
			set
			{ 
				_message = value; 
			}
		}

		protected override void Render(HtmlTextWriter writer)
		{
			if(_message != null && _message.Length > 0)
			{
				writer.Write("<script language=\"JavaScript\">");
				
				string message = _message.Replace("'", @"\'");
				message = message.Replace("\n", @"\n");

				writer.WriteLine("alert('" + message + "');\n");

				writer.Write("</script>");
			}
		}
	}
}
