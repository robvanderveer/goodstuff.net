﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace GoodStuff.Web.Controls
{
    public class TrackerConfigurationSectionHandler : ConfigurationSectionHandler<TrackerConfiguration>
    {
    }

    public class TrackerConfiguration
    {
        public List<TrackerEntry> Entries { get; set; }
    }

    public class TrackerEntry
    {
        [XmlAttribute]
        public string Include { get; set; }
        public List<TrackerReplacement> Replacements { get; set; }
    }

    public class TrackerReplacement
    {
        [XmlAttribute]
        public string Key { get; set; }
        [XmlAttribute]
        public string Value { get; set; }
    }
}
