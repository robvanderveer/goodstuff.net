﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodStuff.Web.Controls
{
    /// <summary>
    /// Helper class for the CommandMenu control
    /// </summary>
    public class CommandMenuItem : Control
    {
        public CommandMenuItem()
        {
            IsEnabled = true;
        }        

        public string Name { get; set; }

        public string ImageUrl { get; set; }

        public string CommandName { get; set; }

        public bool IsEnabled { get; set; }
    }
}
