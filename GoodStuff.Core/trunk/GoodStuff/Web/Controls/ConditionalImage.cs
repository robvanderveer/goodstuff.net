using System;
using System.Web.UI.WebControls;

namespace GoodStuff.Web.Controls
{
	/// <summary>
	/// Een Image control die 2 gedaanten kan aannemen. Afhankelijk van een op te
	/// geven conditie worden een ander image getoond.
	/// </summary>
	public class ConditionalImage : Image
	{
		private string _alternateImage = null;

		public string AlternateImage
		{
			get { return _alternateImage; }
			set { _alternateImage = value; }
		}

		public bool Condition
		{
			get 
			{ 
				if(ViewState["Condition"] != null)
				{
					return (bool)ViewState["Condition"] ;
				}
				return false;
			}
			set {ViewState["Condition"] = value; }
		}

		protected override void OnPreRender(EventArgs e)
		{
			if(this.Condition == false)
			{				
				if(_alternateImage != null)
				{
					this.ImageUrl = _alternateImage;					
				}
			}
			
			base.OnPreRender (e);			
		}

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			if(this.Condition == true || _alternateImage != null)
			{
				base.Render (writer);
			}
		}

	}
}
