﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace GoodStuff.Web.Controls
{
    public class ToolTipLabel : LiteralControl
    {
        public string ToolTip
        {
            get { return ViewState["Tip"] as string; }
            set { ViewState["Tip"] = value; }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (!string.IsNullOrEmpty(ToolTip))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "tooltip");
                writer.AddAttribute(HtmlTextWriterAttribute.Href, "#");
                writer.AddAttribute(HtmlTextWriterAttribute.Onclick, "return false;");
                writer.AddAttribute("onmouseover", "window.status = '" + HttpUtility.HtmlAttributeEncode(ToolTip) + "'");
                writer.RenderBeginTag(HtmlTextWriterTag.A);
                writer.Write(Text);
                writer.RenderBeginTag(HtmlTextWriterTag.Span);
                writer.Write(ToolTip);
                writer.RenderEndTag(); //SPAN
                writer.RenderEndTag(); //A
            }
            else
            {
                base.Render(writer);
            }
        }
    }
}
