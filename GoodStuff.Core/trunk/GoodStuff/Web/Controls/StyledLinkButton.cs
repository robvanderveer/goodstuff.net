﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace GoodStuff.Web.Controls
{
    /// <summary>
    /// This linkbutton creates an addition 'span' tag for the contents of the link button, so that is can be easily styled.
    /// This is required, because &lt;asp:LinkButton Text="&lt;span&gt;Text&lt;/span&gt;"/&gt; does not work.
    /// </summary>
    public class StyledLinkButton : LinkButton
    {
        protected override void RenderContents(System.Web.UI.HtmlTextWriter writer)
        {
            writer.RenderBeginTag(System.Web.UI.HtmlTextWriterTag.Span);
            base.RenderContents(writer);
            writer.RenderEndTag();
            
        }
    }

    public class StyledButton : Button
    {
        public new string Text { get; set; }

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            this.AddAttributesToRender(writer);
            writer.RenderBeginTag(this.TagName);
            writer.RenderBeginTag(System.Web.UI.HtmlTextWriterTag.Span);
            writer.Write(this.Text);
            writer.RenderEndTag();
            writer.RenderEndTag();
        }       
    }

}
