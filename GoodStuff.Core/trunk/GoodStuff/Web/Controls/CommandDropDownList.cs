using System;

namespace GoodStuff.Web.Controls
{
	/// <summary>
	/// A DropDownList expanded with CommandName and CommandArgument so that
	/// it can be used inside a repeater for example. The SelectedIndexChange event will be bubbled.
	/// </summary>
	public class CommandDropDownList : System.Web.UI.WebControls.DropDownList 
	{
		public string CommandName
		{
			get 
			{
				return ViewState["CommandName"] as string; 
			}
			set 
			{
				ViewState["CommandName"] = value;
			}
		}

		public string CommandArgument
		{
			get 
			{
				return ViewState["CommandArgument"] as string; 
			}
			set 
			{
				ViewState["CommandArgument"] = value;
			}
		}

		protected override void OnSelectedIndexChanged(EventArgs e)
		{
			base.OnSelectedIndexChanged (e);

			this.RaiseBubbleEvent(this, new System.Web.UI.WebControls.CommandEventArgs(this.CommandName, this.CommandArgument));
		}		
	}
}
