using System.Web.UI;
using System.Web.UI.WebControls;

namespace GoodStuff.Web.Controls
{
    /// <summary>
    /// Custom validator which can be used to display an non-validation error/message in the validationsummary. 
    /// Example: After sending an e-mail based on a valid filled in form, you want to show an errormessage because the SMTP couldn't be reached.
    /// </summary>
    /// <example>
    /// protected void butSend_Click(object sender, EventArgs e)
    /// {
    ///     Page.Validators.Add(new ValidationSummaryMessage("Foutje... bedankt", "EmailUs"));
    ///     return; // exit current method/execution.
    /// }
    /// </example>
    public class CustomValidationError : BaseValidator, IValidator
    {
        /// <summary>
        /// Maakt een nieuwe melding.
        /// </summary>
        /// <param name="message"></param>
        public CustomValidationError(string message)
        {
            ErrorMessage = message;
            this.IsValid = false;
        }

        public new bool IsValid
        {
            get
            {
                return false;
            }
            set { }
        }

     
        protected override bool EvaluateIsValid()
        {
            return IsValid;
        }

        protected override bool ControlPropertiesValid()
        {
            return true;
        }
    }
}
