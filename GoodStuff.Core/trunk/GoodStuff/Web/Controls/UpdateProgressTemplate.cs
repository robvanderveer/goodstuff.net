﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.ComponentModel;

namespace GoodStuff.Web.Controls
{
    /// <summary>
    /// UpdatePanelProgressTemplate can be used inside a asp:UpdateProgress to disable interaction (lockdown) with the page while
    /// a partial postback is active. It also shows a popup (loader) with information to indicate that a partial postback is active.
    /// </summary>
    public class UpdatePanelProgressTemplate : Control
    {
        //TODO: Create a default image from assembly? see http://ajaxload.info/
        //TODO: ProgressTemplate as innerProperty in stead of the current Text + Image
        

        /// <summary>
        /// Creates a UpdatePanelProgressTemplate with default settings
        /// </summary>
        public UpdatePanelProgressTemplate()
        {
            //some defaults
            this.LoaderHeight = 30;
            this.LoaderWidth = 200;
            this.Text = "Please wait ...";
            this.ImageAlign = System.Web.UI.WebControls.ImageAlign.AbsMiddle;
        }

        protected override void CreateChildControls()
        {
            base.CreateChildControls();

            //Lockdown (prevent users from using the page)
            HtmlGenericControl iframe = new HtmlGenericControl("iframe");
            if (!string.IsNullOrEmpty(this.CssClassLockdown))
            {
                iframe.Attributes["class"] = this.CssClassLockdown;
            }
            else
            {
                //default styles
                iframe.Attributes["class"] = "goodstuff_web_controls_updateprogresstemplate_lockdown";
                iframe.Style[HtmlTextWriterStyle.BackgroundColor] = "#FFFFFF";
                iframe.Style["moz-opacity"] = "0.5";
                iframe.Style["khtml-opacity"] = ".5";
                iframe.Style["opacity"] = ".5";
                iframe.Style["filter"] = "alpha(opacity=50)";
                iframe.Style[HtmlTextWriterStyle.ZIndex] = "120";
                iframe.Style[HtmlTextWriterStyle.Height] = "100%";
                iframe.Style[HtmlTextWriterStyle.Position] = "absolute";
                iframe.Style[HtmlTextWriterStyle.Top] = "0pt";
                iframe.Style[HtmlTextWriterStyle.Left] = "0pt";
            }
            iframe.Attributes["frameborder"] = "0";
            iframe.Attributes["src"] = "about:blank";
            this.Controls.Add(iframe);

            //Container for showing the animated give, display message or other controls.
            HtmlGenericControl loader = new HtmlGenericControl("div");
            if (!string.IsNullOrEmpty(this.CssClassLoader))
            {
                loader.Attributes["class"] = this.CssClassLoader;
            }
            else
            {
                loader.Attributes["class"] = "goodstuff_web_controls_updateprogresstemplate_loader";
                loader.Style[HtmlTextWriterStyle.ZIndex] = "200";
                loader.Style[HtmlTextWriterStyle.BackgroundColor] = "#FFFFFF";
                loader.Style[HtmlTextWriterStyle.Position] = "absolute";
                loader.Style[HtmlTextWriterStyle.Top] = "0pt";
                loader.Style[HtmlTextWriterStyle.Left] = "0pt";
                loader.Style["border"] = "solid 2px #000000;";
                loader.Style[HtmlTextWriterStyle.Padding] = "35px 10px;";
                loader.Style[HtmlTextWriterStyle.TextAlign] = "center";
            }

            //if (this.ProgressTemplate != null)
            //{
            //    this.ProgressTemplate.InstantiateIn(this);
            //}

            loader.Controls.Add(new System.Web.UI.WebControls.Label
            {
                Text = this.Text,
                CssClass = "loader_label"
            });
            if (!string.IsNullOrEmpty(this.ImageUrl))
            {
                System.Web.UI.WebControls.Image image = new System.Web.UI.WebControls.Image();
                image.CssClass = "loader_image";
                image.ImageUrl = this.ImageUrl;
                image.ImageAlign = this.ImageAlign;
                loader.Controls.Add(image);
            }

            this.Controls.Add(loader);

            //We need a scriptmanager to do some calculations
            ScriptManager scriptmanager = ScriptManager.GetCurrent(this.Page);
            if (scriptmanager != null)
            {
                string script = string.Format(
@" Sys.WebForms.PageRequestManager.getInstance().add_initializeRequest(

    function()
    {{
        if (document.getElementById)
        {{
            var updateprogress_lockdown = document.getElementById('{0}');
            updateprogress_lockdown.style.height = document.documentElement.scrollHeight;
            updateprogress_lockdown.style.width = document.documentElement.scrollWidth;

            var updateprogress_loading = document.getElementById('{1}');
            //updateprogress_loading need hard height+width
            updateprogress_loading.style.height = '{2}px';
            updateprogress_loading.style.width = '{3}px';
            updateprogress_loading.style.top = (document.documentElement.clientHeight / 3 - updateprogress_loading.style.height.replace('px', '') / 2) + document.documentElement.scrollTop + 'px';
            updateprogress_loading.style.left = document.body.offsetWidth / 2 - (updateprogress_loading.style.width.replace('px', '') / 2) + document.documentElement.scrollLeft + 'px';
        }}
    }}
);", iframe.ClientID, loader.ClientID, this.LoaderHeight, this.LoaderWidth);

                //Load the script to the page
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "UpdatePanelProgressTemplate", script, true);

            }
        }

        //[Browsable(false)]
        //[DefaultValue(null)]
        //[PersistenceMode(PersistenceMode.InnerProperty)]
        //public ITemplate ProgressTemplate { get; set; }

        /// <summary>
        /// The CssClass of the 'lockdown'-layer. If not set some default styles will be applied.
        /// </summary>
        public string CssClassLockdown { get; set; }

        /// <summary>
        /// The CssClass of the 'loading'-popup. If not set some default styles will be applied.
        /// </summary>
        public string CssClassLoader { get; set; }

        /// <summary>
        /// The height of the 'loadingpopup'. Default = 50
        /// </summary>
        public int LoaderHeight { get; set; }

        /// <summary>
        /// The width of the 'loadingpopup'. Default = 200
        /// </summary>
        public int LoaderWidth { get; set; }

        /// <summary>
        /// The text to display while page partial loads
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// The relative url to the loaderimage
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// The alignment of the Image
        /// </summary>
        public System.Web.UI.WebControls.ImageAlign ImageAlign { get; set; }
    }
}
