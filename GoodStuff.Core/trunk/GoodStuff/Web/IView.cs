﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Web
{
    /// <summary>
    /// Basis-view voor het Model-View-Presenter pattern. A Page object already implements most of these
    /// but user control may need to override behaviour.
    /// </summary>
    /// <remarks>
    /// Tightly coupled to ASP.Net without depending on it.</remarks>
    [Obsolete("Provide more granularity by using IViewBase", false)]
    public interface IView : IViewBase, IViewValidator, IViewExceptionHandler
    {
               
    }
}
