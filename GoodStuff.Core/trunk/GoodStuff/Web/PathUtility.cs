﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace GoodStuff.Web
{
    public class PathUtility
    {
        /// <summary>
        /// Returns a resolved url, e.g. ~/images/test.gif becomes http://localhost/approot/images/test.gif
        /// </summary>
        /// <param name="virtualPath"></param>
        /// <returns></returns>
        public static string MakeAbsoluteUrl(string virtualPath)
        {
            if (HttpContext.Current == null)
            {
                return virtualPath;
            }
            return GetAbsoluteRoot() + VirtualPathUtility.ToAbsolute(virtualPath);
        }

        /// <summary>
        /// Returns the full url root of the current request, e.g. http://server:81/appdirectory/
        /// </summary>
        /// <returns></returns>
        public static string GetAbsoluteRoot()
        {
            if (HttpContext.Current == null)
            {
                return string.Empty;
            }
            return HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
           
        }      
    }
}
