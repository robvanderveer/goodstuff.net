﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace GoodStuff.Web
{
    public interface IRedirectProvider
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="original"></param>
        /// <returns>the URL to redirect to, or NULL when not to redirect this request.</returns>
        string DetermineRedirect(string virtualPath);
    }
}
