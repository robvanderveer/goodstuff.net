﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Web
{
    public interface IViewLocator
    {
        /// <summary>
        /// Returns the default view for the given controller
        /// </summary>
        /// <typeparam name="TView"></typeparam>
        TView Locate<TView>(string controllername) where TView : IViewBase;
    }

    public class ConventionUserControlViewLocator : IViewLocator
    {
        public TView Locate<TView>(string controllerName) where TView : IViewBase
        {
            //returns the instance of a new view base on the pattern

            return default(TView);
        }
    }
}
