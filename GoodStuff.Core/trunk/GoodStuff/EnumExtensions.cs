﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff
{
    /// <summary>
    /// Extensions for enumerators
    /// </summary>
    [Obsolete("Use Generic Enum")]
    public static class EnumExtensions
    {
        /// <summary>
        /// Creates a Generic List with all posible values of the enum specified
        /// </summary>
        /// <typeparam name="T">Type of enum</typeparam>
        /// <returns>a Generic List with all posible values of the enum specified</returns>
        public static List<T> EnumToList<T>()
        {
            Type enumType = typeof(T);

            // Can't use type constraints on value types, so have to do check like this
            if (enumType.BaseType != typeof(Enum))
                throw new ArgumentException("EnumExtensions : Type must be of type System.Enum");

            return Enum.GetValues(enumType).AsQueryable().Cast<T>().ToList();
        }

        /// <summary>
        /// Parses a string to a strongly-typed enum value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T Parse<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value);
        }  
    }

    /// <summary>
    /// Enum helper class
    /// </summary>
    /// <remarks>
    /// Thanks to Christopher Bennage 
    /// http://devlicious.com/blogs/christopher_bennage/archive/2007/09/13/my-new-little-friend-enum-lt-t-gt.aspx
    /// </remarks>
    /// <typeparam name="T"></typeparam>
    public static class Enum<T>
    {
        /// <summary>
        /// Returns all values of an anum
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<T> GetValues()
        {
            return Enum.GetValues(typeof(T)).AsQueryable().Cast<T>();
        }

        /// <summary>
        /// Parses a string to a strongly-type enum value.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T Parse(string value)
        {
            return (T)Enum.Parse(typeof(T), value);
        }

        /// <summary>
        /// Parses a string to a strongly-type enum value.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T Parse(string value, bool ignoreCase)
        {
            return (T)Enum.Parse(typeof(T), value, ignoreCase);
        }
    }
}