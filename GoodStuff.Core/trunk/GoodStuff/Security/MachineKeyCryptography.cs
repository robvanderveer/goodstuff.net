using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Security;

namespace GoodStuff.Security
{
    /// <summary>
    /// Provides access to encryption of cookie data using the machinekey
    /// </summary>
    public static class MachineKeyCryptography
    {
        /// <summary>
        /// Encodes a gives string using machinekey encryption.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="cookieProtection"></param>
        /// <returns></returns>
        public static string Encode(string text, CookieProtection cookieProtection)
        {
            if (string.IsNullOrEmpty(text) || cookieProtection == CookieProtection.None)
            {
                return text;
            }
            byte[] buf = Encoding.UTF8.GetBytes(text);
            return CookieProtectionHelperWrapper.Encode(cookieProtection, buf, buf.Length);
        }

        /// <summary>
        /// Decodes an encrypted string using machinekey encryption.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="cookieProtection"></param>
        /// <returns></returns>
        public static string Decode(string text, CookieProtection cookieProtection)
        {
            if (string.IsNullOrEmpty(text))
            {
                return text;
            }
            byte[] buf;
            try
            {
                buf = CookieProtectionHelperWrapper.Decode(cookieProtection, text);
            }
            catch (Exception ex)
            {
                throw new System.Security.SecurityException(
                    "Unable to decode the text", ex.InnerException);
            }
            if (buf == null || buf.Length == 0)
            {
                throw new System.Security.SecurityException(
                    "Unable to decode the text");
            }
            return Encoding.UTF8.GetString(buf, 0, buf.Length);
        }

    }
}
