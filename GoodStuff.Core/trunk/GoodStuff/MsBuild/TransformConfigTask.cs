﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Build.Framework;
using System.Xml;
using System.IO;
using Microsoft.Build.Utilities;
using System.Xml.Serialization;

namespace GoodStuff.MsBuild
{
    /// <summary>
    /// Custom MSBuild task to transform a web.config or app.config by injecting required Xml fragments into a target file.
    /// </summary>
    public class TransformConfigTask : Microsoft.Build.Utilities.Task
    {
        private TransformationConfiguration Configuration;

        [Required]
        public ITaskItem[] SourceFiles
        {
            get;
            set;
        }

        /// <summary>
        /// One or more transformation files to be applied to each of the sourcefiles.
        /// </summary>
        [Required]
        public ITaskItem[] Transformations
        {
            get;
            set;
        }

        [Required]
        public string DestinationFolder
        {
            get;
            set;
        }

        [Output]
        public ITaskItem[] Output
        {
            get;
            set;
        }


        public override bool Execute()
        {
            //load the optional configuration file.
            

            IList<ITaskItem> outputs = new List<ITaskItem>();

            foreach (ITaskItem source in SourceFiles)
            {
                Log.LogMessage(MessageImportance.Normal, "Processing {0}", source.ItemSpec);

                string sourceDir = System.IO.Path.GetDirectoryName(source.ItemSpec);
                string sourceFile = System.IO.Path.GetFileName(source.ItemSpec);

                Dictionary<string, string> parameters = GetConfigurationForFile(sourceDir);

                if (parameters != null)
                {
                    Log.LogWarning("Override found for {0}", source.ItemSpec);

                    foreach (var param in parameters)
                    {
                        Log.LogMessage("{0} => {1}", param.Key, param.Value);
                    }
                }

                //load the xml document.
                XmlDocument sourceDoc = new XmlDocument();
                sourceDoc.PreserveWhitespace = true;
                sourceDoc.Load(source.ItemSpec);

                foreach (ITaskItem transformation in Transformations)
                {
                    //only if the subdir of the itemspec partially matches the source, apply it.
                    string transformDirectory = Path.GetDirectoryName(transformation.ItemSpec);
                    if (sourceDir.StartsWith(transformDirectory))
                    {
                        Log.LogMessage(MessageImportance.Normal, "Applying {0}", transformation.ItemSpec);
                        ApplyTransformation(sourceDoc, transformation.ItemSpec, parameters);
                    }
                }

                //construct a filename from the source with the filename.
                string destDir = Path.Combine(DestinationFolder, sourceDir);
                if (!Directory.Exists(destDir))
                {
                    Directory.CreateDirectory(destDir);
                }
                string destinationFile = Path.Combine(destDir, sourceFile);

                Log.LogMessage(MessageImportance.Normal, "Writing to {0}", destinationFile);

                XmlWriterSettings settings = new XmlWriterSettings()
                {
                    Indent = true,
                    IndentChars = @"    ",
                    NewLineChars = Environment.NewLine,
                    NewLineHandling = NewLineHandling.Replace,
                };
                using (XmlWriter writer = XmlWriter.Create(destinationFile, settings))
                {
                    sourceDoc.Save(writer);
                }
                ITaskItem output = new TaskItem(destinationFile);
                outputs.Add(output);
            }

            Output = outputs.ToArray();

            return true;
        }

        private void ApplyTransformation(XmlDocument sourceDoc, string p, Dictionary<string,string> overrides)
        {
            //recursively merge the source file with the destination document.
            XmlDocument transformDoc = new XmlDocument();
            transformDoc.PreserveWhitespace = true;
            transformDoc.Load(p);

            Xml.XmlMerger merger = new Xml.XmlMerger(sourceDoc.DocumentElement);
            merger.ApplyMerge(transformDoc.DocumentElement, overrides);
        }
        
        /// <summary>
        /// The filename contains directory parts. Recursively scan the parent directories to collect values,
        /// using depth-first collections, then, replace local override parameters
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private Dictionary<string, string> GetConfigurationForFile(string folder)
        {
            Dictionary<string, string> parentConfig = new Dictionary<string, string>();

            if (!string.IsNullOrEmpty(folder))
            {
                string parentDir = Path.GetDirectoryName(folder);
                parentConfig = GetConfigurationForFile(parentDir);  
            }

            //load the config
            string configFile = Path.Combine(folder, "transformation.xml");

            Log.LogMessage("Looking for {0}", configFile);
            if (!string.IsNullOrEmpty(configFile) && File.Exists(configFile))
            {
                using (var stream = File.OpenRead(configFile))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(TransformationConfiguration));
                    TransformationConfiguration localConfig = (TransformationConfiguration)serializer.Deserialize(stream);

                    foreach (var param in localConfig.Parameters)
                    {
                        parentConfig[param.Key] = param.Value;
                    }
                }
            }

            return parentConfig;
        }
    }
}
