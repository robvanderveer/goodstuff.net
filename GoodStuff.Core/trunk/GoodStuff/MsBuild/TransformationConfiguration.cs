﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace GoodStuff.MsBuild
{
    /// <summary>
    /// Serialized class that contains the optional 'transformation.xml' configuration section
    /// to drive the transformation task.
    /// </summary>
    public class TransformationConfiguration
    {
        [XmlArrayItem("Parameter")]
        public List<ParameterEntry> Parameters { get; set; }

        public class ParameterEntry
        {
            [XmlAttribute]
            public string Key { get; set; }

            [XmlAttribute]
            public string Value { get; set; }
        }
    }

    
}
