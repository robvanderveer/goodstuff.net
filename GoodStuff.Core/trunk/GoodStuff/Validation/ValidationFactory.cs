﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Validation
{
    public class ValidationFactory
    {
        public static IRuleBuilder<T> CreateValidator<T>() where T: class
        {
            //generate a validation ruleset from attributes.
            return new AttributeBasedRuleSet<T>();
        }
    }   
}
