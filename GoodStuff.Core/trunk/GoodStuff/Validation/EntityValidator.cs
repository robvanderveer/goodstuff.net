﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace GoodStuff.Validation
{
    public abstract class EntityValidator<TEntity> : IRuleBuilder<TEntity> where TEntity : class
    {
        IList<IValidationRule<TEntity>> rules = new List<IValidationRule<TEntity>>();

        public EntityValidator()
        {
            Setup();
        }

        protected abstract void Setup();

        protected void Require<TProperty>(Func<TEntity, TProperty> propExpression, IValidationRule<TProperty> validator)
        {
            rules.Add(new Rules.PropertyValidator<TEntity, TProperty>(propExpression, validator));
        }       

        public IList<IValidationRule<TEntity>> GetRules()
        {
            return rules;
        }
    }

    public class PropertyRuleBuilder
    {
        public PropertyRuleBuilder()
        {

        }


    }
}
