﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Validation
{
    /// <summary>
    /// Defines a rule that must be satisfied in order to succeed. This can anything really.
    /// </summary>
    public interface IValidationRule<TType>
    {
        void Validate(TType target);
        bool IsValid { get; }
        string ErrorMessage { get; }
    }
}
