﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Validation
{
    public class ValidationException : Exception
    {
        public ValidationException(ValidationResults results)
        {
            Results = results;
        }

        public ValidationResults Results { get; private set; }

        public override string Message
        {
            get
            {
                return string.Join("\n", Results.Errors.Select(p => p.Message).ToArray());
            }
        }
    }
}
