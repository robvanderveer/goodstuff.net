﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Validation
{
    public class ValidationResults
    {
        private List<IValidationError> _errors;

        public ValidationResults()
        {
            _errors = new List<IValidationError>();
        }

        public bool IsValid { get { return _errors.Count == 0; } }
        public List<IValidationError> Errors { get { return _errors; } }
    }

    public interface IValidationError
    {
        /// <summary>
        /// This property name is require to be able to pinpoint the actual error back to the input field.
        /// </summary>
        string PropertyName { get; }
        string Message { get; }
    }

    /// <summary>
    /// TODO: Sample concrete class.
    /// </summary>
    public class ValidationError : IValidationError
    {
        public ValidationError(string message)
        {
            // TODO: Complete member initialization
            Message = message;
        }
        public string PropertyName { get; private set; }
        public string Message { get; private set; }
    }
}
