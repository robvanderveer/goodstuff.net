﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Validation.Rules
{
    public class StringLengthRule : ValidatorRuleBase<string>
    {
        public StringLengthRule(int maxLength)
        {
            MaxLength = maxLength;
        }

        public int MaxLength { get; private set; }

        /// <summary>
        /// Validates any value to see if it is null or empty. The type is given.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected override bool ValidateCore(string value)
        {
            if (value == null)
            {
                return true;
            }
            return value.Length < MaxLength;
        }

        public override string ErrorMessage
        {
            get { return "is too long"; }
        }
    }
}
