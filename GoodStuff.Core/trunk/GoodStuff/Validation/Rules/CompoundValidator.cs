﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Validation.Rules
{
    public class CompoundValidator<TEntity> : IValidationRule<TEntity>
    {
        IEnumerable<IValidationRule<TEntity>> _validators;

        public CompoundValidator(IEnumerable<IValidationRule<TEntity>> validators )
        {
            _validators = validators;
        }

        public virtual void Validate(TEntity target)
        {
            IsValid = true;
            foreach (IValidationRule<TEntity> rule in _validators)
            {
                rule.Validate(target);
                IsValid &= rule.IsValid;
            }
        }

        public bool IsValid
        {
            get;
            private set;
        }

        public string ErrorMessage
        {
            get
            {
                return string.Join("\n", _validators.Where(p => p.IsValid == false).Select(p => p.ErrorMessage).ToArray());
            }
        }
    }
}
