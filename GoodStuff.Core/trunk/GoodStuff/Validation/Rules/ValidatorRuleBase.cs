﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace GoodStuff.Validation.Rules
{
    /// <summary>
    /// Base class to support the validation of a single property value.
    /// </summary>
    public abstract class ValidatorRuleBase<TType> : IValidationRule<TType>
    {
        public void Validate(TType target)
        {
            IsValid = ValidateCore(target);
        }

        public bool IsValid
        {
            get;
            private set;
        }   

        protected abstract bool ValidateCore(TType value);

        public abstract string ErrorMessage { get; }        
    }
}
