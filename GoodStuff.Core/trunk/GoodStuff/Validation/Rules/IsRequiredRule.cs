﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace GoodStuff.Validation.Rules
{
    public class IsRequiredRule : ValidatorRuleBase<string>
    {        
        /// <summary>
        /// Validates any value to see if it is null or empty. The type is given.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected override bool ValidateCore(string value)
        {          
            return (!string.IsNullOrEmpty(value));
        }


        public override string ErrorMessage
        {
            get { return "is required"; }
        }
    }   
}
