﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Validation
{
    public interface IRuleBuilder<TClass> where TClass : class
    {
        //ValidationResults Validate(TClass entity);
        IList<IValidationRule<TClass>> GetRules();
    }
}
