﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace GoodStuff
{
    /// <summary>
    /// Extensions for Lists
    /// </summary>
    public static class CollectionExtensions
    {
        /// <summary>
        /// Shuffles the items within the collection.
        /// </summary>
        /// <param name="source"></param>
        public static IList Shuffle(this IList source)
        {
            Random rnd = new Random();
            for (int inx = source.Count - 1; inx > 0; --inx)
            {
                int position = rnd.Next(inx);
                object temp = source[inx];
                source[inx] = source[position];
                source[position] = temp;
            }
            return source;
        }
    }
}
