﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace GoodStuff
{
    public class ContainerConfigurationSectionHandler : ConfigurationSectionHandler<ContainerConfiguration>
    {
    }

    public class ContainerConfiguration
    {
        public List<ContainerConfigurationEntry> Types { get; set; }
    }

    public class ContainerConfigurationEntry
    {
        [XmlAttribute]
        public string Interface { get; set; }
        [XmlAttribute]
        public string Type { get; set; }        
    }
}
