﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Exceptions
{
    [Serializable]
    public class UnclearSpecificationException : Exception
    {
        public UnclearSpecificationException() { }
        public UnclearSpecificationException(string message) : base(message) { }
        public UnclearSpecificationException(string message, Exception inner) : base(message, inner) { }
        protected UnclearSpecificationException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
