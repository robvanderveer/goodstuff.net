﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Exceptions
{
    [Serializable]
    public class OutOfTheWindowException : Exception
    {
        public OutOfTheWindowException() { }
        public OutOfTheWindowException(string message) : base(message) { }
        public OutOfTheWindowException(string message, Exception inner) : base(message, inner) { }
        protected OutOfTheWindowException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
