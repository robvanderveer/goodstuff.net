﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Exceptions
{
    [Serializable]
    public class IncompetentUserException : Exception
    {
        public IncompetentUserException() { }
        public IncompetentUserException(string message) : base(message) { }
        public IncompetentUserException(string message, Exception inner) : base(message, inner) { }
        protected IncompetentUserException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
