﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GoodStuff.Exceptions
{
    [Serializable]
    public class ForTheLoveOfGodWhyException : Exception
    {
        public ForTheLoveOfGodWhyException() { }
        public ForTheLoveOfGodWhyException(string message) : base(message) { }
        public ForTheLoveOfGodWhyException(string message, Exception inner) : base(message, inner) { }
        protected ForTheLoveOfGodWhyException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
