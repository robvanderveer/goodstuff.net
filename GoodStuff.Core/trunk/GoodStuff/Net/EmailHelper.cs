﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace GoodStuff.Net
{
    public class EmailHelper
    {
        public readonly static string Email = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

        public static bool Verify(string email)
        {
            //get the domain part.
            if(email.IndexOf("@") > 0)
            {
                string[] parts = email.Split('@');

                try
                {
                    IPHostEntry entry = System.Net.Dns.GetHostEntry(parts[1]);
                    if (entry != null && entry.AddressList != null && entry.AddressList.Length > 0)
                    {
                        return true;
                    }
                }
                catch (SocketException ee)
                {
                    //No such host is known.
                    return false;
                }                
            }

            return false;            
        }
    }
}
