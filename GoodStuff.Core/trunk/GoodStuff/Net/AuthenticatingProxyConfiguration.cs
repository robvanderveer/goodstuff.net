﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace GoodStuff.Net
{
    public class AuthenticatingProxyConfigurationSection : ConfigurationSectionHandler<AuthenticatingProxyConfiguration>
    {
    }

    public class AuthenticatingProxyConfiguration
    {
        [XmlAttribute("userName")]
        public string UserName { get; set; }

        [XmlAttribute("password")]
        public string Password { get; set; }

        [XmlAttribute("domain")]
        public string Domain { get; set; }

        [XmlAttribute("proxyUrl")]
        public string ProxyUrl { get; set; }
    }
}
