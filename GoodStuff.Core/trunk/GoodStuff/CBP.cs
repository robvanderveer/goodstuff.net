﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Microsoft.Contracts;

namespace GoodStuff
{
    /// <summary>
    /// Namespace that contains assertion statement for Contract Based Programming
    /// </summary>
    public class CBP
    {
        [Conditional("DEBUG")]
        public static void Precondition(bool expression, string message)
        {
            if(!expression)
            {
                throw new PreconditionException(message);
            }

        }
    }

    public class PreconditionException : Exception
    {
        public PreconditionException(string message):base(message)
        {

        }
    }
}